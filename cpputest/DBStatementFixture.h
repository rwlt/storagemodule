#ifndef D_DBSTATEMENTFIXTURE_H
#define D_DBSTATEMENTFIXTURE_H

#include <string>
#include <vector>
#include "storage/RowItem.h"
#include "storage/DatabaseConnection.h"
#include <algorithm>

///////////////////////////////////////////////////////////////////////////////
//
//  ParseStatementFixture:
//
///////////////////////////////////////////////////////////////////////////////
using namespace Storage;
using namespace std;

void create_test_db(Storage::Driver::DatabaseConnection dbconnect, string userName, string userPassword, string role,
    vector<string> movies);

struct MovieAttrs {
  struct Table {
    string Movies{"Movies"};
  } T;
  struct Fields {
    string ID{"M_ID"};
    string Movie{"Movie"};
  } F;
};

struct TestModelAttrs {
  struct Table {
    string Todo{"Todo"};
  } T;
  struct Fields {
    string ID{"ID"};
    string Task{"Task"};
    string Duration{"Duration"};
    string Complete{"Complete"};
    string Time{"Time"};
  } F;
};

class DBStatementFixture
{
public:
    explicit DBStatementFixture() :
    m_xml("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
    "<table entity=\"Module::Hero\" name=\"HERO\" key=\"ID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field name=\"ID\" type=\"autogenint\"/>\n"
    "        <field name=\"statute\" type=\"string\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</table>\n"
    "<table entity=\"Module::Person\" name=\"PERSON\" key=\"ID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field name=\"ID\" type=\"string\"/>\n"
    "        <field name=\"firstname\" type=\"string\"/>\n"
    "        <field name=\"surname\" type=\"string\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</table>\n"
    "<table entity=\"Module::Staff\" name=\"STAFF\" key=\"ID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field ame=\"ID\" type=\"string\"/>\n"
    "        <field name=\"firstname\" type=\"string\"/>\n"
    "        <property name=\"surname\" type=\"string\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</table>\n"
    "</module-dbstorage>\n")
    {};
    virtual ~DBStatementFixture(){};

    const char* m_xml;

public:
    DBStatementFixture ( const DBStatementFixture & );
    DBStatementFixture &operator= ( const DBStatementFixture & );

private:
        
};


#endif  // D_DBSTATEMENTFIXTURE_H
