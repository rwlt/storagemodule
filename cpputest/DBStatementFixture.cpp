
#include "DBStatementFixture.h"
#include <storage/DBStorage.h>
#include "storage/RowItem.h"

using namespace Storage;

void create_test_db(
    Driver::DatabaseConnection dbconnect, string userName, string userPassword, string role,
    vector<string> movies)
{
  auto dr = Driver::DBDriver::CreateDBDriver(dbconnect, userName, userPassword, role);
  try
  {
    unique_ptr<Driver::DBStatement> pSt;
    unique_ptr<Driver::DBResult> pRt;
    Domain::RowItem row;
    int m_id = 0;
    dr->BeginTransaction();
    for (string new_str : movies)
    {
      pSt = dr->CreateStatement("INSERT INTO Movies(M_ID, Movie) VALUES (" + std::to_string(m_id++) + ", '" + new_str + "') ");
      //std::cerr << "create_test_db\n";
      pSt->Execute();
      //res->Fetch();
    }
    dr->Commit();
  }
  catch (exception &e)
  {
    stringstream message;
    std::cerr << "Model create_tables - " << e.what();
  }
}
