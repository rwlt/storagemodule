
#include "storage/DatabaseConnection.h"
#include <sstream>
#include <string>
#include <iostream>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
//
//-------------------------------------------------------
// ParseConnectionAccessor test group
//-------------------------------------------------------

//
//-------------------------------------------------------
// ParseConnectionAccessor test group
//-------------------------------------------------------

TEST_GROUP(DatabaseConnectionAccessor) {

};

TEST(DatabaseConnectionAccessor, Constructor) {
    Storage::Driver::DatabaseConnection dc{"firebird", "localhost", "listbook_test", "UTF8"};
    Storage::Driver::DatabaseConnection dc_comp{dc};

    STRCMP_EQUAL(dc.getServer().c_str(), dc_comp.getServer().c_str());
    STRCMP_EQUAL(dc.getDriver().c_str(), dc_comp.getDriver().c_str());
    STRCMP_EQUAL(dc.getDBFile().c_str(), dc_comp.getDBFile().c_str());
    STRCMP_EQUAL(dc.getCharSet().c_str(), dc_comp.getCharSet().c_str());

}

TEST(DatabaseConnectionAccessor, AssignmentOperator) {
    Storage::Driver::DatabaseConnection dc{"firebird", "localhost", "listbook_test", "UTF8"};
    Storage::Driver::DatabaseConnection dc_assign = dc;
    CHECK(dc == dc_assign);
    Storage::Driver::DatabaseConnection &dc_assign2 = dc;
    CHECK(dc == dc_assign2);
}

TEST(DatabaseConnectionAccessor, EqualOperator) {
    Storage::Driver::DatabaseConnection dc{"firebird", "localhost", "listbook_test", "UTF8"};
    Storage::Driver::DatabaseConnection dc_comp{dc};
    CHECK(dc == dc_comp);
}

TEST(DatabaseConnectionAccessor, NotEqualOperator) {
    Storage::Driver::DatabaseConnection dc{"firebird", "localhost", "listbook_test", "UTF8"};
    Storage::Driver::DatabaseConnection dc_comp{dc};
    Storage::Driver::DatabaseConnection dc_notcomp{"firebird", "local", "listbook", "UTF*"};
    CHECK_EQUAL(false, dc != dc_comp); // these are equal
    CHECK(dc != dc_notcomp);

}
