
#include "storage/DBStorage.h"
#include "DBDerivedBind.h"
#include <sstream>
#include <vector>
#include <string>
#include <iostream>
#include <future>

#ifdef HAVE_FIREBIRD
#include "storage/ibpp/ibpp.h"
#endif
#ifdef HAVE_POSTGRES
#include <libpq-fe.h>
#include <pqxx/pqxx>
#endif

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
//
//-------------------------------------------------------
// DBStorageConstructor test group
//-------------------------------------------------------

TEST_GROUP(DBStorageConstructor) {
	//Storage::DatabaseConnection
    const std::string ServerName{"localhost"};
	std::string desc{"Module DBStorage 1.0 - September 6, 2016"};
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
	"<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
	"  <database-statements>\n"
	"      <statement type=\"select\">\n"
	"        <field name=\"StatuteID\" type=\"integer\"/>\n"
	"        <field name=\"Title\" type=\"string\"/>\n"
	"        <field name=\"Quota\" type=\"integer\"/>\n"
	"      </statement>\n"
	"  </database-statements>\n"
	"</table>\n"
    "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field name=\"ID\" type=\"integer\"/>\n"
    "        <field name=\"firstname\" type=\"string\"/>\n"
    "        <field name=\"surname\" type=\"string\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</table>\n"
    "</module-dbstorage>\n"};
    void setup() {

    }

};

TEST(DBStorageConstructor, ConstructorWithXml) {

    CHECK(dbStorage.isLoggedUser() == false);

}

TEST(DBStorageConstructor, AssignmentWithXml) {

    Storage::DBStorage &dbStorage2 = dbStorage;
    CHECK(dbStorage2.isLoggedUser() == false);

    // Login a known test user which uses ConnectionType::TEST
#ifdef HAVE_FIREBIRD
    dbStorage2.loginUser("TEST", "pass");
    CHECK(true == dbStorage2.isLoggedUser());

    // Able to read if known table name ( as specifed in config xml )
    Storage::DBRetrieveMap where;
    std::vector<Module::Statute> list = dbStorage2.doRead<Module::Statute>(where, {});
    LONGS_EQUAL(3, list.size());
#endif

}

TEST(DBStorageConstructor, DeconstructorWithXml) {

    Storage::DBStorage* dbStorage2 = new Storage::DBStorage(desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    	    "<module-dbstorage>\n"
    	    "  <database>\n"
    	    "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    	    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    	    "  </database>\n"
    		"<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
    		"  <database-statements>\n"
    		"      <statement type=\"select\">\n"
    		"        <field name=\"StatuteID\" type=\"integer\"/>\n"
    		"        <field name=\"Title\" type=\"string\"/>\n"
    		"        <field name=\"Quota\" type=\"integer\"/>\n"
    		"      </statement>\n"
    		"  </database-statements>\n"
    		"</table>\n"
    	    "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
    	    "  <database-statements>\n"
    	    "      <statement type=\"select\">\n"
    	    "        <field name=\"ID\" type=\"integer\"/>\n"
    	    "        <field name=\"firstname\" type=\"string\"/>\n"
    	    "        <field name=\"surname\" type=\"string\"/>\n"
    	    "      </statement>\n"
    	    "  </database-statements>\n"
    	    "</table>\n"
    	    "</module-dbstorage>\n");
    CHECK(dbStorage2->isLoggedUser() == false);
    delete dbStorage2;

}

//
//-------------------------------------------------------
// DBStorageAccessor test group
//-------------------------------------------------------

TEST_GROUP(DBStorageAccessor) {
    const std::string ServerName{"localhost"};
	std::string desc{"Module DBStorage 1.0 - September 6, 2016"};
	std::vector<Module::Statute> source1;
    void setup() {

    }

};

TEST(DBStorageAccessor, DBDriverAccessor) {

}

#ifdef HAVE_FIREBIRD
TEST(DBStorageAccessor, LoginUserAccessor) {

    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
    "<table entity=\"Module::Statute\" name=\"HERO\" key=\"ID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field name=\"ID\" type=\"integer\"/>\n"
    "        <field name=\"statute\" type=\"string\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</table>\n"
    "<table entity=\"Module::Person\" name=\"PERSON\" key=\"ID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field name=\"ID\" type=\"integer\"/>\n"
    "        <field name=\"firstname\" type=\"string\"/>\n"
    "        <field name=\"surname\" type=\"string\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known user
    auto result = dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());

    // Login unknown user
    result = dbStorage.loginUser("USERINVALID", "pass");
    CHECK(false == dbStorage.isLoggedUser());
    STRCMP_EQUAL(" Your user name and password are not defined. Ask your database administrator to set up a login.", std::get<1>(result).error.c_str());

}

TEST(DBStorageAccessor, IsUserLoggedAccessor) {

	// Use a test database
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
    "</module-dbstorage>\n"};

    CHECK(false == dbStorage.isLoggedUser());

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());

    // Login unknown user and no PROD connection in xml config above and try to use LIVE
    Storage::DBStorageCallReturn result = dbStorage.loginUser("USERINVALID", "pass");
    std::string message = std::get<1>(result).error;
    CHECK(true == dbStorage.isLoggedUser());
    STRCMP_EQUAL(" File role type for LIVE not found.", message.c_str());

    Storage::DBStorage dbStorage2{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
    "</module-dbstorage>\n"};

    CHECK(false == dbStorage2.isLoggedUser());

    // Login a known test user which uses ConnectionType::TEST and no TEST role database
    result = dbStorage2.loginUser("TEST", "pass");
    CHECK(false == dbStorage2.isLoggedUser());
    STRCMP_EQUAL(" File role type for TEST not found.", std::get<1>(result).error.c_str());

}

TEST(DBStorageAccessor, GetErrorsAccessor) {

	// Use a test database
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());

    Storage::DBStorage dbStorageXmlError{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file rol=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
    "</module-dbstorage>\n"};

    Storage::DBStorageCallReturn result = dbStorageXmlError.loginUser("TEST", "pass");
    std::string message = std::get<1>(result).error;

    CHECK(false == dbStorageXmlError.isLoggedUser());
    STRCMP_EQUAL(" File role attribute not found at line 4 File role is not test or prod at line 4 File role type for TEST not found.", message.c_str());

}


TEST(DBStorageAccessor, DoReadAccessor) {

    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"StatuteID\" type=\"integer\"/>\n"
        "        <field name=\"Title\" type=\"string\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    auto callresult = dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());
    Storage::DBRetrieveMap where;
    try {
		std::vector<Module::Statute> result = dbStorage.doRead<Module::Statute>(where, {});
		LONGS_EQUAL(3, result.size());
		std::stringstream sresult;
		for (const Module::Statute& s : result ) {
			sresult << s.getTitle() << " ";
		}
		STRCMP_EQUAL("KNight Cavalry Warrior ", sresult.str().c_str());
    } catch (Storage::StorageException &e) {
    	STRCMP_EQUAL("" , e.what());
    }


    Storage::DBStorage dbStorage2{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Statute\" name=\"STATUTE\" key=\"StatuteID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"StatuteID\" type=\"integer\"/>\n"
        "        <field name=\"Title\" type=\"string\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    callresult = dbStorage2.loginUser("TEST", "pass");
    CHECK(true == dbStorage2.isLoggedUser());
    try {
		std::vector<Module::Statute> result2 = dbStorage2.doRead<Module::Statute>(where, {});
	} catch (Storage::StorageException &e) {
		STRCMP_EQUAL("Read Statute unable to read SELECT configuration." , e.what());
	}

}

TEST(DBStorageAccessor, GetDataAccessor) {
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"StatuteID\" type=\"integer\"/>\n"
        "        <field name=\"Title\" type=\"string\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());

    double fraction_done;
    std::string message;
    dbStorage.get_data(&fraction_done, &message);
    DOUBLES_EQUAL(0.0, fraction_done, 0.01);
    STRCMP_EQUAL("", message.c_str());

}

TEST(DBStorageAccessor, DoReadAsyncAccessor) {
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"StatuteID\" type=\"integer\"/>\n"
        "        <field name=\"Title\" type=\"string\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());
    Storage::DBRetrieveMap where;
    Storage::DBRetrieveMap parms;

	std::future<std::vector<Module::Statute>> result(
			std::async(
					std::launch::async, &Storage::DBStorage::doRead<Module::Statute>, &dbStorage, where, parms));
	auto list = result.get(); // wait for for async to finish
	LONGS_EQUAL(3, list.size());
}

TEST(DBStorageAccessor, StopWorkAccessor) {
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"StatuteID\" type=\"integer\"/>\n"
        "        <field name=\"Title\" type=\"string\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());
    dbStorage.stop_work();

}

TEST(DBStorageAccessor, DoReadRetrieveAccessor) {

    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
    	"<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
    	"  <database-statements>\n"
    	"      <statement type=\"select\">\n"
    	"        <field name=\"StatuteID\" type=\"integer\"/>\n"
    	"        <field name=\"Title\" type=\"string\"/>\n"
    	"        <field name=\"Quota\" type=\"integer\"/>\n"
    	"      </statement>\n"
    	"  </database-statements>\n"
    	"</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());
    // Use known correct column name for SQL
    std::string fld{"StatuteID"};
    int index = 3;
    //dbStorage.where({{fld, index}});

    try {
		auto result = dbStorage.doRead<Module::Statute>({{fld, index}}, {});
		LONGS_EQUAL(1, result.size());
		STRCMP_EQUAL("Cavalry", result.at(0).getTitle().c_str());
    } catch (Storage::StorageException &e) {
    	STRCMP_EQUAL("" , e.what());
    }


    // We can reset where again and redo doRead with new criteria
    fld = "Title";
    std::string search{"KNight"};
    try {
		auto result = dbStorage.doRead<Module::Statute>({{fld, search}}, {});
		LONGS_EQUAL(1, result.size());
		STRCMP_EQUAL("KNight", result.at(0).getTitle().c_str());
    } catch (Storage::StorageException &e) {
    	STRCMP_EQUAL("" , e.what());
    }

}


TEST(DBStorageAccessor, DBStorageAddItem)
{
	Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"StatuteID\" type=\"integer\"/>\n"
        "        <field name=\"Title\" type=\"string\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"ID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"ID\" type=\"integer\"/>\n"
        "        <field name=\"Name\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());

    Module::Statute statute;
    statute.setTitle("Pikeman");

    try {
		Module::Statute new_item = dbStorage.doAddItem<Module::Statute>(statute);
		CHECK(new_item.getStatuteID() > 0);
		for (int i = 0; i < 4; ++i) {
			dbStorage.doAddItem<Module::Statute>(statute);
		}
    } catch (Storage::StorageException &e) {
    	STRCMP_EQUAL("" , e.what());
    }
    try {
		auto result = dbStorage.doRead<Module::Statute>({{"Title", "Pikeman"}}, {});
		LONGS_EQUAL(5, result.size());
		STRCMP_EQUAL("Pikeman", result.at(0).getTitle().c_str());
		// Clean up the add items with doRemoveItem's
		for (Module::Statute statute: result) {
			dbStorage.doRemoveItem<Module::Statute>(statute);
		}
    } catch (Storage::StorageException &e) {
    	STRCMP_EQUAL("" , e.what());
    }


}


TEST(DBStorageAccessor, DBStorageRemoveItem)
{
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"StatuteID\" type=\"integer\"/>\n"
        "        <field name=\"Title\" type=\"string\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"ID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"ID\" type=\"integer\"/>\n"
        "        <field name=\"Name\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    Module::Statute statute;
    statute.setTitle("Archer");

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());

    try {
		dbStorage.doAddItem<Module::Statute>(statute);
		auto result = dbStorage.doRead<Module::Statute>({{"Title", "Archer"}}, {});
		LONGS_EQUAL(1, result.size());
		STRCMP_EQUAL("Archer", result.at(0).getTitle().c_str());
		statute = result.at(0); // Get it to remove it.

		// Remove it now
		dbStorage.doRemoveItem<Module::Statute>(statute);

		// Do it again
		dbStorage.doRemoveItem<Module::Statute>(statute);
    } catch (Storage::StorageException &e) {
    	STRCMP_EQUAL("" , e.what());
    }

}

TEST(DBStorageAccessor, DBStorageUpdateItem)
{
    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"StatuteID\" type=\"integer\"/>\n"
        "        <field name=\"Title\" type=\"string\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"ID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"ID\" type=\"integer\"/>\n"
        "        <field name=\"Name\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());
    // Use known correct column name for SQL
    std::string fld{"StatuteID"};
    int index = 3;
    try {
		auto result = dbStorage.doRead<Module::Statute>({{fld, index}}, {});
		LONGS_EQUAL(1, result.size());
		STRCMP_EQUAL("Cavalry", result.at(0).getTitle().c_str());

		Module::Statute hero = result.at(0);
		Module::Statute orig_hero = result.at(0);
		hero.setTitle("Archer");

		Module::Statute result_item = dbStorage.doUpdateItem<Module::Statute>(orig_hero, hero);
		STRCMP_EQUAL("Archer", result_item.getTitle().c_str());
		auto search = dbStorage.doRead<Module::Statute>({{"Title", "Archer"}}, {});
		LONGS_EQUAL(1, result.size());
		STRCMP_EQUAL("Archer", search.at(0).getTitle().c_str());

		// Change it back by update with original orig_item
		Module::Statute result_item2 = dbStorage.doUpdateItem<Module::Statute>(result_item, orig_hero);
		STRCMP_EQUAL("Cavalry", result_item2.getTitle().c_str());
    } catch (Storage::StorageException &e) {
    	STRCMP_EQUAL("" , e.what());
    }

}


TEST(DBStorageAccessor, GranteeAdminAccessor) {

    const std::string UserNameBilly = "BILLY";
    const std::string Password2 = "pass";

    if (!IBPP::CheckVersion(IBPP::Version)) {
        printf(_("\nThis program got linked to an incompatible version of the IBPP source code.\n"
                "Can't execute safely.\n"));
        LONGS_EQUAL(0, 1);
    }
    try {

        // firebird gsec level with grantee Billy admin
        IBPP::Service svc = IBPP::ServiceFactory(ServerName, UserNameBilly, Password2);
        svc->Connect();
        std::vector<IBPP::User> users;
        svc->GetUsers(users);
        std::stringstream name; // For test comparing result of iter's
        name.str(std::string());
        std::for_each(users.begin(), users.end(), [&name](IBPP::User u) {
            name << u.username;
            name << " ";
        });
        STRCMP_EQUAL(std::string("BILLY ").c_str(), name.str().c_str());
        LONGS_EQUAL(1, users.size());

    } catch (IBPP::Exception &e) {
        std::cout << e.what() << "\n";
        LONGS_EQUAL(0, 1);
    } catch (std::exception &e) {
        std::cout << e.what() << "\n";
        LONGS_EQUAL(0, 1);
    }

}

TEST(DBStorageAccessor, GranteeAdminCreateUser) {
    const std::string UserNameBilly = "BILLY";
    const std::string Password2 = "pass";
    const std::string role("RDB$ADMIN");
    const std::string charset("ISO8859_1");
    const std::string createparam("");

    if (!IBPP::CheckVersion(IBPP::Version)) {
        printf(_("\nThis program got linked to an incompatible version of the IBPP source code.\n"
                "Can't execute safely.\n"));
        LONGS_EQUAL(0, 1);
    }
    try {
        const char *DbName = "/home/rodney/databases/utest.fdb";
        IBPP::Database db1;
        IBPP::Transaction tr1;
        IBPP::Statement st1;

        db1 = IBPP::DatabaseFactory(ServerName, DbName, UserNameBilly, Password2, role, charset, createparam);
        db1->Connect();

        tr1 = IBPP::TransactionFactory(db1);
        tr1->Start();

        st1 = IBPP::StatementFactory(db1, tr1);
        st1->ExecuteImmediate("create user john password 'fYe_3Ksw' firstname 'John' lastname 'Doe'");
        tr1->CommitRetain();
        st1->ExecuteImmediate("GRANT GUEST TO USER JOHN"); // So Billy admin can check privileges to new user
        tr1->CommitRetain();

        std::string sql("SELECT r.RDB$USER, r.RDB$GRANTOR, r.RDB$PRIVILEGE, "
                " r.RDB$RELATION_NAME, t.RDB$TYPE_NAME"
                " FROM RDB$USER_PRIVILEGES r"
                " JOIN RDB$TYPES t ON t.RDB$TYPE = r.RDB$OBJECT_TYPE"
                " WHERE r.RDB$USER = 'JOHN' AND t.RDB$FIELD_NAME = 'RDB$OBJECT_TYPE'"
                " UNION "
                " SELECT r.RDB$USER, r.RDB$GRANTOR, r.RDB$PRIVILEGE,"
                " r.RDB$RELATION_NAME, t.RDB$TYPE_NAME"
                " FROM RDB$USER_PRIVILEGES r"
                " JOIN RDB$TYPES t ON t.RDB$TYPE = r.RDB$USER_TYPE"
                " WHERE r.RDB$USER = 'JOHN' AND t.RDB$FIELD_NAME = 'RDB$OBJECT_TYPE' ");

        st1->Prepare(sql);
        st1->Execute();

        std::stringstream name; // For test comparing result of iter's
        name.str(std::string());
        while (st1->Fetch()) {
            std::string user_role, grantor, privilege, rel_name;
            std::string user_type;
            st1->Get(1, user_role);
            st1->Get(2, grantor);
            st1->Get(3, privilege);
            st1->Get(4, rel_name);
            st1->Get(5, user_type);
            name << user_role << " " <<
                    grantor << " " <<
                    privilege << " " <<
                    rel_name << " " <<
                    user_type;
            name << "|";
        }

        db1->Disconnect(); // Added User

        STRCMP_EQUAL(std::string("JOHN                            BILLY                           M      GUEST                           ROLE                           |JOHN                            BILLY                           M      GUEST                           USER                           |").c_str(), name.str().c_str());

        db1 = IBPP::DatabaseFactory(ServerName, DbName, UserNameBilly, Password2, role, charset, createparam);
        db1->Connect(); // Drop User john

        tr1 = IBPP::TransactionFactory(db1);
        tr1->Start();

        st1 = IBPP::StatementFactory(db1, tr1);
        st1->ExecuteImmediate("REVOKE GUEST FROM USER JOHN"); // So Billy admin can check privileges to new user
        tr1->CommitRetain();
        st1->ExecuteImmediate("drop user john");
        tr1->CommitRetain();

        st1->Prepare(sql);
        st1->Execute();

        // Retest For test comparing result of iter's
        name.str(std::string());
        while (st1->Fetch()) {
            std::string user_role, grantor, privilege, rel_name;
            std::string user_type;
            st1->Get(1, user_role);
            st1->Get(2, grantor);
            st1->Get(3, privilege);
            st1->Get(4, rel_name);
            st1->Get(5, user_type);
            name << user_role << " " <<
                    grantor << " " <<
                    privilege << " " <<
                    rel_name << " " <<
                    user_type;
            name << "|";
        }

        STRCMP_EQUAL(std::string("").c_str(), name.str().c_str());

        db1->Disconnect(); // Dropped User

    } catch (IBPP::Exception &e) {
        std::cout << e.what() << "\n";
        LONGS_EQUAL(0, 1);
    } catch (std::exception &e) {
        std::cout << e.what() << "\n";
        LONGS_EQUAL(0, 1);
    }

}
#endif

TEST(DBStorageAccessor, GranteeAdminRoleGuest) {
    const std::string UserNameBilly = "BILLY";
    const std::string Password2 = "pass";
    const std::string role("RDB$ADMIN");
    const std::string charset("ISO8859_1");
    const std::string createparam("");

#ifdef HAVE_FIREBIRD
    if (!IBPP::CheckVersion(IBPP::Version)) {
        printf(_("\nThis program got linked to an incompatible version of the IBPP source code.\n"
                "Can't execute safely.\n"));
        LONGS_EQUAL(0, 1);
    }
    try {
        const char *DbName = "/home/rodney/databases/utest.fdb";
        IBPP::Database db1;
        IBPP::Transaction tr1;
        IBPP::Statement st1;

        db1 = IBPP::DatabaseFactory(ServerName, DbName, UserNameBilly, Password2, role, charset, createparam);
        db1->Connect();

        tr1 = IBPP::TransactionFactory(db1);
        tr1->Start();

        st1 = IBPP::StatementFactory(db1, tr1);

        std::string sql("SELECT r.RDB$USER, r.RDB$GRANTOR, r.RDB$PRIVILEGE, r.RDB$RELATION_NAME, t.RDB$TYPE_NAME"
                " FROM RDB$USER_PRIVILEGES r "
                " LEFT JOIN RDB$TYPES t ON r.RDB$USER_TYPE = t.RDB$TYPE"
                " WHERE r.RDB$USER = 'GUEST' AND t.RDB$FIELD_NAME = 'RDB$OBJECT_TYPE'"
                " ORDER BY r.RDB$USER, r.RDB$RELATION_NAME");

        st1->Prepare(sql);
        st1->Execute();

        std::stringstream name; // For test comparing result of iter's
        name.str(std::string());
        while (st1->Fetch()) {
            std::string user_role, grantor, privilege, rel_name;
            std::string user_type;
            st1->Get(1, user_role);
            st1->Get(2, grantor);
            st1->Get(3, privilege);
            st1->Get(4, rel_name);
            st1->Get(5, user_type);
            name << user_role << " " <<
                    grantor << " " <<
                    privilege << " " <<
                    rel_name << " " <<
                    user_type;
            name << " |";
        }
        db1->Disconnect(); // Added User

        STRCMP_EQUAL(std::string("GUEST                           BILLY                           S      CHEST                           ROLE                            |GUEST                           BILLY                           S      HERO                            ROLE                            |GUEST                           BILLY                           S      PERSON                          ROLE                            |").c_str(), name.str().c_str());

    } catch (IBPP::Exception &e) {
        std::cout << e.what() << "\n";
        LONGS_EQUAL(0, 1);
    } catch (std::exception &e) {
        std::cout << e.what() << "\n";
        LONGS_EQUAL(0, 1);
    }
#endif

#ifdef HAVE_POSTGRES

    std::stringstream conn_detail;
    conn_detail << "dbname=listbook_test user=test password=password hostaddr=";
    conn_detail << ServerName;
    conn_detail << " port=5432";
    int lib_ver = PQlibVersion();
    std::printf("Version of libpq: %d\n", lib_ver);

    PGconn *conn = PQconnectdb(conn_detail.str().c_str());

    if (PQstatus(conn) == CONNECTION_BAD) {

        std::cout << "Connection to database failed: " << PQerrorMessage(conn) << std::endl;
        PQfinish(conn);
        exit(1);
    }

    int ver = PQserverVersion(conn);
    std::printf("Server version: %d\n", ver);

    PGresult *res;
    int rec_count;
    int row;
    int col;
    res = PQexec(conn,
            "update person set firstname=\'Barn\' where id=3");
    PQclear(res); // always clear after res is done and finiseh with

    res = PQexec(conn,
            "select firstname,surname from person order by id");

    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        puts("We did not get any data!");
        PQclear(res); // always clear after res is done and finiseh with
        PQfinish(conn);
        exit(1);
    }

    rec_count = PQntuples(res);

    std::printf("We received %d records.\n", rec_count);
    puts("==========================");

    for (row = 0; row < rec_count; row++) {
        for (col = 0; col < 2; col++) {
            printf("%s\t", PQgetvalue(res, row, col));
        }
        puts("");
    }

    puts("==========================");

    PQclear(res);

    PQfinish(conn);

    std::string sql;
    try {
        pqxx::connection C(conn_detail.str());
        if (C.is_open()) {
            std::cout << "Opened database successfully: " << C.dbname() << std::endl;
        } else {
            std::cout << "Can't open database" << std::endl;
        }
        /* Create SQL statement */
        sql = "SELECT * from person";

        /* Create a non-transactional object. */
        pqxx::nontransaction N(C);

        /* Execute SQL query */
        pqxx::result R(N.exec(sql.c_str()));

        /* List down all the records */
        for (pqxx::result::const_iterator c = R.begin(); c != R.end(); ++c) {
            std::cout << "ID = " << c[0].as<int>();
            std::cout << " First name = " << c[1].as<std::string>();
            std::cout << " Surname = " << c[2].as<std::string>();
            std::cout << std::endl;
        }
        C.disconnect();
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
#endif
}

#ifdef HAVE_FIREBIRD

//
//-------------------------------------------------------
// DBStorageAccessor test group
//-------------------------------------------------------

TEST_GROUP(DBStorageReadAccessor) {
    const std::string ServerName{"localhost"};
	std::string desc{"Module DBStorage 1.0 - September 6, 2016"};
	std::vector<Module::Statute> source1;
    void setup() {

    }

};

TEST(DBStorageReadAccessor, DoReadAccessor) {

    Storage::DBStorage dbStorage{desc, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/listbook_test.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Inventory\" name=\"Inventory\" key=\"InventoryID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"InventoryID\" type=\"integer\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "        <field name=\"Name\" type=\"string\"/>\n"
        "        <field name=\"ChestID\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
        "<table entity=\"Module::ChestItems\" name=\"ChestItems\" key=\"ChestItemID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"ChestItemID\" type=\"integer\"/>\n"
        "        <field name=\"InventoryID\" type=\"integer\"/>\n"
        "        <field name=\"ChestID\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());

    // We cause exception as ChestID is not a column in Inventory table yet it is put into
    // the config above.
    try {
    	std::vector<Module::Inventory> result = dbStorage.doRead<Module::Inventory>({},{});
    } catch (Storage::StorageException &e) {
    	STRCMP_EQUAL("Read store Inventory database exception and unable to read SELECT InventoryID, Quota, Name, ChestID FROM Inventory ", e.what());
    }

}

TEST(DBStorageReadAccessor, DoReadStoredProcedure) {

    Storage::DBStorage dbStorage{"Module DBStorage 1.0 - September 6, 2016",
    	"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/listbook_test.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
        "<table entity=\"Module::Inventory\" name=\"Inventory\" key=\"InventoryID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"InventoryID\" type=\"integer\"/>\n"
        "        <field name=\"Quota\" type=\"integer\"/>\n"
        "        <field name=\"Name\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
        "<procedure entity=\"Module::InventoryCount\" name=\"InventoryCount\" key=\"InventoryID\">\n"
        "  <database-statements>\n"
		"      <statement type=\"select\">\n"
		"        <parameters>\n"
		"          <parameter name=\"InventoryID\" type=\"integer\"/>\n"
		"        </parameters>\n"
	    "        <fields>\n"
		"          <field name=\"InventoryID\" type=\"integer\"/>\n"
		"          <field name=\"Quota\" type=\"integer\"/>\n"
		"          <field name=\"Name\" type=\"string\"/>\n"
		"          <field name=\"CountItem\" type=\"integer\"/>\n"
        "        </fields>\n"
		"      </statement>\n"
        "  </database-statements>\n"
        "</procedure>\n"
        "<table entity=\"Module::ChestItems\" name=\"ChestItems\" key=\"ChestItemID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"ChestItemID\" type=\"integer\"/>\n"
        "        <field name=\"InventoryID\" type=\"integer\"/>\n"
        "        <field name=\"ChestID\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
        "<table entity=\"Module::Chest\" name=\"Chest\" key=\"ChestID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"ChestID\" type=\"integer\"/>\n"
        "        <field name=\"Name\" type=\"string\"/>\n"
        "        <field name=\"PersonID\" type=\"integer\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
    "</module-dbstorage>\n"};

    // Login a known test user which uses ConnectionType::TEST
    dbStorage.loginUser("TEST", "pass");
    CHECK(true == dbStorage.isLoggedUser());
    std::vector<Module::InventoryCount> result = dbStorage.doRead<Module::InventoryCount>({},{});
    LONGS_EQUAL(0, result.size());

    // Need to set param for procedures
    auto result2 = dbStorage.doRead<Module::InventoryCount>({}, {{"inv_id", 2}});
    LONGS_EQUAL(1, result2.size());

}

#endif


