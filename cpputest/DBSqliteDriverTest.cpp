/*
 * DBSqliteDriverTest.cpp
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#include "storage/DBSqliteDriver.h"
#include "storage/DBDriver.h"
#include "storage/DBResult.h"
#include "storage/DBStorage.h"
#include "DBStatementFixture.h"
#include <unordered_map>
#include <memory>
#include <vector>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
//
//-------------------------------------------------------
// DBSqliteDriverConstructor test group
//-------------------------------------------------------
using Storage::DBStorage;
using Storage::Model;
using Storage::Domain::RowItem;
using Storage::Domain::Schema;
using Storage::Driver::CreateKind;
using Storage::Driver::DatabaseConnection;
using Storage::Driver::DBDriver;
using Storage::Driver::DBResult;
using Storage::Driver::DBSqliteDriver;
using Storage::Driver::DBStatement;
using Storage::Driver::FieldType;
using Storage::Driver::StorageException;

TEST_GROUP(DBSqliteDriverConstructor)
{
  DatabaseConnection dbconnect{"Sqlite", "localhost", "test.db", "ISO8859_1"};
  std::shared_ptr<DBDriver> dr{};
  void setup()
  {
    dr = DBDriver::CreateDBDriver(dbconnect);
  }
  void teardown() {}
};

TEST(DBSqliteDriverConstructor, Constructor)
{
  STRCMP_EQUAL(std::string("Sqlite").c_str(), dbconnect.getDriver().c_str());
  STRCMP_EQUAL(std::string("localhost").c_str(), dbconnect.getServer().c_str());
  STRCMP_EQUAL(std::string("test.db").c_str(), dbconnect.getDBFile().c_str());
  STRCMP_EQUAL(std::string("ISO8859_1").c_str(), dbconnect.getCharSet().c_str());
  try
  {
    DBSqliteDriver fbDriver{dbconnect};
    CHECK(false == fbDriver.hasTransactionStarted());
  }
  catch (std::exception &e)
  {
    STRCMP_EQUAL("", e.what());
  }
}

//
//-------------------------------------------------------
// DBSqliteDriverAccessor test group
//-------------------------------------------------------

TEST_GROUP(DBSqliteDriverAccessor)
{
  DatabaseConnection dbconnect{"Sqlite", "localhost", "test.db", "ISO8859_1"};
  std::unique_ptr<DBDriver> dr{};
  DBStorage dbStorage{dbconnect};
  std::vector<std::string> movies{};
  Schema schemaMovie{
      {{"M_ID", FieldType::PK_INT},
       {"Movie", FieldType::STRING}}};
  MovieAttrs MA;

  void setup()
  {
    movies.push_back("One");
    movies.push_back("2One");

    if (dbconnect.getDriver().compare("Sqlite") == 0)
    {
      dr = std::unique_ptr<DBDriver>(new DBSqliteDriver{dbconnect});
      schemaMovie = Schema(
          {{"M_ID", FieldType::PK_INT},
           {"Movie", FieldType::STRING}});
      dbStorage.model(MA.T.Movies, schemaMovie, CreateKind::DROP_CREATE);

      create_test_db(dbconnect, "TEST", "pass", "QUEST", movies);
    }
  }

  void teardown() {}
};

TEST(DBSqliteDriverAccessor, ConstructorAndDeconstructor)
{
  STRCMP_EQUAL(std::string("Sqlite").c_str(), dbconnect.getDriver().c_str());
  STRCMP_EQUAL(std::string("localhost").c_str(), dbconnect.getServer().c_str());
  STRCMP_EQUAL(std::string("test.db").c_str(), dbconnect.getDBFile().c_str());
  STRCMP_EQUAL(std::string("ISO8859_1").c_str(), dbconnect.getCharSet().c_str());
  auto fields = schemaMovie.rowFields();
  auto row = RowItem(fields);

  std::unique_ptr<DBStatement> pSt;
  std::unique_ptr<DBResult> pRt;

  DBSqliteDriver fbDriver{dbconnect};
  CHECK(false == fbDriver.hasTransactionStarted());
  //fbDriver.BeginTransaction();
  pSt = fbDriver.CreateStatement(schemaMovie, "SELECT * FROM Movies");
  CHECK(false == fbDriver.hasTransactionStarted());
  pRt = pSt->Execute();
  //fbDriver.Commit();
  try
  {
    auto fields = schemaMovie.rowFields();
    auto row = RowItem(fields);

    CHECK((pRt->Fetch().first) == true);
    CHECK((pRt->Fetch().first) == true);
    CHECK((pRt->Fetch().first) == false);
    CHECK((pRt->Fetch().first) == false);
    pRt->Fetch(); // Now will be no-op - false and no  op made
  }
  catch (std::exception &e)
  {
    std::cerr << e.what() << "\n";
    LONGS_EQUAL(1, 0);
  }
}

TEST(DBSqliteDriverAccessor, CreateStatementSelectAccessor)
{
  try
  {
    auto fields = schemaMovie.rowFields();
    auto row = RowItem(fields);

    CHECK(false == dr->hasTransactionStarted());

    dr->BeginTransaction();
    CHECK(true == dr->hasTransactionStarted());
    auto st = dr->CreateStatement(schemaMovie, "SELECT * FROM MOVIES");
    auto result = st->Execute();

    auto st1 = dr->CreateStatement(schemaMovie, "SELECT Movie FROM MOVIES");
    CHECK(true == dr->hasTransactionStarted());
    st1->Execute();
    dr->Commit();
    CHECK(false == dr->hasTransactionStarted());

    dr->BeginTransaction();
    auto st2 = dr->CreateStatement(schemaMovie, "SELECT M_ID FROM MOVIES");
    CHECK(true == dr->hasTransactionStarted());
    st2->Execute();
    // Not committed yet
    CHECK(true == dr->hasTransactionStarted());
    dr->Commit();
    CHECK(false == dr->hasTransactionStarted());
  }
  catch (std::exception &e)
  {
    LONGS_EQUAL(1, 0);
  }

  try
  {
    auto fields = schemaMovie.rowFields();
    auto row = RowItem(fields);

    CHECK(false == dr->hasTransactionStarted());

    dr->BeginTransaction();
    auto st = dr->CreateStatement(schemaMovie, "SELECT NoMovie FROM Movies");
    st->Execute();
    LONGS_EQUAL(1, 0);
  }
  catch (std::exception &e)
  {
    STRCMP_CONTAINS("DBSqliteStatement::Execute prepare statement on database exception  no such column: NoMovie", e.what());
    CHECK(true == dr->hasTransactionStarted());
  }
  dr->Commit();
}

TEST(DBSqliteDriverAccessor, CreateStatementInsertAccessor)
{
  auto fields = schemaMovie.rowFields();
  auto new_item = RowItem(fields);

  new_item.setField(0, 100);
  new_item.setField(1, "Swordsman");
  auto sqlinsert = dr->getSQL("Movies", new_item, StatementType::INSERT);

  STRCMP_EQUAL(
      "INSERT INTO Movies(M_ID, Movie) VALUES (100, 'Swordsman') RETURNING M_ID, Movie",
      sqlinsert.c_str());

  try
  {
    auto fields = schemaMovie.rowFields();
    auto row = RowItem(fields);
    CHECK(false == dr->hasTransactionStarted());
    dr->BeginTransaction();
    dr->CreateStatement(schemaMovie, sqlinsert);
    CHECK(true == dr->hasTransactionStarted());
    dr->Commit();
    CHECK(false == dr->hasTransactionStarted());
  }
  catch (std::exception &e)
  {
    LONGS_EQUAL(1, 0);
  }

  try
  {
    auto fields = schemaMovie.rowFields();
    auto row = RowItem(fields);
    CHECK(false == dr->hasTransactionStarted());

    // Wont let us do a wrong statement...
    dr->BeginTransaction();
    auto st = dr->CreateStatement(schemaMovie, "INSERT INTO MOVIES(Movie) VALUES (100,'Swordsman') RETURNING M_ID, Movie");
    st->Execute();
    LONGS_EQUAL(1, 0);
  }
  catch (std::exception &e)
  {
    STRCMP_CONTAINS("DBSqliteStatement::Execute prepare statement on database exception  2 values for 1 columns", e.what());
  }
  CHECK(true == dr->hasTransactionStarted());
  dr->Commit();

  try
  {
    auto fields = schemaMovie.rowFields();
    auto row = RowItem(fields);
    CHECK(false == dr->hasTransactionStarted());
    dr->BeginTransaction();
    dr->CreateStatement(schemaMovie, sqlinsert);
    CHECK(true == dr->hasTransactionStarted());
    dr->Commit();
  }
  catch (std::exception &e)
  {
    LONGS_EQUAL(1, 0);
  }
}

TEST(DBSqliteDriverAccessor, ExecuteSelectAccessor)
{
  try
  {
    CHECK(false == dr->hasTransactionStarted());

    Schema schemaCount({{"COUNT", FieldType::INTEGER}});
    dr->BeginTransaction();
    auto st = dr->CreateStatement("SELECT COUNT(*) FROM Movies");
    CHECK(true == dr->hasTransactionStarted());
    auto result = st->Execute();

    result->useSchema(&schemaCount);
    auto fetch = result->Fetch();
    CHECK(true == fetch.first);
    LONGS_EQUAL(1, fetch.second->TotalFields());
    dr->Commit();
  }
  catch (std::exception &e)
  {
    STRCMP_EQUAL("", e.what());
  }

  try
  {
    CHECK(false == dr->hasTransactionStarted());
    Schema schemaCount({{"COUNT", FieldType::INTEGER}});
    dr->BeginTransaction();
    auto st = dr->CreateStatement("SELECT COUNT(*) FROM Movies");
    CHECK(true == dr->hasTransactionStarted());
    auto result = st->Execute();

    result->useSchema(&schemaCount);
    auto fetch = result->Fetch();
    CHECK(true == fetch.first);
    int count;
    fetch.second->getField(0, count);
    LONGS_EQUAL(2, count);

    dr->Commit();
  }
  catch (std::exception &e)
  {
    STRCMP_EQUAL("", e.what());
  }
}

TEST(DBSqliteDriverAccessor, ExecuteReturnInsertAndExecuteRemoveAccessor)
{
  auto fields = schemaMovie.rowFields();
  auto new_item = RowItem(fields);
  new_item.setField(0, 100);
  new_item.setField(1, "Swordsman");
  auto sqlinsert = dr->getSQL("Movies", new_item, StatementType::INSERT);
  STRCMP_EQUAL("INSERT INTO Movies(M_ID, Movie) VALUES (100, 'Swordsman') RETURNING M_ID, Movie", sqlinsert.c_str());

  //RowItem remove_item = dbStatementMovies.createRowItem();
  auto remove_item = RowItem(fields);
  try
  {

    CHECK(false == dr->hasTransactionStarted());

    dr->BeginTransaction();
    auto st = dr->CreateStatement(schemaMovie, sqlinsert); //, new_item);
    CHECK(true == dr->hasTransactionStarted());

    // Execute Return Insert which fills the new_item we setup with create statement
    // RowItem row = st->ExecuteReturn();
    // std::string value;
    //
    // row.getField(1, value);
    // STRCMP_EQUAL("Swordsman", value.c_str());
    dr->Commit();

    // remove_item = row;
    // std::stringstream sqldelete;
    // sqldelete << dbStatementMovies.getSQL(StatementType::REMOVE, remove_item)
    //           << dbStatementMovies.getWhereSQL(StatementType::REMOVE, remove_item);

    // CHECK(false == dr->hasTransactionStarted());
    // auto st2 = dr->CreateStatement(sqldelete.str());
    // CHECK(true == dr->hasTransactionStarted());

    // // Execute Remove, note no need to get returned DBResult
    // st2->Execute();
    // CHECK(true == dr->hasTransactionStarted());
    // dr->Commit();
    // CHECK(false == dr->hasTransactionStarted());
    // STRCMP_CONTAINS("DELETE FROM Movies  WHERE M_ID =", sqldelete.str().c_str());
    // STRCMP_CONTAINS("AND Movie = 'Swordsman'", sqldelete.str().c_str());
  }
  catch (std::exception &e)
  {
    STRCMP_EQUAL("", e.what());
  }

  try
  {
    auto fields = schemaMovie.rowFields();
    auto row = RowItem(fields);

    // Not new Connect but committed so no transaction is still going
    CHECK(false == dr->hasTransactionStarted());

    dr->BeginTransaction();
    auto st = dr->CreateStatement(schemaMovie, "SELECT M_ID, Movie FROM Movies");
    CHECK(true == dr->hasTransactionStarted());
    auto result = st->Execute();

    auto fetch = result->Fetch();
    CHECK(true == fetch.first);
    LONGS_EQUAL(2, row.TotalFields());
    dr->Commit();
  }
  catch (std::exception &e)
  {
    STRCMP_EQUAL("", e.what());
  }
}

// TEST(DBSqliteDriverAccessor, ExecuteUpdateAccessor)
// {
//   STRCMP_EQUAL("Movies: 1. [M_ID INTEGER STR: INT:0 ] 2. [Movie STRING STR: INT:0 ]",
//                dbStatementMovies.StatementDetail().c_str());
//   RowItem new_item = dbStatementMovies.createRowItem();
//   new_item.setField(0, 100);
//   new_item.setField(1, "Swordsman");
//   std::string sqlinsert = dbStatementMovies.getSQL(StatementType::INSERT, new_item);
//   STRCMP_EQUAL("INSERT INTO Movies(M_ID, Movie) VALUES (100, 'Swordsman') RETURNING M_ID, Movie", sqlinsert.c_str());

//   std::stringstream sql_update;
//   RowItem row;

//   /// Add an item to test updates
//   try
//   {
//     dr->Connect(UserName, UserPassword, UserRole);
//
//     CHECK(false == dr->hasTransactionStarted());

//     auto st = dr->CreateStatement(sqlinsert, new_item);
//     CHECK(true == dr->hasTransactionStarted());

//     // Execute Return for three Insert
//     row = st->ExecuteReturn();
//     std::string value;
//
//     row.getField(1, value);
//     STRCMP_EQUAL("Swordsman", value.c_str());
//     dr->Commit(); // Commit insert..
//   }
//   catch (std::exception &e)
//   {
//     STRCMP_EQUAL("", e.what());
//   }

//   RowFields upfields;
//   FieldDetail fd{"Movie", "Scout"};
//   upfields.insert(std::make_pair(0, fd));
//   RowItem rowp(upfields);

//   try
//   {
//     // 2) Update a collection of statutes just added
//     CHECK(false == dr->hasTransactionStarted());
//     sql_update << dbStatementMovies.getSQL(StatementType::UPDATE, rowp)
//                << " WHERE Movie = 'Swordsman' ";
//     STRCMP_EQUAL("UPDATE Movies SET Movie = 'Scout'  WHERE Movie = 'Swordsman' ", sql_update.str().c_str());
//     auto st = dr->CreateStatement(sql_update.str());
//     st->Execute();
//     dr->Commit(); // Finish our update and commit transaction (not started = true)
//   }
//   catch (std::exception &e)
//   {
//     STRCMP_EQUAL("", e.what());
//   }

//   // Cleanup the added item with Delete item.
//   try
//   {
//     std::string sqld("DELETE FROM Movies  WHERE Movie ='Scout'");
//     CHECK(false == dr->hasTransactionStarted());
//     auto st = dr->CreateStatement(sqld);
//     CHECK(true == dr->hasTransactionStarted());

//     // Execute Remove, note no need to get returned DBResult
//     st->Execute();
//     CHECK(true == dr->hasTransactionStarted());
//     dr->Commit();
//     CHECK(false == dr->hasTransactionStarted());
//   }
//   catch (std::exception &e)
//   {
//     STRCMP_EQUAL("", e.what());
//   }
// }

// TEST(DBSqliteDriverAccessor, ExecuteUpdateItemAccessor)
// {
//   STRCMP_EQUAL("Movies: 1. [M_ID INTEGER STR: INT:0 ] 2. [Movie STRING STR: INT:0 ]",
//                dbStatementMovies.StatementDetail().c_str());
//   RowItem new_item = dbStatementMovies.createRowItem();
//   new_item.setField(0, 100);
//   new_item.setField(1, "Swordsman");
//   std::string sqlinsert = dbStatementMovies.getSQL(StatementType::INSERT, new_item);
//   STRCMP_EQUAL("INSERT INTO Movies(M_ID, Movie) VALUES (100, 'Swordsman') RETURNING M_ID, Movie", sqlinsert.c_str());

//   std::stringstream sql_delete;
//   std::stringstream sql_update;

//   RowItem orig_item = dbStatementMovies.createRowItem();
//   RowItem update_item = dbStatementMovies.createRowItem();
//   RowItem row;

//   /// Add an item to test updates
//   try
//   {
//     dr->Connect(UserName, UserPassword, UserRole);
//
//     CHECK(false == dr->hasTransactionStarted());

//     // For ExecuteReturn we send our new_item object to Prepare..
//     auto st = dr->CreateStatement(sqlinsert, new_item);
//     CHECK(true == dr->hasTransactionStarted());

//     // Execute Return Insert 1) Insert new hero to edit
//     row = st->ExecuteReturn();
//     std::string value;
//
//     row.getField(1, value);
//     STRCMP_EQUAL("Swordsman", value.c_str());
//     dr->Commit(); // Commit insert..
//   }
//   catch (std::exception &e)
//   {
//     STRCMP_EQUAL("", e.what());
//   }

//   orig_item = row;
//   update_item = orig_item;
//   update_item.setField(1, "Scout");

//   try
//   {
//     // 2) Update the hero just added
//     CHECK(false == dr->hasTransactionStarted());
//     sql_update << dbStatementMovies.getSQL(StatementType::UPDATE, update_item)
//                << dbStatementMovies.getWhereSQL(StatementType::SELECT, orig_item);
//     STRCMP_CONTAINS("UPDATE Movies SET M_ID =", sql_update.str().c_str());
//     STRCMP_CONTAINS("AND Movie = 'Swordsman'", sql_update.str().c_str());

//     auto st = dr->CreateStatement(sql_update.str()); //, update_item);
//     st->Execute();
//     CHECK(true == dr->hasTransactionStarted());
//     dr->Commit(); // Finish our update and commit transaction (not started = true)
//   }
//   catch (std::exception &e)
//   {
//     STRCMP_EQUAL("", e.what());
//   }

//   // Cleanup the added item with Delete item.
//   try
//   {
//     sql_delete << dbStatementMovies.getSQL(StatementType::REMOVE, update_item)
//                << dbStatementMovies.getWhereSQL(StatementType::REMOVE, update_item);
//     CHECK(false == dr->hasTransactionStarted());
//     auto st = dr->CreateStatement(sql_delete.str());
//     CHECK(true == dr->hasTransactionStarted());
//     // Execute Remove, note no need to get returned DBResult
//     st->Execute();
//     CHECK(true == dr->hasTransactionStarted());
//     dr->Commit();
//     CHECK(false == dr->hasTransactionStarted());
//     STRCMP_CONTAINS("DELETE FROM Movies  WHERE M_ID =", sql_delete.str().c_str());
//     STRCMP_CONTAINS("AND Movie = 'Scout'", sql_delete.str().c_str());
//   }
//   catch (std::exception &e)
//   {
//     STRCMP_EQUAL("", e.what());
//   }
// }
