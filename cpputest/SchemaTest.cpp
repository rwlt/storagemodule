#include "storage/Model.h"
#include "storage/DBStorage.h"
#include "storage/DatabaseConnection.h"
#include "storage/Entity.h"
#include "DBStatementFixture.h"
#include <string>
#include "CppUTest/TestHarness.h"

using Storage::Driver::FieldType;
using Storage::Driver::CreateKind;
using Storage::Driver::DatabaseConnection;
using Storage::Driver::StorageException;
using Storage::Domain::Schema;
using Storage::DBStorage;
using Storage::Model;

//-------------------------------------------------------
// SchemaModel test group
//-------------------------------------------------------

TEST_GROUP(SchemaModel){
  string userName{ "TEST" };
  string userPassword{ "pass" };
  string role{ "GUEST" };
  string driver{"Sqlite"}, server{"localhost"}, dbname{"test.db"}, charset{"ISO8859_1"};
  vector<string> movies{};
 	DatabaseConnection dbconnect{driver, server, dbname, charset}; 
  DBStorage dbStorage{dbconnect};
  MovieAttrs MA;

  void setup() {
    //dbStorage.connect(dbconnect);

    movies.push_back("One");
    movies.push_back("2One");
    Schema schemaMovie = Schema(
        {{"M_ID", FieldType::PK_INT},
         {"Movie", FieldType::STRING}});
    dbStorage.model(MA.T.Movies, schemaMovie, CreateKind::DROP_CREATE);

    create_test_db(dbconnect, userName, userPassword, role, movies);

   }

   void teardown() {
   }

};

TEST(SchemaModel, ConstructorSchemaWithEmptyFieldsAndCreateEmptyEntity)
{
  Schema schemaMovie({});
  auto rowFields = schemaMovie.rowFields();
  LONGS_EQUAL(0, rowFields.size());
}

TEST(SchemaModel, ConstructorSchemaAndCreateEntity)
{
  Schema schemaMovie(
    { { "M_ID", FieldType::PK_INT },
      { "Movie", FieldType::STRING } }
  );
  auto rowFields = schemaMovie.rowFields();
  LONGS_EQUAL(2, rowFields.size());
}

TEST(SchemaModel, DBStorageModelFunctionWithSchema)
{
  Schema schemaMovie( 
    { { "M_ID", FieldType::PK_INT },
      { "Movie", FieldType::STRING } }
  );
  auto rowFields = schemaMovie.rowFields();
  LONGS_EQUAL(2, rowFields.size());

  Model movies = dbStorage.model("Movies", schemaMovie);
  auto entity = movies.entity();

  STRCMP_EQUAL("0", std::get<0>(entity->get(MA.F.ID)).c_str());
  STRCMP_EQUAL("", std::get<0>(entity->get(MA.F.Movie)).c_str());

}

// TEST(SchemaModel, DBStorageConstructorThrowsConnect)
// {
// 	DatabaseConnection dbconnect("", "", "", charset); 
//   CHECK_THROWS(StorageException, { dbStorage.connect(dbconnect);} );
// }


