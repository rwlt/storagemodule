/*
 * DBFirebirdDriverTest.cpp
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#include "storage/driver/DBFirebirdDriver.h"
#include "storage/driver/DBDriver.h"
#include "storage/driver/DBResult.h"
#include "storage/ParseDatabaseStatement.h"
#include "storage/ibpp/ibpp.h"
#include "storage/DBStorage.h"
#include "Hero.h"
#include <unordered_map>
#include <memory>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
//
//-------------------------------------------------------
// DBFirebirdDriverConstructor test group
//-------------------------------------------------------

TEST_GROUP(DBFirebirdDriverConstructor) {
    Storage::DatabaseConnection dbconnect{"firebird", "localhost", "/home/rodney/databases/utest.fdb", "ISO8859_1" };
	std::shared_ptr<Storage::Driver::DBDriver> dr{};
    void setup() {
    	dr = Storage::Driver::DBDriver::CreateDBDriver(dbconnect);
    }
};

TEST(DBFirebirdDriverConstructor, Constructor) {
    STRCMP_EQUAL(std::string("firebird").c_str(), dbconnect.getDriver().c_str());
    STRCMP_EQUAL(std::string("localhost").c_str(), dbconnect.getServer().c_str());
    STRCMP_EQUAL(std::string("/home/rodney/databases/utest.fdb").c_str(), dbconnect.getDBFile().c_str());
    STRCMP_EQUAL(std::string("ISO8859_1").c_str(), dbconnect.getCharSet().c_str());
    try {
    	Storage::Driver::DBFirebirdDriver fbDriver{dbconnect};
    	CHECK(false == fbDriver.isConnected());
    	CHECK(false == fbDriver.hasTransactionStarted());
    } catch (std::exception& e) {
    	STRCMP_EQUAL("", e.what());
    }

	IBPP::Database m_db = IBPP::DatabaseFactory("localhost", "/home/rodney/databases/utest.fdb",
			 "TEST", "pass", "RDB$ADMIN",
			"ISO8859_1", "");
	m_db->Connect();
	CHECK(m_db.intf() != 0);
	m_db->Disconnect();
	CHECK(m_db.intf() != 0);
	m_db = 0;
	CHECK(m_db.intf() == 0);


}


//
//-------------------------------------------------------
// DBFirebirdDriverAccessor test group
//-------------------------------------------------------

TEST_GROUP(DBFirebirdDriverAccessor) {
    Storage::DatabaseConnection dbconnect{"firebird", "localhost", "/home/rodney/databases/utest.fdb", "ISO8859_1" };
	std::string UserName{"TEST"}, UserPassword{"pass"}, UserRole{"RDB$ADMIN"};
	std::unique_ptr<Storage::Driver::DBDriver> dr{};

    const char* m_xml{"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    "  </database>\n"
	"<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
	"  <database-statements>\n"
	"      <statement type=\"select\">\n"
	"        <field name=\"StatuteID\" type=\"integer\"/>\n"
	"        <field name=\"Title\" type=\"string\"/>\n"
	"        <field name=\"Quota\" type=\"integer\"/>\n"
	"      </statement>\n"
	"  </database-statements>\n"
	"</table>\n"
    "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field name=\"PersonID\" type=\"string\"/>\n"
    "        <field name=\"Firstname\" type=\"string\"/>\n"
    "        <field name=\"Surname\" type=\"string\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</table>\n"
    "<table entity=\"Module::Staff\" name=\"STAFF\" key=\"ID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field ame=\"ID\" type=\"string\"/>\n"
    "        <field name=\"firstname\" type=\"string\"/>\n"
    "        <property name=\"surname\" type=\"string\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</table>\n"
    "</module-dbstorage>\n"};
    Storage::ParseDatabaseStatement parseStatement{"Statute", m_xml};
    Storage::DatabaseStatement dbStatementStatuteSelect{"","",""};
    void setup() {
    	if (dbconnect.getDriver().compare("firebird") == 0) {
    		dr = std::unique_ptr<Storage::Driver::DBDriver>(new Storage::Driver::DBFirebirdDriver{dbconnect});
    	}
        Storage::MapStatement map = parseStatement.ReadStatements();
        if (map.count(Storage::StatementType::SELECT) > 0) {
        	dbStatementStatuteSelect = map.at(Storage::StatementType::SELECT);
        }

    }
};

TEST(DBFirebirdDriverAccessor, ConstructorAndDeconstructor) {
    STRCMP_EQUAL(std::string("firebird").c_str(), dbconnect.getDriver().c_str());
    STRCMP_EQUAL(std::string("localhost").c_str(), dbconnect.getServer().c_str());
    STRCMP_EQUAL(std::string("/home/rodney/databases/utest.fdb").c_str(), dbconnect.getDBFile().c_str());
    STRCMP_EQUAL(std::string("ISO8859_1").c_str(), dbconnect.getCharSet().c_str());
    std::unique_ptr<Storage::Driver::DBStatement> pSt;
    std::unique_ptr<Storage::Driver::DBResult> pRt;
    try {
    	Storage::Driver::DBFirebirdDriver fbDriver{dbconnect};
    	CHECK(false == fbDriver.isConnected());
    	CHECK(false == fbDriver.hasTransactionStarted());
    	fbDriver.Connect(UserName, UserPassword, UserRole);
        CHECK(true == fbDriver.isConnected());
    	CHECK(false == fbDriver.hasTransactionStarted());
    	pSt = fbDriver.CreateStatement("SELECT * FROM Statute");
    	CHECK(true == fbDriver.hasTransactionStarted());
    	pRt = pSt->Execute();
    } catch (std::exception& e) {
    	STRCMP_EQUAL("", e.what());
    }
    try {
    	pSt->Execute();
    } catch (std::exception& e) {
    	STRCMP_CONTAINS("DBFirebirdStatement::Execute statement on database exception *** IBPP::LogicException ***", e.what());
    }
    try {
    	Storage::RowItem row = dbStatementStatuteSelect.createRowItem();
    	pRt->Fetch(row);
    } catch (std::exception& e) {
    	STRCMP_CONTAINS("DBFirebirdResult::Fetch row on database exception *** IBPP::LogicException ***", e.what());
    }

}

TEST(DBFirebirdDriverAccessor, ConnectAccessor) {
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

    try {
        dr->Connect("JOHN", UserPassword, UserRole);
    	LONGS_EQUAL(1,0);
    } catch (std::exception& e) {
        STRCMP_CONTAINS("DBFirebirdDriver::Connect user to database exception *** IBPP::SQLException ***", e.what());
    }
    CHECK(false == dr->isConnected());
	CHECK(false == dr->hasTransactionStarted());

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }
}

TEST(DBFirebirdDriverAccessor, PrepareSelectAccessor) {
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        dr->CreateStatement("SELECT * FROM HERO");
    	CHECK(true == dr->hasTransactionStarted());
        //
        dr->CreateStatement("SELECT ID FROM HERO");
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        dr->CreateStatement("SELECT Statute FROM HERO");
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        auto st = dr->CreateStatement("SELECT Surname FROM HERO");
        st->Execute();
    	LONGS_EQUAL(1,0);
    } catch (std::exception& e) {
        STRCMP_CONTAINS("DBFirebirdStatement::Execute statement on database exception *** IBPP::SQLException ***", e.what());
    }
	CHECK(true == dr->hasTransactionStarted());

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        dr->CreateStatement("SELECT Statute FROM HERO");
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

}

TEST(DBFirebirdDriverAccessor, CreateStatementInsertAccessor) {
    STRCMP_EQUAL("Statute(StatuteID):3(1)[StatuteID INTEGER ISKEY STR: INT:0 ](2)[Title STRING STR: INT:0 ](3)[Quota INTEGER STR: INT:0 ]",
    		dbStatementStatuteSelect.StatementDetail().c_str());
	Storage::RowItem new_item = dbStatementStatuteSelect.createRowItem();
	new_item.setField(0, 0);
	new_item.setField(1, "Swordsman");
	std::string sqlinsert = dbStatementStatuteSelect.getSQL(Storage::StatementType::INSERT, new_item);
    STRCMP_EQUAL("INSERT INTO Statute(StatuteID, Title, Quota) VALUES (0, 'Swordsman', 0) RETURNING StatuteID, Title, Quota", sqlinsert.c_str());

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
        dr->CreateStatement(sqlinsert);
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
        dr->CreateStatement(sqlinsert);
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
    	// Wont let us do a wrong statement...
        auto st = dr->CreateStatement("INSERT INTO Statute(surname) VALUES (?) RETURNING ID, surname");
        st->Execute();
    	LONGS_EQUAL(1,0);
    } catch (std::exception& e) {
        STRCMP_CONTAINS("DBFirebirdStatement::Execute statement on database exception *** IBPP::SQLException ***", e.what());
    }
	CHECK(true == dr->hasTransactionStarted());

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
        dr->CreateStatement(sqlinsert);
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

}

TEST(DBFirebirdDriverAccessor, ExecuteSelectAccessor) {
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	std::unique_ptr<Storage::Driver::DBStatement> st = dr->CreateStatement("SELECT StatuteID, Title, Quota FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());

        STRCMP_EQUAL("Statute(StatuteID):3(1)[StatuteID INTEGER ISKEY STR: INT:0 ](2)[Title STRING STR: INT:0 ](3)[Quota INTEGER STR: INT:0 ]",
        		dbStatementStatuteSelect.StatementDetail().c_str());

    	std::unique_ptr<Storage::Driver::DBResult> result = st->Execute();
    	Storage::RowItem row = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result->Fetch(row));
        STRCMP_EQUAL("(1)[StatuteID INTEGER ISKEY ISSET STR: INT:2 ](2)[Title STRING ISSET STR:KNight INT:0 ](3)[Quota INTEGER ISSET STR: INT:25 ]", row.ItemDetail().c_str());

        dr->Commit();
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	auto st2 = dr->CreateStatement("SELECT StatuteID, Title, Quota FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());
    	std::unique_ptr<Storage::Driver::DBResult> result2 = st2->Execute();
    	Storage::RowItem row2 = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result2->Fetch(row2));
        STRCMP_EQUAL("(1)[StatuteID INTEGER ISKEY ISSET STR: INT:2 ](2)[Title STRING ISSET STR:KNight INT:0 ](3)[Quota INTEGER ISSET STR: INT:25 ]", row.ItemDetail().c_str());
        dr->Commit();
    	CHECK(false == dr->hasTransactionStarted());
    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    try {
    	// Not new Connect but committed we can still prepare new transaction with prepare on a db connection
    	CHECK(false == dr->hasTransactionStarted());
        auto st = dr->CreateStatement("SELECT StatuteID, Title, Quota FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());
    	std::unique_ptr<Storage::Driver::DBResult> result = st->Execute();
    	Storage::RowItem row = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result->Fetch(row));
        STRCMP_EQUAL("(1)[StatuteID INTEGER ISKEY ISSET STR: INT:2 ](2)[Title STRING ISSET STR:KNight INT:0 ](3)[Quota INTEGER ISSET STR: INT:25 ]", row.ItemDetail().c_str());

    	// Re executing same statement prepared is working
    	std::unique_ptr<Storage::Driver::DBResult> result2 = st->Execute();
    	Storage::RowItem row2 = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result2->Fetch(row2));
        STRCMP_EQUAL("(1)[StatuteID INTEGER ISKEY ISSET STR: INT:2 ](2)[Title STRING ISSET STR:KNight INT:0 ](3)[Quota INTEGER ISSET STR: INT:25 ]", row2.ItemDetail().c_str());
    	CHECK(true == result2->Fetch(row2));
        STRCMP_EQUAL("(1)[StatuteID INTEGER ISKEY ISSET STR: INT:3 ](2)[Title STRING ISSET STR:Cavalry INT:0 ](3)[Quota INTEGER ISSET STR: INT:25 ]", row2.ItemDetail().c_str());
    	CHECK(true == result2->Fetch(row2));
        STRCMP_EQUAL("(1)[StatuteID INTEGER ISKEY ISSET STR: INT:4 ](2)[Title STRING ISSET STR:Warrior INT:0 ](3)[Quota INTEGER ISSET STR: INT:250 ]", row2.ItemDetail().c_str());
    	CHECK(false == result2->Fetch(row2));
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        auto st = dr->CreateStatement("SELECT COUNT(*) FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());

    	std::unique_ptr<Storage::Driver::DBResult> result = st->Execute();

    	dr->Commit(); // try now before getting result with st_fetch member
    	// We make our row to get Count results
    	Storage::RowFields fields;
    	fields.insert(std::make_pair(1, Storage::FieldDetail{"COUNT", 0}));
    	Storage::RowItem row{fields};
        STRCMP_EQUAL("(1)[COUNT INTEGER ISSET STR: INT:0 ]", row.ItemDetail().c_str());
        CHECK(true == result->Fetch(row)); // should be no transaction open on st_fetch member in DBResult
        LONGS_EQUAL(1,0);

    } catch (std::exception& e) {
        STRCMP_CONTAINS("DBFirebirdResult::Fetch row on database exception *** IBPP::LogicException ***", e.what());
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        auto st = dr->CreateStatement("SELECT COUNT(*) FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());
    	dr->Commit();
    	// We can make new statement as it starts a transaction.
        auto st2 = dr->CreateStatement("SELECT COUNT(*) FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        auto st = dr->CreateStatement("SELECT COUNT(*) FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());

    	std::unique_ptr<Storage::Driver::DBResult> result = st->Execute();
    	Storage::RowFields fields;
    	fields.insert(std::make_pair(1, Storage::FieldDetail{"COUNT", 0}));
    	Storage::RowItem row{fields};
        STRCMP_EQUAL("(1)[COUNT INTEGER ISSET STR: INT:0 ]", row.ItemDetail().c_str());
        CHECK(true == result->Fetch(row)); // should be no transaction open on st_fetch memeber in DBResult
    	int count;
    	bool is_set;
    	row.getField(1, count, is_set);
    	LONGS_EQUAL(3, count);

    	dr->Commit(); // be sure to use after getting DBResult

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

}

TEST(DBFirebirdDriverAccessor, ExecuteReturnInsertAndExecuteRemoveAccessor) {
    STRCMP_EQUAL("Statute(StatuteID):3(1)[StatuteID INTEGER ISKEY STR: INT:0 ](2)[Title STRING STR: INT:0 ](3)[Quota INTEGER STR: INT:0 ]",
    		dbStatementStatuteSelect.StatementDetail().c_str());
	Storage::RowItem new_item = dbStatementStatuteSelect.createRowItem();
	new_item.setField(1, 0);
	new_item.setField(2, "Swordsman");
	std::string sqlinsert = dbStatementStatuteSelect.getSQL(Storage::StatementType::INSERT, new_item);
    STRCMP_EQUAL("INSERT INTO Statute(StatuteID, Title, Quota) VALUES (0, 'Swordsman', 0) RETURNING StatuteID, Title, Quota", sqlinsert.c_str());

    Storage::RowItem remove_item = dbStatementStatuteSelect.createRowItem();

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	//NOT using ? placeholder we send our new_item object within the INSERT sql statement.
    	auto st = dr->CreateStatement(sqlinsert, new_item);
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Return Insert which fills the new_item we setup wit create statement
    	Storage::RowItem row = st->ExecuteReturn();
    	std::string value;
    	bool is_set;
    	row.getField(1, value, is_set);
        STRCMP_EQUAL("Swordsman", value.c_str());
        dr->Commit();

        remove_item = row;
    	std::stringstream sqldelete;
    	sqldelete << dbStatementStatuteSelect.getSQL(Storage::StatementType::DELETE, remove_item)
    		      << dbStatementStatuteSelect.getWhereSQL(Storage::StatementType::DELETE, remove_item);

    	CHECK(false == dr->hasTransactionStarted());
    	auto st2 = dr->CreateStatement(sqldelete.str());
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Remove, note no need to get returned DBResult
    	st2->Execute();
    	CHECK(true == dr->hasTransactionStarted());
    	dr->Commit();
    	CHECK(false == dr->hasTransactionStarted());

    	STRCMP_CONTAINS("DELETE FROM Statute  WHERE StatuteID", sqldelete.str().c_str());

    } catch (std::exception& e) {
       // STRCMP_EQUAL("Fetch row on database exception. ]", e.what());
    	LONGS_EQUAL(1,0);
    }

    try {
    	// Not new Connect but commited so no transaction is still going
    	CHECK(false == dr->hasTransactionStarted());
        auto st = dr->CreateStatement("SELECT StatuteID, Title, Quota FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());
    	std::unique_ptr<Storage::Driver::DBResult> result = st->Execute();
    	Storage::RowItem row = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result->Fetch(row));
        STRCMP_EQUAL("(1)[StatuteID INTEGER ISKEY ISSET STR: INT:2 ](2)[Title STRING ISSET STR:KNight INT:0 ](3)[Quota INTEGER ISSET STR: INT:25 ]", row.ItemDetail().c_str());

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

}

TEST(DBFirebirdDriverAccessor, ExecuteUpdateAccessor) {
    STRCMP_EQUAL("Statute(StatuteID):3(1)[StatuteID INTEGER ISKEY STR: INT:0 ](2)[Title STRING STR: INT:0 ](3)[Quota INTEGER STR: INT:0 ]",
    		dbStatementStatuteSelect.StatementDetail().c_str());
	Storage::RowItem new_item = dbStatementStatuteSelect.createRowItem();
	new_item.setField(0, 0);
	new_item.setField(1, "Swordsman");
	std::string sqlinsert = dbStatementStatuteSelect.getSQL(Storage::StatementType::INSERT, new_item);
    STRCMP_EQUAL("INSERT INTO Statute(StatuteID, Title, Quota) VALUES (0, 'Swordsman', 0) RETURNING StatuteID, Title, Quota", sqlinsert.c_str());

	std::stringstream sql_delete;
	std::stringstream sql_selectupdate;
	std::stringstream sql_update;

    Storage::RowItem orig_item = dbStatementStatuteSelect.createRowItem();
    Storage::RowItem update_item = dbStatementStatuteSelect.createRowItem();
    Storage::RowItem remove_item = dbStatementStatuteSelect.createRowItem();
    Storage::RowItem row;

    /// Add an item to test updates
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	//NOT using ? placeholder we send our new_item object within the INSERT sql statement.
    	// Zero length of fields
    	auto st = dr->CreateStatement(sqlinsert, new_item);
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Return Insert 1) Insert new hero to edit
    	row = st->ExecuteReturn();
    	st->ExecuteReturn();
    	st->ExecuteReturn();
    	std::string value;
    	bool is_set;
    	row.getField(1, value, is_set);
        STRCMP_EQUAL("Swordsman", value.c_str());
        dr->Commit(); // Commit insert..

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    remove_item = row;
    orig_item = row;
    update_item = orig_item;
    update_item.setField(2, "Scout");
	Storage::RowFields upfields;
	Storage::FieldDetail fd{"Title", "Scout"};
	upfields.insert(std::make_pair(1, fd));
	Storage::RowItem rowp(upfields);

    try{
        // 2) Update the hero just added
    	CHECK(false == dr->hasTransactionStarted());

    	sql_update << dbStatementStatuteSelect.getSQL(Storage::StatementType::UPDATE, rowp)
    			<< " WHERE CURRENT OF MYCURSOR";
    	STRCMP_EQUAL("UPDATE Statute SET Title = 'Scout'  WHERE CURRENT OF MYCURSOR", sql_update.str().c_str());

    	std::string selup2("SELECT StatuteID, Title FROM Statute  WHERE Title = 'Swordsman'  FOR UPDATE");
    	auto st1 = dr->CreateStatement(selup2);
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Cursor select to fetch.
    	// Statement 1 execute.
    	std::unique_ptr<Storage::Driver::DBResult> result = st1->ExecuteCursor("MYCURSOR");

    	auto st2 = dr->CreateStatement(sql_update.str());
    	// Fetch over result - will be one row for our hero update found
    	// Statement 2 execute on each iteration found where Current of MYCURSOR
    	Storage::RowItem row;
    	CHECK(true == result->Fetch(row));
    	st2->Execute();
    	CHECK(true == result->Fetch(row));
    	st2->Execute();
    	CHECK(true == result->Fetch(row));
    	st2->Execute();
    	CHECK(false == result->Fetch(row));

    	dr->Commit(); // Finish our update and commit transaction (not started = true)

    } catch (std::exception& e) {
    	dr->Commit(); // Finish our update and commit transaction (not started = true)
        STRCMP_EQUAL("", e.what());
    }

    // Cleanup the added item with Delete item.
    try{
    	std::string sqld ("DELETE FROM Statute  WHERE Title ='Scout'");
    	CHECK(false == dr->hasTransactionStarted());
    	auto st = dr->CreateStatement(sqld);
    	//auto st = dr->Prepare(sql_delete.str());
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Remove, note no need to get returned DBResult
    	st->Execute();
    	CHECK(true == dr->hasTransactionStarted());
    	dr->Commit();
    	CHECK(false == dr->hasTransactionStarted());

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

}

TEST(DBFirebirdDriverAccessor, ExecuteUpdateItemAccessor) {
    STRCMP_EQUAL("Statute(StatuteID):3(1)[StatuteID INTEGER ISKEY STR: INT:0 ](2)[Title STRING STR: INT:0 ](3)[Quota INTEGER STR: INT:0 ]",
    		dbStatementStatuteSelect.StatementDetail().c_str());
	Storage::RowItem new_item = dbStatementStatuteSelect.createRowItem();
	new_item.setField(0, 0);
	new_item.setField(1, "Swordsman");
	std::string sqlinsert = dbStatementStatuteSelect.getSQL(Storage::StatementType::INSERT, new_item);
    STRCMP_EQUAL("INSERT INTO Statute(StatuteID, Title, Quota) VALUES (0, 'Swordsman', 0) RETURNING StatuteID, Title, Quota", sqlinsert.c_str());

	std::stringstream sql_delete;
	std::stringstream sql_selectupdate;
	std::stringstream sql_update;

    Storage::RowItem orig_item = dbStatementStatuteSelect.createRowItem();
    Storage::RowItem update_item = dbStatementStatuteSelect.createRowItem();
    Storage::RowItem remove_item = dbStatementStatuteSelect.createRowItem();
    Storage::RowItem row;

    /// Add an item to test updates
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	// For ExecuteReturn we send our new_item object to Prepare..
    	auto st = dr->CreateStatement(sqlinsert, new_item);
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Return Insert 1) Insert new hero to edit
    	row = st->ExecuteReturn();
    	std::string value;
    	bool is_set;
    	row.getField(1, value, is_set);
        STRCMP_EQUAL("Swordsman", value.c_str());
        dr->Commit(); // Commit insert..

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    remove_item = row;
    orig_item = row;
    update_item = orig_item;
    update_item.setField(2, "Scout");

    try{
        // 2) Update the hero just added
    	CHECK(false == dr->hasTransactionStarted());

    	sql_selectupdate << dbStatementStatuteSelect.getSQL(Storage::StatementType::SELECT, orig_item) << " "
    			<< dbStatementStatuteSelect.getWhereSQL(Storage::StatementType::SELECT,
    					orig_item) << " FOR UPDATE";
    	sql_update << dbStatementStatuteSelect.getSQL(Storage::StatementType::UPDATE, update_item)
    			<< " WHERE CURRENT OF MYCURSOR";
    	STRCMP_CONTAINS("SELECT StatuteID, Title, Quota FROM Statute   WHERE StatuteID", sql_selectupdate.str().c_str());
    	STRCMP_CONTAINS("AND Title = 'Swordsman' AND Quota = 0  FOR UPDATE", sql_selectupdate.str().c_str());
    	STRCMP_CONTAINS("UPDATE Statute SET StatuteID =", sql_update.str().c_str());
    	STRCMP_CONTAINS("Title = 'Scout', Quota = 0  WHERE CURRENT OF MYCURSOR", sql_update.str().c_str());

    	auto st1 = dr->CreateStatement(sql_selectupdate.str());
    	CHECK(true == dr->hasTransactionStarted());
    	// Execute Cursor select to fetch.
    	// Statement 1 execute.
    	std::unique_ptr<Storage::Driver::DBResult> result = st1->ExecuteCursor("MYCURSOR");

    	auto st2 = dr->CreateStatement(sql_update.str());//, update_item);

    	// Fetch over result - will be one row for our hero update found
    	Storage::RowItem row;// use an empty row for fetch as we dont need the original data
    	CHECK(true == result->Fetch(row));
    	// Statement 2 execute on each iteration found where Current of MYCURSOR
    	st2->Execute();//AtCurrent();
//    	CHECK(false == result->Fetch(row));
//
//    	CHECK(true == dr->hasTransactionStarted());
    	dr->Commit(); // Finish our update and commit transaction (not started = true)

    } catch (std::exception& e) {
    	dr->Commit(); // Finish our update and commit transaction (not started = true)
        STRCMP_EQUAL("", e.what());
    }

    // Cleanup the added item with Delete item.
    try{
    	sql_delete << dbStatementStatuteSelect.getSQL(Storage::StatementType::DELETE, update_item)
    		      << dbStatementStatuteSelect.getWhereSQL(Storage::StatementType::DELETE, update_item);
    	CHECK(false == dr->hasTransactionStarted());
    	auto st = dr->CreateStatement(sql_delete.str());
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Remove, note no need to get returned DBResult
    	st->Execute();
    	CHECK(true == dr->hasTransactionStarted());
    	dr->Commit();
    	CHECK(false == dr->hasTransactionStarted());
    	STRCMP_CONTAINS("DELETE FROM Statute  WHERE StatuteID = ", sql_delete.str().c_str());

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

}

