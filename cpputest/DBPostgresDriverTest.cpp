/*
 * DBPostgresDriverTest.cpp
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#include "storage/driver/DBPostgresDriver.h"
#include "storage/driver/DBDriver.h"
#include "storage/driver/DBResult.h"
#include "storage/ParseDatabaseStatement.h"
#include "storage/DBStorage.h"
#include "Hero.h"
#include <unordered_map>
#include <memory>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
//
//-------------------------------------------------------
// DBPostgresDriverConstructor test group
//-------------------------------------------------------

TEST_GROUP(DBPostgresDriverConstructor) {
    Storage::DatabaseConnection dbconnect{"postgres", "localhost "listbook_test", "ISO8859_1" };
	std::shared_ptr<Storage::Driver::DBDriver> dr{};
    void setup() {
    	dr = Storage::Driver::DBDriver::CreateDBDriver(dbconnect);
    }
};

TEST(DBPostgresDriverConstructor, Constructor) {
    STRCMP_EQUAL(std::string("postgres").c_str(), dbconnect.getDriver().c_str());
    STRCMP_EQUAL(std::string("lolocalhost_str(), dbconnect.getServer().c_str());
    STRCMP_EQUAL(std::string("listbook_test").c_str(), dbconnect.getDBFile().c_str());
    STRCMP_EQUAL(std::string("ISO8859_1").c_str(), dbconnect.getCharSet().c_str());
    try {
    	Storage::Driver::DBPostgresDriver pgDriver{dbconnect};
    	CHECK(false == pgDriver.isConnected());
    	CHECK(false == pgDriver.hasTransactionStarted());
    	pgDriver.Connect("test", "password", "");
    	CHECK(true == pgDriver.isConnected());
    } catch (std::exception& e) {
    	STRCMP_EQUAL("", e.what());
    }

}
//
//-------------------------------------------------------
// DBPostgresDriverAccessor test group
//-------------------------------------------------------

TEST_GROUP(DBPostgresDriverAccessor) {

    const char* m_xml{"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    "<module-dbstorage>\n"
    "  <database>\n"
    "   <file role=\"live\" driver=\"postgres\" name=\"listbook\" server=\"localocalhostrset=\"ISO8859_1\"/>\n"
    "   <file role=\"test\" driver=\"postgres\" name=\"listbook_test\" server=\"localhlocalhostet=\"ISO8859_1\"/>\n"
    "  </database>\n"
    "<table entity=\"Module::Statute\" name=\"Statute\" key=\"ID\">\n"
    "  <database-statements>\n"
    "      <statement type=\"select\">\n"
    "        <field name=\"ID\" type=\"autogenint\"/>\n"
    "        <field name=\"title\" type=\"string\"/>\n"
    "      </statement>\n"
    "  </database-statements>\n"
    "</object>\n"
    "</module-dbstorage>\n"};
    Storage::ParseDatabaseStatement parseStatement{"Statute", m_xml};
    Storage::DatabaseStatement dbStatementStatuteSelect{"","",""};
    Storage::DatabaseConnection dbconnect{"postgres", "localhoslocalhostok_test", "ISO8859_1" };
	std::string UserName{"test"}, UserPassword{"password"}, UserRole{""};
	std::unique_ptr<Storage::Driver::DBDriver> dr{};
    void setup() {
    	if (dbconnect.getDriver().compare("postgres") == 0) {
    		dr = std::unique_ptr<Storage::Driver::DBDriver>(new Storage::Driver::DBPostgresDriver{dbconnect});
    	}
        Storage::MapStatement map = parseStatement.ReadStatements();
        if (map.count(Storage::StatementType::SELECT) > 0) {
        	dbStatementStatuteSelect = map.at(Storage::StatementType::SELECT);
        }

    }
};

TEST(DBPostgresDriverAccessor, ConstructorAndDeconstructor) {
    STRCMP_EQUAL(std::string("postgres").c_str(), dbconnect.getDriver().c_str());
    STRCMP_EQUAL(std::string("localhost"localhostdbconnect.getServer().c_str());
    STRCMP_EQUAL(std::string("listbook_test").c_str(), dbconnect.getDBFile().c_str());
    STRCMP_EQUAL(std::string("ISO8859_1").c_str(), dbconnect.getCharSet().c_str());
    std::unique_ptr<Storage::Driver::DBStatement> pSt;
    std::unique_ptr<Storage::Driver::DBResult> pRt;
    try {
    	Storage::Driver::DBPostgresDriver fbDriver{dbconnect};
    	CHECK(false == fbDriver.isConnected());
    	CHECK(false == fbDriver.hasTransactionStarted());
    	fbDriver.Connect(UserName, UserPassword, UserRole);
        CHECK(true == fbDriver.isConnected());
    	CHECK(false == fbDriver.hasTransactionStarted());
    	pSt = fbDriver.CreateStatement("SELECT * FROM Statute");
    	CHECK(true == fbDriver.hasTransactionStarted());
    	pRt = pSt->Execute();
    	fbDriver.Commit();// Close transactions and committed
    } catch (std::exception& e) {
    	STRCMP_EQUAL("", e.what());
    }
    try {
    	pSt->Execute();
    } catch (std::exception& e) {
    	STRCMP_CONTAINS("DBPostgresStatement::Execute statement on database exception Error", e.what());
    }
    try {
    	Storage::RowItem row = dbStatementStatuteSelect.createRowItem();
    	pRt->Fetch(row);
    } catch (std::exception& e) {
    	STRCMP_EQUAL("Fetch row on database exception.", e.what());
    }

}

TEST(DBPostgresDriverAccessor, ConnectAccessor) {
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

    try {
        dr->Connect("JOHN", UserPassword, UserRole);
    	LONGS_EQUAL(1,0);
    } catch (std::exception& e) {
        STRCMP_CONTAINS("DBPostgresDriver::Connect user to database exception FATAL", e.what());
    }
    CHECK(false == dr->isConnected());
	CHECK(false == dr->hasTransactionStarted());

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }
}

TEST(DBPostgresDriverAccessor, CreateStatementSelectAccessor) {
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        auto st = dr->CreateStatement("SELECT * FROM STATUTE");
    	CHECK(true == dr->hasTransactionStarted());
        auto result = st->Execute();

        auto st1 = dr->CreateStatement("SELECT ID FROM STATUTE");
    	CHECK(true == dr->hasTransactionStarted());
        st1->Execute();
        dr->Commit();
        // Commit will end transaction;
    	CHECK(false == dr->hasTransactionStarted());
        CHECK(true == dr->isConnected());
        auto st2 = dr->CreateStatement("SELECT ID FROM STATUTE");
    	CHECK(true == dr->hasTransactionStarted());
        st2->Execute();
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }
    // Not committed yet
	CHECK(true == dr->hasTransactionStarted());
    CHECK(true == dr->isConnected());

    try {
        // Connect a user will do a committed if transaction start is true
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        auto st = dr->CreateStatement("SELECT Surname FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());
        st->Execute();
        LONGS_EQUAL(1,0);
    } catch (std::exception& e) {
    	STRCMP_CONTAINS("DBPostgresStatement::Execute statement on database exception ERROR", e.what());
        CHECK(true == dr->isConnected());
    	CHECK(true == dr->hasTransactionStarted());
    }

    try {
        // This commit can not be done as transaction aborted with earlier bad statement
        dr->Commit();
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	STRCMP_CONTAINS("DBPostgresDriver::Commit on database exception Attempt to commit previously aborted transaction", e.what());
        CHECK(true == dr->isConnected());
        // Driver marks transaction as not started
    	CHECK(false == dr->hasTransactionStarted());
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	STRCMP_EQUAL("", e.what());
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        dr->CreateStatement("SELECT Statute FROM HERO");
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

}

TEST(DBPostgresDriverAccessor, CreateStatementInsertAccessor) {
    STRCMP_EQUAL("Statute(ID):2(1)[ID INTEGER ISKEY AUTOGEN STR: INT:0 ](2)[title STRING STR: INT:0 ]",
    		dbStatementStatuteSelect.StatementDetail().c_str());
	Storage::RowItem new_item = dbStatementStatuteSelect.createRowItem();
	new_item.setField(1, 0);
	new_item.setField(2, "Swordsman");
	std::string sqlinsert = dbStatementStatuteSelect.getSQL(Storage::StatementType::INSERT, new_item);
    STRCMP_EQUAL("INSERT INTO Statute(title) VALUES ('Swordsman') RETURNING ID, title", sqlinsert.c_str());

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
        dr->CreateStatement(sqlinsert);
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
        dr->CreateStatement(sqlinsert);
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
    	// Wont let us do a wrong statement...
        auto st = dr->CreateStatement("INSERT INTO HERO(surname) VALUES ('Swordsman') RETURNING ID, surname");
        st->Execute();
    	LONGS_EQUAL(1,0);
    } catch (std::exception& e) {
        STRCMP_CONTAINS("DBPostgresStatement::Execute statement on database exception ERROR", e.what());
    }
	CHECK(true == dr->hasTransactionStarted());
//    try {
//        CHECK(true == dr->isConnected());
//        dr->Commit();
//    	LONGS_EQUAL(1,0);
//    } catch (std::exception& e) {
//        STRCMP_EQUAL("Commit on database exception.", e.what());
//    }
//	CHECK(false == dr->hasTransactionStarted());

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());
        dr->CreateStatement(sqlinsert);
    	CHECK(true == dr->hasTransactionStarted());
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }

}

TEST(DBPostgresDriverAccessor, ExecuteSelectAccessor) {
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	auto st = dr->CreateStatement("SELECT ID, Title FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());

        STRCMP_EQUAL("Statute(ID):2(1)[ID INTEGER ISKEY AUTOGEN STR: INT:0 ](2)[title STRING STR: INT:0 ]",
        		dbStatementStatuteSelect.StatementDetail().c_str());

    	auto result = st->Execute();
    	Storage::RowItem row = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result->Fetch(row));
        STRCMP_EQUAL("(1)[ID INTEGER ISKEY ISSET AUTOGEN STR: INT:1 ](2)[title STRING ISSET STR:Knight INT:0 ]", row.ItemDetail().c_str());
    	CHECK(true == result->Fetch(row));
        STRCMP_EQUAL("(1)[ID INTEGER ISKEY ISSET AUTOGEN STR: INT:2 ](2)[title STRING ISSET STR:Warrior INT:0 ]", row.ItemDetail().c_str());
    	CHECK(false == result->Fetch(row));

        dr->Commit();
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	auto st2 = dr->CreateStatement("SELECT ID, Title FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());
    	auto result2 = st2->Execute();
    	Storage::RowItem row2 = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result2->Fetch(row2));
        STRCMP_EQUAL("(1)[ID INTEGER ISKEY ISSET AUTOGEN STR: INT:1 ](2)[title STRING ISSET STR:Knight INT:0 ]", row2.ItemDetail().c_str());
    	CHECK(true == result2->Fetch(row2));
        STRCMP_EQUAL("(1)[ID INTEGER ISKEY ISSET AUTOGEN STR: INT:2 ](2)[title STRING ISSET STR:Warrior INT:0 ]", row2.ItemDetail().c_str());
    	CHECK(false == result2->Fetch(row2));
        dr->Commit();
    	CHECK(false == dr->hasTransactionStarted());
    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    try {
    	// Not new Connect but committed we can still prepare new transaction with prepare on a db connection
    	CHECK(false == dr->hasTransactionStarted());
        auto st = dr->CreateStatement("SELECT ID, Title FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());
    	auto result = st->Execute();
    	Storage::RowItem row = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result->Fetch(row));
        STRCMP_EQUAL("(1)[ID INTEGER ISKEY ISSET AUTOGEN STR: INT:1 ](2)[title STRING ISSET STR:Knight INT:0 ]", row.ItemDetail().c_str());

    	// Re executing same statement prepared is working
    	auto result2 = st->Execute();
    	Storage::RowItem row2 = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result2->Fetch(row2));
        STRCMP_EQUAL("(1)[ID INTEGER ISKEY ISSET AUTOGEN STR: INT:1 ](2)[title STRING ISSET STR:Knight INT:0 ]", row2.ItemDetail().c_str());
    	CHECK(true == result2->Fetch(row2));
        STRCMP_EQUAL("(1)[ID INTEGER ISKEY ISSET AUTOGEN STR: INT:2 ](2)[title STRING ISSET STR:Warrior INT:0 ]", row2.ItemDetail().c_str());
    	CHECK(false == result2->Fetch(row2));
    } catch (std::exception& e) {
    	LONGS_EQUAL(1,0);
    }


    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        auto st = dr->CreateStatement("SELECT COUNT(*) FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());

    	auto result = st->Execute();

    	dr->Commit(); // try now before getting result
    	// We make our row to get Count results
    	Storage::RowFields fields;
    	fields.insert(std::make_pair(1, Storage::FieldDetail{"COUNT", 0}));
    	Storage::RowItem row{fields};
        STRCMP_EQUAL("(1)[COUNT INTEGER ISSET STR: INT:0 ]", row.ItemDetail().c_str());
        CHECK(true == result->Fetch(row)); // should be ok even when committed on transaction in pgxx::results
        STRCMP_EQUAL("(1)[COUNT INTEGER ISSET STR: INT:2 ]", row.ItemDetail().c_str());

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

        auto st = dr->CreateStatement("SELECT COUNT(*) FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());

    	auto result = st->Execute();
    	Storage::RowFields fields;
    	fields.insert(std::make_pair(1, Storage::FieldDetail{"COUNT", 0}));
    	Storage::RowItem row{fields};
        STRCMP_EQUAL("(1)[COUNT INTEGER ISSET STR: INT:0 ]", row.ItemDetail().c_str());
        CHECK(true == result->Fetch(row));
    	int count;
    	bool is_set;
    	row.getField(1, count, is_set);
    	LONGS_EQUAL(2, count);

    	dr->Commit(); // be sure to use after getting DBResult

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

}

TEST(DBPostgresDriverAccessor, ExecuteReturnInsertAndExecuteRemoveAccessor) {
    STRCMP_EQUAL("Statute(ID):2(1)[ID INTEGER ISKEY AUTOGEN STR: INT:0 ](2)[title STRING STR: INT:0 ]",
    		dbStatementStatuteSelect.StatementDetail().c_str());
	Storage::RowItem new_item = dbStatementStatuteSelect.createRowItem();
	new_item.setField(1, 0);
	new_item.setField(2, "Swordsman");
	std::string sqlinsert = dbStatementStatuteSelect.getSQL(Storage::StatementType::INSERT, new_item);
    STRCMP_EQUAL("INSERT INTO Statute(title) VALUES ('Swordsman') RETURNING ID, title", sqlinsert.c_str());

    Storage::RowItem remove_item = dbStatementStatuteSelect.createRowItem();

    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	//NOT using ? placeholder we send our new_item object within the INSERT sql statement.
    	auto st = dr->CreateStatement(sqlinsert, new_item);
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Return Insert which fills the new_item we setup with create statement
    	Storage::RowItem row = st->ExecuteReturn();
    	std::string value;
    	bool is_set;
    	row.getField(2, value, is_set);
        STRCMP_EQUAL("Swordsman", value.c_str());
        dr->Commit();

        remove_item = row;
    	std::stringstream sqldelete;
    	sqldelete << dbStatementStatuteSelect.getSQL(Storage::StatementType::DELETE, remove_item)
    		      << dbStatementStatuteSelect.getWhereSQL(Storage::StatementType::DELETE, remove_item);

    	CHECK(false == dr->hasTransactionStarted());
    	auto st2 = dr->CreateStatement(sqldelete.str());
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Remove, note no need to get returned DBResult
    	st2->Execute();
    	CHECK(true == dr->hasTransactionStarted());
    	dr->Commit();
    	CHECK(false == dr->hasTransactionStarted());
    	STRCMP_CONTAINS("DELETE FROM Statute  WHERE ID =", sqldelete.str().c_str());
    	STRCMP_CONTAINS("AND title = 'Swordsman'", sqldelete.str().c_str());

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    try {
    	// Not new Connect but committed so no transaction is still going
    	CHECK(false == dr->hasTransactionStarted());
        auto st = dr->CreateStatement("SELECT ID, Title FROM Statute");
    	CHECK(true == dr->hasTransactionStarted());
    	auto result = st->Execute();
    	Storage::RowItem row = dbStatementStatuteSelect.createRowItem();
    	CHECK(true == result->Fetch(row));
        STRCMP_EQUAL("(1)[ID INTEGER ISKEY ISSET AUTOGEN STR: INT:1 ](2)[title STRING ISSET STR:Knight INT:0 ]", row.ItemDetail().c_str());

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

}

TEST(DBPostgresDriverAccessor, ExecuteUpdateAccessor) {
    STRCMP_EQUAL("Statute(ID):2(1)[ID INTEGER ISKEY AUTOGEN STR: INT:0 ](2)[title STRING STR: INT:0 ]",
    		dbStatementStatuteSelect.StatementDetail().c_str());
	Storage::RowItem new_item = dbStatementStatuteSelect.createRowItem();
	new_item.setField(1, 0);
	new_item.setField(2, "Swordsman");
	std::string sqlinsert = dbStatementStatuteSelect.getSQL(Storage::StatementType::INSERT, new_item);
    STRCMP_EQUAL("INSERT INTO Statute(title) VALUES ('Swordsman') RETURNING ID, title", sqlinsert.c_str());

	std::stringstream sql_update;
    Storage::RowItem row;

    /// Add an item to test updates
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	auto st = dr->CreateStatement(sqlinsert, new_item);
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Return for three Insert
    	row = st->ExecuteReturn();
    	st->ExecuteReturn();
    	st->ExecuteReturn();
    	std::string value;
    	bool is_set;
    	row.getField(2, value, is_set);
        STRCMP_EQUAL("Swordsman", value.c_str());
        dr->Commit(); // Commit insert..

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    Storage::RowFields upfields;
	Storage::FieldDetail fd{"title", "Scout"};
	upfields.insert(std::make_pair(1, fd));
	Storage::RowItem rowp(upfields);

    try{
        // 2) Update a collection of statutes just added
    	CHECK(false == dr->hasTransactionStarted());
    	sql_update << dbStatementStatuteSelect.getSQL(Storage::StatementType::UPDATE, rowp)
    			<< " WHERE title = 'Swordsman' ";
    	STRCMP_EQUAL("UPDATE Statute SET title = 'Scout'  WHERE title = 'Swordsman' ", sql_update.str().c_str());
    	auto st = dr->CreateStatement(sql_update.str());
    	st->Execute();
    	dr->Commit(); // Finish our update and commit transaction (not started = true)

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    // Cleanup the added item with Delete item.
    try{
    	std::string sqld ("DELETE FROM Statute  WHERE title ='Scout'");
    	CHECK(false == dr->hasTransactionStarted());
    	auto st = dr->CreateStatement(sqld);
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Remove, note no need to get returned DBResult
    	st->Execute();
    	CHECK(true == dr->hasTransactionStarted());
    	dr->Commit();
    	CHECK(false == dr->hasTransactionStarted());

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

}

TEST(DBPostgresDriverAccessor, ExecuteUpdateItemAccessor) {
    STRCMP_EQUAL("Statute(ID):2(1)[ID INTEGER ISKEY AUTOGEN STR: INT:0 ](2)[title STRING STR: INT:0 ]",
    		dbStatementStatuteSelect.StatementDetail().c_str());
	Storage::RowItem new_item = dbStatementStatuteSelect.createRowItem();
	new_item.setField(1, 0);
	new_item.setField(2, "Swordsman");
	std::string sqlinsert = dbStatementStatuteSelect.getSQL(Storage::StatementType::INSERT, new_item);
    STRCMP_EQUAL("INSERT INTO Statute(title) VALUES ('Swordsman') RETURNING ID, title", sqlinsert.c_str());

	std::stringstream sql_delete;
	std::stringstream sql_update;

    Storage::RowItem orig_item = dbStatementStatuteSelect.createRowItem();
    Storage::RowItem update_item = dbStatementStatuteSelect.createRowItem();
    Storage::RowItem row;

    /// Add an item to test updates
    try {
        dr->Connect(UserName, UserPassword, UserRole);
        CHECK(true == dr->isConnected());
    	CHECK(false == dr->hasTransactionStarted());

    	// For ExecuteReturn we send our new_item object to Prepare..
    	auto st = dr->CreateStatement(sqlinsert, new_item);
    	CHECK(true == dr->hasTransactionStarted());

    	// Execute Return Insert 1) Insert new hero to edit
    	row = st->ExecuteReturn();
    	std::string value;
    	bool is_set;
    	row.getField(2, value, is_set);
        STRCMP_EQUAL("Swordsman", value.c_str());
        dr->Commit(); // Commit insert..

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    orig_item = row;
    update_item = orig_item;
    update_item.setField(2, "Scout");

    try{
        // 2) Update the hero just added
    	CHECK(false == dr->hasTransactionStarted());
    	sql_update << dbStatementStatuteSelect.getSQL(Storage::StatementType::UPDATE, update_item)
                   << dbStatementStatuteSelect.getWhereSQL(Storage::StatementType::SELECT, orig_item);
    	STRCMP_CONTAINS("UPDATE Statute SET ID =", sql_update.str().c_str());
    	STRCMP_CONTAINS("AND title = 'Swordsman'", sql_update.str().c_str());

    	auto st = dr->CreateStatement(sql_update.str());//, update_item);
    	st->Execute();
    	CHECK(true == dr->hasTransactionStarted());
    	dr->Commit(); // Finish our update and commit transaction (not started = true)

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

    // Cleanup the added item with Delete item.
    try{
    	sql_delete << dbStatementStatuteSelect.getSQL(Storage::StatementType::DELETE, update_item)
    		      << dbStatementStatuteSelect.getWhereSQL(Storage::StatementType::DELETE, update_item);
    	CHECK(false == dr->hasTransactionStarted());
    	auto st = dr->CreateStatement(sql_delete.str());
    	CHECK(true == dr->hasTransactionStarted());
    	// Execute Remove, note no need to get returned DBResult
    	st->Execute();
    	CHECK(true == dr->hasTransactionStarted());
    	dr->Commit();
    	CHECK(false == dr->hasTransactionStarted());
    	STRCMP_CONTAINS("DELETE FROM Statute  WHERE ID =", sql_delete.str().c_str());
    	STRCMP_CONTAINS("AND title = 'Scout'", sql_delete.str().c_str());

    } catch (std::exception& e) {
        STRCMP_EQUAL("", e.what());
    }

}

