#include "storage/storage.h"
#include "DBStatementFixture.h"
#include <string>
#include "CppUTest/TestHarness.h"
#include <thread>
using namespace std;
using namespace std::chrono;

//-------------------------------------------------------
// StorageLibTest group
//-------------------------------------------------------

TEST_GROUP(StorageLibTest)
{
  string userName{"TEST"};
  string userPassword{"pass"};
  string role{"GUEST"};
  string driver{"Sqlite"}, server{"localhost"}, dbname{"test.db"}, charset{"ISO8859_1"};

  Schema schemaMovie{{{"M_ID", FieldType::PK_INT},
                      {"Movie", FieldType::STRING}}};
  MovieAttrs MA;
  TestModelAttrs TA;

  void setup(){};

  void teardown(){};
};

TEST(StorageLibTest, DBStorageModelBeginTransaction)
{
  DatabaseConnection dbConnect{driver, server, dbname, charset};
  Storage::DBStorage dbStorage{dbConnect, "", "", "GUEST"};
  Model movies = dbStorage.model(MA.T.Movies, schemaMovie, CreateKind::DROP_CREATE);
  Model movies2 = dbStorage.model(MA.T.Movies, schemaMovie);

  auto t0 = high_resolution_clock::now();

  try
  {
    dbStorage.beginTransaction();
    for (int i = 50; i < 1500; i++)
    {
      auto e = movies.entity();
      e->set(MA.F.ID, std::to_string(i));
      e->set(MA.F.Movie, ("New movies - [" + std::to_string(i) + "]"));
      e->save();
    }
    for (int i = 1500; i < 3000; i++)
    {
      auto e = movies2.entity();
      e->set(MA.F.ID, std::to_string(i));
      e->set(MA.F.Movie, ("New movies - [" + std::to_string(i) + "]"));
      e->save();
    }
    dbStorage.commit();
  }
  catch (std::exception &e)
  {
    std::cerr << "DBStorageModelBeginTransaction " << e.what() << "\n";
  }

  auto t1 = high_resolution_clock::now();
  auto saveTimeMsecs = duration_cast<microseconds>(t1 - t0).count();
  auto saveTimeSecs = duration_cast<seconds>(t1 - t0).count();
  cout << "Save took " << saveTimeSecs << " seconds [" << saveTimeMsecs << "] msecs\n";

  try
  {
    auto list = movies.find({{MA.F.ID, "51"}});
    auto t2 = high_resolution_clock::now();
    saveTimeMsecs = duration_cast<microseconds>(t2 - t1).count();
    saveTimeSecs = duration_cast<seconds>(t2 - t1).count();
    cout << "Find took: " << saveTimeSecs << " seconds [" << saveTimeMsecs << "] msecs\n";
    LONGS_EQUAL(1, list.size());
  }
  catch (std::exception &e)
  {
    std::cerr << "DBStorageModelBeginThreadBatch2 " << e.what() << "\n";
  }
}

TEST(StorageLibTest, DBStorageModelBeginThreadsDBStorage)
{
  // !!!! Beware Sqlite with many connnections to file is not very reliable with locking database !!!!!
  // Best to sync threads in code and not let more than one thread write.
  // Tried the busy_callback, but the callback can only be set once, etc, etc.

  // autocommit = false (now it alays is autocommit)
  DatabaseConnection dbConnect{driver, server, dbname, charset};
  Storage::DBStorage sdb2{dbConnect, "", "", "GUEST"};
  // Using beginTransaction makes the sql statements not autocommit unitl commit() methods tells to write database.

  // Put in test data of "The Main" movie title so to find this later in find().
  Model moviesCreate = sdb2.model(MA.T.Movies, schemaMovie, CreateKind::DROP_CREATE);
  sdb2.beginTransaction();
  for (int i = 20000; i < 20050; i++)
  {
    auto e = moviesCreate.entity();
    e->set(MA.F.ID, std::to_string(i));
    e->set(MA.F.Movie, ("The Main"));
    e->save();
  }
  sdb2.commit();

  std::thread thread1([this]() {
    try
    {
      DatabaseConnection dbConnect{driver, server, dbname, charset};
      Storage::DBStorage db{dbConnect, "", "", "GUEST"};
      Model movies = db.model(MA.T.Movies, schemaMovie);

      db.beginTransaction();
      for (int i = 50; i < 1500; i++)
      {
        auto e = movies.entity();
        e->set(MA.F.ID, std::to_string(i));
        e->set(MA.F.Movie, ("Terminator"));
        e->save();
      }
      db.commit();
    }
    catch (std::exception &e)
    {
      std::cerr << "DBStorageModelBeginThreadsDBStorage:thread1 " << e.what() << "\n";
    }
    std::cerr << "DBStorageModelBeginThreadsDBStorage thread1 done\n";
  });

  std::thread thread4([this]() {
    try
    {
      DatabaseConnection dbConnect{driver, server, dbname, charset};
      Storage::DBStorage db{dbConnect, "", "", "GUEST"};
      Model movies = db.model(MA.T.Movies, schemaMovie);
      db.beginTransaction();
      auto listAll = movies.find({{MA.F.Movie, "The Main"}});
      db.commit();
      LONGS_EQUAL(50, listAll.size());
    }
    catch (std::exception &e)
    {
      std::cerr << "DBStorageModelBeginThreadsDBStorage:thread2 " << e.what() << "\n";
    }
  });

  thread1.join();
  thread4.join();
}

TEST(StorageLibTest, DBStorageModelSqlNonTransactionAutoCommit)
{
  {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    Storage::DBStorage db{dbConnect};

    Model movies = db.model(MA.T.Movies, schemaMovie, CreateKind::DROP_CREATE);
  }
  auto t0 = high_resolution_clock::now();

  std::thread th1([this]() {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    Storage::DBStorage db{dbConnect};
    Model movies = db.model(MA.T.Movies, schemaMovie);
    db.beginTransaction();
    for (int i = 10000; i < 20000; i++)
    {
      auto e = movies.entity();
      e->set(MA.F.ID, std::to_string(i));
      e->set(MA.F.Movie, ("New movies - [" + std::to_string(i) + "]"));
      e->save();
    }
    db.commit();
    std::cerr << "th1 done\n";
  });

  std::thread th2([this]() {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    Storage::DBStorage db{dbConnect};
    Model movies = db.model(MA.T.Movies, schemaMovie);
    db.beginTransaction();
    for (int i = 20000; i < 30000; i++)
    {
      auto e = movies.entity();
      e->set(MA.F.ID, std::to_string(i));
      e->set(MA.F.Movie, ("New movies - [" + std::to_string(i) + "]"));
      e->save();
    }
    db.commit();
    std::cerr << "th2 done\n";
  });

  {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    Storage::DBStorage db{dbConnect};

    Model movies = db.model(MA.T.Movies, schemaMovie);

    for (int i = 30000; i < 30500; i++)
    {
      auto e = movies.entity();
      e->set(MA.F.ID, std::to_string(i));
      e->set(MA.F.Movie, ("New movies - [" + std::to_string(i) + "]"));
      e->save();
    }
    std::cerr << "Auto sql write statements done (With Begin and Commit)\n";
  }
  th1.join();
  th2.join();
  // No commit sqlite in autocommit

  auto t1 = high_resolution_clock::now();
  auto saveTimeMsecs = duration_cast<microseconds>(t1 - t0).count();
  auto saveTimeSecs = duration_cast<seconds>(t1 - t0).count();
  cout << "Save non transaction took " << saveTimeSecs << " seconds [" << saveTimeMsecs << "] msecs\n";

  t0 = high_resolution_clock::now();

  std::thread th3([this]() {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    Storage::DBStorage db{dbConnect};
    Model movies = db.model(MA.T.Movies, schemaMovie);
    db.beginTransaction();
    for (int i = 40000; i < 50000; i++)
    {
      auto e = movies.entity();
      e->set(MA.F.ID, std::to_string(i));
      e->set(MA.F.Movie, ("one two"));
      e->save();
    }
    std::cerr << "th3 done\n";
    db.commit();
  });

  // This is risky and trying many writes concurrently - sqlite busy callback on locked databases
  std::thread th4([this]() {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    Storage::DBStorage db{dbConnect};
    Model movies = db.model(MA.T.Movies, schemaMovie);
    db.beginTransaction();
    for (int i = 50000; i < 60000; i++)
    {
      auto e = movies.entity();
      e->set(MA.F.ID, std::to_string(i));
      e->set(MA.F.Movie, ("New movies - [" + std::to_string(i) + "]"));
      e->save();
    }
    std::cerr << "th4 done\n";
    db.commit();
  });

  {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    Storage::DBStorage db{dbConnect};
    Model movies = db.model(MA.T.Movies, schemaMovie);

    db.beginTransaction();
    for (int i = 60000; i < 60500; i++)
    {
      auto e = movies.entity();
      e->set(MA.F.ID, std::to_string(i));
      e->set(MA.F.Movie, ("New movies - [" + std::to_string(i) + "]"));
      e->save();
    }
    db.commit();
  }
  th3.join();
  th4.join();

  t1 = high_resolution_clock::now();
  saveTimeMsecs = duration_cast<microseconds>(t1 - t0).count();
  saveTimeSecs = duration_cast<seconds>(t1 - t0).count();
  cout << "Save with transaction took " << saveTimeSecs << " seconds [" << saveTimeMsecs << "] msecs\n";

  {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    Storage::DBStorage db{dbConnect};
    Model movies = db.model(MA.T.Movies, schemaMovie);
    auto list = movies.find({{MA.F.Movie, "one two"}});
    std::cerr << "sqlite non transactional auto commit\n";
    LONGS_EQUAL(10000, list.size());
  }
}

TEST(StorageLibTest, DBStorageModelCreate)
{
  {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    Storage::DBStorage db{dbConnect};

    Model movies = db.model(MA.T.Movies, schemaMovie, CreateKind::DROP_CREATE);
  }

  Schema schemaTest = Schema(
      {{TA.F.ID, FieldType::PK_INT},
       {TA.F.Task, FieldType::STRING},
       {TA.F.Duration, FieldType::STRING},
       {TA.F.Complete, FieldType::INTEGER},
       {TA.F.Time, FieldType::INTEGER}});

  {
    DatabaseConnection dbConnect{driver, server, dbname, charset};
    DBStorage dbStorage{dbConnect};
    Model testModel = dbStorage.model(TA.T.Todo, schemaTest, CreateKind::DROP_CREATE);
    dbStorage.beginTransaction();
    for (int i = 60000; i < 70000; i++)
    {
      auto t1 = testModel.entity();
      t1->set(TA.F.ID, std::to_string(i));
      t1->set(TA.F.Task, "Hello");
      t1->set(TA.F.Duration, "3 days");
      t1->set(TA.F.Complete, "1");
      t1->set(TA.F.Time, "24");
      t1->save();
    }
    dbStorage.commit();
    std::cout << "Todo list saved\n";
    auto list = testModel.find({{TA.F.Task, "Hello"}});

    LONGS_EQUAL(10000, list.size());
    std::cout << "Todo finished\n";

  }
}
