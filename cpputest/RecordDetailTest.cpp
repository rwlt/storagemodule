/*
 * RelationDetailTest.cpp
 *
 *  Created on: 15/07/2017
 *      Author: rodney
 */

#include "storage/RowItem.h"
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
//
//-------------------------------------------------------
// RelationDetailAccessor test group
//-------------------------------------------------------

TEST_GROUP(RelationDetailAccessor) {

    void setup() {
    }
};

TEST(RelationDetailAccessor, Constructor) {
	Storage::RelationDetail rd{};
	Storage::RelationDetail rd_copy{rd};
	STRCMP_EQUAL("", rd.detailed().c_str());
	STRCMP_EQUAL("", rd_copy.detailed().c_str());
}

TEST(RelationDetailAccessor, Deconstructor) {
    Storage::RelationDetail* rd = new Storage::RelationDetail{};
    STRCMP_EQUAL("", rd->detailed().c_str());
    delete rd;
}

TEST(RelationDetailAccessor, ConstructorWithEmptyFields) {
    Storage::RelationDetail rd{};
    Storage::RelationDetail rd_copy{rd};
    STRCMP_EQUAL("", rd.detailed().c_str());
    STRCMP_EQUAL("", rd_copy.detailed().c_str());
}

TEST(RelationDetailAccessor, ConstructorNameAndRelationType) {
    Storage::RelationDetail rd{"ID", Storage::RelationType::ONETOMANY};
    STRCMP_EQUAL("[ID Many ]", rd.detailed().c_str());
}

TEST(RelationDetailAccessor, SetOne2OneAccessor) {
    Storage::RelationDetail rd{"Chest", Storage::RelationType::ONETOONE};
	rd.field_type_pk = Storage::FieldType::INTEGER;
	rd.field_pk = "PersonID";
	rd.field_type_fk = Storage::FieldType::INTEGER;
	rd.field_fk = "ChestID";
	rd.foreign_table = "Chest";
	rd.foreign_pk = "ChestID";
	rd.foreign_fk = "PersonID";
    STRCMP_EQUAL("[Chest One PK(INT) PersonID FK(INT) ChestID  To table Chest PK ChestID FK PersonID]", rd.detailed().c_str());
}

TEST(RelationDetailAccessor, SetOne2ManyAccessor) {
    Storage::RelationDetail rd{"ChestItems", Storage::RelationType::ONETOMANY};
	rd.field_type_pk = Storage::FieldType::INTEGER;
	rd.field_pk = "ChestID";
	rd.field_type_fk = Storage::FieldType::INTEGER;
	rd.field_fk = "ChestID";
	rd.foreign_table = "ChestItems";
	rd.foreign_pk = "ChestItemID";
	rd.foreign_fk = "ChestID";
    STRCMP_EQUAL("[ChestItems Many PK(INT) ChestID FK(INT) ChestID  To table ChestItems PK ChestItemID FK ChestID]", rd.detailed().c_str());
}

TEST(RelationDetailAccessor, AssignmentOperator) {
    Storage::RelationDetail rd{"ID", Storage::RelationType::ONETOMANY};
    Storage::RelationDetail rd_assign;

    rd_assign = rd;
    CHECK(rd == rd_assign);
    rd.field_pk = "New Value";
    CHECK_EQUAL(false, rd == rd_assign);

    Storage::RelationDetail &rd_assign2 = rd;
    CHECK(rd == rd_assign2);
    rd.field_fk = "New Key";
    CHECK_EQUAL(true, rd == rd_assign2);
}

TEST(RelationDetailAccessor, EqualOperator) {
    Storage::RelationDetail rd{"ID", Storage::RelationType::ONETOMANY};
    Storage::RelationDetail rd2{};
    rd.field_pk = "New Value";
    Storage::RelationDetail rd_copy{rd};
    CHECK(rd == rd_copy);
    CHECK_EQUAL(false, rd == rd2);
}

TEST(RelationDetailAccessor, NotEqualOperator) {
    Storage::RelationDetail rd{"ID", Storage::RelationType::ONETOMANY};
    rd.field_pk = "New Value";
    Storage::RelationDetail rd_copy{rd};
    Storage::RelationDetail rd_notequal{rd};
    rd_notequal.field_fk = "New Key"; //

    CHECK_EQUAL(false, rd != rd_copy); // these are equal
    CHECK_EQUAL(false, rd == rd_notequal); // these are not equal
    CHECK(rd != rd_notequal);

}







