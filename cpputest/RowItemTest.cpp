
#include "storage/RowItem.h"
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
//
//-------------------------------------------------------
// RowItemAccessor test group
//-------------------------------------------------------

using Storage::Driver::FieldType;
using Storage::Domain::RowFields;
using Storage::Domain::FieldDetail;
using Storage::Domain::RowItem;

TEST_GROUP(RowItemAccessor) {
    RowFields m_fields;
    FieldDetail fd;
    FieldDetail fd2;

    void setup() {
        fd.field_type = FieldType::INTEGER;
        fd.field_name = "ID";
        fd.int_value = 0; // Empty initialized
        fd.str_value = ""; // Empty initialized

        fd2.field_type = FieldType::STRING;
        fd2.field_name = "Statute";
        fd2.int_value = 0; // Empty initialized
        fd2.str_value = ""; // Empty initialized

        m_fields.insert(std::make_pair(0, fd));
        m_fields.insert(std::make_pair(1, fd2));
    }
};

TEST(RowItemAccessor, Constructor) {
    {
        RowItem ri{
          { {0, { "HI", 1}},
            {1, { "THERE", "Hello"}} }
        };
        RowItem ri_copy{ri};
        LONGS_EQUAL(2, ri.TotalFields());
        CHECK(ri == ri_copy);
    }
}

TEST(RowItemAccessor, Deconstructor) {
    RowItem* ri = new RowItem{};
    LONGS_EQUAL(0, ri->TotalFields());
    delete ri;
}

TEST(RowItemAccessor, ConstructorWithEmptyFields) {
    RowFields m_fields;
    RowItem ri{m_fields};
    RowItem ri_copy{ri};
    LONGS_EQUAL(0, ri.TotalFields());
    CHECK(ri == ri_copy);

    try {
        ri.setField(0, 1);
        ri.setField(1, "Knight");
    } catch (std::exception &e) {
        // Should be caused error - can no access empty fields or set them
        STRCMP_EQUAL("RowItem: out_of_range getFieldDetail index 0", e.what());
    }
}

TEST(RowItemAccessor, ConstructorWithFields) {
    RowItem ri{m_fields};
    RowItem ri_copy{ri};
    LONGS_EQUAL(2, ri.TotalFields());
    CHECK(ri == ri_copy);
}

TEST(RowItemAccessor, SetFieldAccessor) {
    RowItem ri{m_fields};
    int int_value;
    std::string value;

    ri.getField(0, int_value);
    LONGS_EQUAL(0, int_value);

    ri.setField(0, 11); // Hero ID
    ri.getField(0, int_value);
    
    LONGS_EQUAL(11, int_value);

    ri.getField(1, value);
    STRCMP_EQUAL("", value.c_str());

    ri.setField(1, "Knight"); // 
    ri.getField(1, value);
    
    STRCMP_EQUAL("Knight", value.c_str());

    try {
        ri.setField(3, "Smith");
    } catch (std::exception &e) {
        // Should be caused error - can not access the unknown index
        STRCMP_EQUAL("RowItem: out_of_range getFieldDetail index 3", e.what());
    }
    try {
        ri.setField(0, "Smith");
    } catch (std::exception &e) {
        // Should be caused error - can not access the unknown index
        STRCMP_EQUAL("RowItem: out_of_range setField is not STRING type at index 0", e.what());
    }

    try {
        ri.setField(3, 100);
    } catch (std::exception &e) {
        // Should be caused error - can not access the unknown index
        STRCMP_EQUAL("RowItem: out_of_range getFieldDetail index 3", e.what());
    }

    try {
        ri.setField(1, 100);
    } catch (std::exception &e) {
        // Should be caused error - can not access the unknown index
        STRCMP_EQUAL("RowItem: out_of_range setField is not INTEGER type at index 1", e.what());
    }
}

TEST(RowItemAccessor, GetFieldAccessor) {
    RowItem ri{m_fields};
    int int_value;
    std::string value;
    

    ri.setField(0, 11); // Hero ID
    ri.setField(1, "Knight"); // 

    ri.getField(0, int_value);
    
    LONGS_EQUAL(11, int_value);
    ri.getField(1, value);
    
    STRCMP_EQUAL("Knight", value.c_str());

    try {
        ri.getField(3, value);
    } catch (std::exception &e) {
        // Should be caused error - can not access the unknown index
        STRCMP_EQUAL("RowItem: out_of_range getFieldDetail index 3", e.what());
    }

    try {
        ri.getField(0, value);
    } catch (std::exception &e) {
        // Should be caused error - can not access the unknown index
        STRCMP_EQUAL("RowItem: out_of_range getField is not STRING type at index 0", e.what());
    }

    try {
        ri.getField(3, int_value);
    } catch (std::exception &e) {
        // Should be caused error - can not access the unknown index
        STRCMP_EQUAL("RowItem: out_of_range getFieldDetail index 3", e.what());
    }
        

    try {
        ri.getField(1, int_value);
    } catch (std::exception &e) {
        // Should be caused error - can not access the unknown index
        STRCMP_EQUAL("RowItem: out_of_range getField is not INTEGER type at index 1", e.what());
    }

}

TEST(RowItemAccessor, TotalFields) {
    RowItem ri{m_fields};
    LONGS_EQUAL(2, ri.TotalFields());
}

TEST(RowItemAccessor, ItemDetails) {
    RowItem ri{m_fields};
    ri.setField(0, 11); // Hero ID
    ri.setField(1, "Knight"); // 
    LONGS_EQUAL(2, ri.TotalFields());
    int value;
    std::string v2;
    
    ri.getField(0, value);
    LONGS_EQUAL(11, value);
    ri.getField(1, v2);
    STRCMP_EQUAL("Knight", v2.c_str());
}

TEST(RowItemAccessor, AssignmentOperator) {
    RowItem ri{m_fields};
    RowItem ri_assign;

    ri_assign = ri;
    CHECK(ri == ri_assign);
    ri.setField(0, 11); // Hero ID
    ri.setField(1, "Knight"); // 
    CHECK_EQUAL(false, ri == ri_assign);
    
    RowItem &ri_assign2 = ri;
    CHECK(ri == ri_assign2);
    ri.setField(0, 11); // Hero ID
    ri.setField(1, "King"); // 
    CHECK(ri == ri_assign2);
}

TEST(RowItemAccessor, EqualOperator) {
    RowItem ri{m_fields};
    RowItem r2{};
    ri.setField(0, 11); // Hero ID
    ri.setField(1, "Knight"); // 
    RowItem ri_copy{ri};
    CHECK(ri == ri_copy);
    CHECK_EQUAL(false, ri == r2);
}

TEST(RowItemAccessor, NotEqualOperator) {
    RowItem ri{m_fields};
    ri.setField(0, 11); // Hero ID
    ri.setField(1, "Knight"); // 
    RowItem ri_copy{ri};
    RowItem ri_notequal{ri};
    ri_notequal.setField(1, "Warrior"); // 

    CHECK_EQUAL(false, ri != ri_copy); // these are equal
    CHECK_EQUAL(false, ri == ri_notequal); // these are not equal
    CHECK(ri != ri_notequal);

}

