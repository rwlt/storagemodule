#include <iostream>
#include "CppUTest/CommandLineTestRunner.h"

int main(int argc, char **argv)
{
  std::cout << "This is " << argv[0] << std::endl;
  return CommandLineTestRunner::RunAllTests(argc, argv);
}
