#include "storage/Schema.h"
#include "storage/Model.h"
#include "storage/DBStorage.h"
#include "storage/DatabaseConnection.h"
#include "DBStatementFixture.h"
#include <string>
#include "CppUTest/TestHarness.h"

using Storage::DBStorage;
using Storage::Model;
using Storage::Domain::Schema;
using Storage::Driver::CreateKind;
using Storage::Driver::DatabaseConnection;
using Storage::Driver::FieldType;
using Storage::Driver::StorageException;

//-------------------------------------------------------
// ModelFunctionAccess test group
//-------------------------------------------------------

TEST_GROUP(ModelFunctionAccess)
{
  string userName{"TEST"};
  string userPassword{"pass"};
  string role{"GUEST"};
  string driver{"Sqlite"}, server{"localhost"}, dbname{"test.db"}, charset{"ISO8859_1"};
  vector<string> movies{};
  DatabaseConnection dbconnect{driver, server, dbname, charset};
  Schema schemaMovie{{}};
  MovieAttrs MA;
  TestModelAttrs TA;

  void setup()
  {
    movies.push_back("One");
    movies.push_back("2One");
    schemaMovie = Schema(
        {{"M_ID", FieldType::PK_INT},
         {"Movie", FieldType::STRING}});
    DBStorage dbStorage{dbconnect};
    dbStorage.model(MA.T.Movies, schemaMovie, CreateKind::DROP_CREATE);
    create_test_db(dbconnect, userName, userPassword, role, movies);
  };

  void teardown() {}
};

TEST(ModelFunctionAccess, DBStorageModelFunctionWithSchema)
{
  DBStorage dbStorage{dbconnect};
  Storage::Model movies = dbStorage.model(MA.T.Movies, schemaMovie);
  auto entity = movies.entity();
  LONGS_EQUAL(2, entity->size());
}

TEST(ModelFunctionAccess, DBStorageModelFindAll)
{
  DBStorage dbStorage{dbconnect};
  Model movies = dbStorage.model(MA.T.Movies, schemaMovie); //, CreateKind::CREATE_ONLY);
  auto entity = movies.entity();
  LONGS_EQUAL(2, entity->size());
  dbStorage.beginTransaction();
  auto moviesCollection = movies.findAll();
  LONGS_EQUAL(2, moviesCollection.size());

  std::for_each(moviesCollection.begin(), moviesCollection.end(), [=](std::unique_ptr<DBEntity> &movie) -> void {
    std::cout << std::get<0>(movie->get(MA.F.ID)) << " " << std::get<0>(movie->get(MA.F.Movie));
    std::cout << "\n";
  });

  auto e1 = moviesCollection[0].get();
  auto e2 = moviesCollection[1].get();
  STRCMP_EQUAL("One", std::get<0>(e1->get(MA.F.Movie)).c_str());
  STRCMP_EQUAL("1", std::get<0>(e2->get(MA.F.ID)).c_str());
  STRCMP_EQUAL("2One", std::get<0>(e2->get(MA.F.Movie)).c_str());
}

TEST(ModelFunctionAccess, DBStorageModelFindWhere)
{
  DBStorage dbStorage{dbconnect};
  Model movies = dbStorage.model(MA.T.Movies, schemaMovie);

  auto moviesCollection = movies.find({{MA.F.Movie, "2One"}});
  LONGS_EQUAL(1, moviesCollection.size());
  auto e1 = moviesCollection[0].get();
  STRCMP_EQUAL("1", std::get<0>(e1->get(MA.F.ID)).c_str());
  STRCMP_EQUAL("2One", std::get<0>(e1->get(MA.F.Movie)).c_str());
}

TEST(ModelFunctionAccess, DBStorageModelFindWhereNoFind)
{
  DBStorage dbStorage{dbconnect};
  Model movies = dbStorage.model(MA.T.Movies, schemaMovie);

  auto moviesCollection = movies.find({{MA.F.Movie, "2DDDDDOne"}});
  LONGS_EQUAL(0, moviesCollection.size());
  CHECK_THROWS(StorageException, { auto moviesCollection2 = movies.find({{"DDDDDD", "2DDDDDOne"}}); });
}

TEST(ModelFunctionAccess, DBStorageModelEntitySetAccessor)
{
  DBStorage dbStorage{dbconnect};
  Model movies = dbStorage.model(MA.T.Movies, schemaMovie);
  auto entity = movies.entity();
  LONGS_EQUAL(2, entity->size());

  entity->set(MA.F.Movie, "New movie");
  STRCMP_EQUAL("New movie", std::get<0>(entity->get(MA.F.Movie)).c_str());

  CHECK_THROWS(std::out_of_range, { entity->set(MA.F.ID, "x12"); });
}

TEST(ModelFunctionAccess, DBStorageModelSaveAndCheck)
{
  DBStorage dbStorage{dbconnect};
  Model movies = dbStorage.model(MA.T.Movies, schemaMovie);
  auto entity = movies.entity();
  LONGS_EQUAL(2, entity->size());

  entity->set(MA.F.Movie, "New movie");
  entity->set(MA.F.ID, "213");

  entity->save();

  auto moviesCollection = movies.find({{MA.F.Movie, "New movie"}});
  LONGS_EQUAL(1, moviesCollection.size());

  auto e1 = moviesCollection[0].get();
  STRCMP_EQUAL("213", std::get<0>(e1->get(MA.F.ID)).c_str());
  STRCMP_EQUAL("New movie", std::get<0>(e1->get(MA.F.Movie)).c_str());
}

TEST(ModelFunctionAccess, DBStorageModelCreate)
{

  Schema schemaTest = Schema(
      {{TA.F.ID, FieldType::PK_INT},
       {TA.F.Task, FieldType::STRING},
       {TA.F.Duration, FieldType::STRING},
       {TA.F.Complete, FieldType::INTEGER},
       {TA.F.Time, FieldType::INTEGER}});

  DBStorage dbStorage{dbconnect};
  Model testModel = dbStorage.model(TA.T.Todo, schemaTest, CreateKind::DROP_CREATE);
  auto t1 = testModel.entity();
  auto t2 = testModel.entity();
  try
  {
    t1->set(TA.F.ID, "100");
    t1->set(TA.F.Task, "Hello");
    t1->set(TA.F.Duration, "3 days");
    t1->set(TA.F.Complete, "1");
    t1->set(TA.F.Time, "24");
    t1->save();
    t2->set(TA.F.ID, "110");
    t2->set(TA.F.Task, "Time check");
    t2->set(TA.F.Duration, "5 days");
    t2->set(TA.F.Complete, "0");
    t2->set(TA.F.Time, "12");
    t2->save();
    auto find = testModel.find({{TA.F.Complete, "1"}});
    LONGS_EQUAL(1, find.size());
    STRCMP_EQUAL("Hello", std::get<0>(find[0]->get(TA.F.Task)).c_str());
  }
  catch (std::exception &e)
  {
    std::cout << "DBStorageModelCreate " << e.what() << "\n";
  }
}

TEST(ModelFunctionAccess, DBStorageModelUpdate)
{

  Schema schemaTest = Schema(
      {{TA.F.ID, FieldType::PK_INT},
       {TA.F.Task, FieldType::STRING},
       {TA.F.Duration, FieldType::STRING},
       {TA.F.Complete, FieldType::INTEGER},
       {TA.F.Time, FieldType::INTEGER}});

  DBStorage dbStorage{dbconnect};
  Model testModel = dbStorage.model(TA.T.Todo, schemaTest, CreateKind::DROP_CREATE);
  auto t1 = testModel.entity();
  auto t2 = testModel.entity();
  try
  {
    // New item added
    t1->set(TA.F.ID, "100");
    t1->set(TA.F.Task, "Hello");
    t1->set(TA.F.Duration, "3 days");
    t1->set(TA.F.Complete, "1");
    t1->set(TA.F.Time, "24");
    t1->save();

    // retrieve t1
    auto find = testModel.find({{TA.F.ID, "100"}});
    LONGS_EQUAL(1, find.size());
    STRCMP_EQUAL("Hello", std::get<0>(find[0]->get(TA.F.Task)).c_str());
    STRCMP_EQUAL("3 days", std::get<0>(find[0]->get(TA.F.Duration)).c_str());

    // Save again should update the current t1 in DB with a change
    auto toChange = std::move(find[0]);

    toChange->set(TA.F.Duration, "2.5 days");
    STRCMP_EQUAL("3 days", std::get<0>(toChange->getRetreived(TA.F.Duration)).c_str());
    toChange->save();

    find = testModel.find({{TA.F.ID, "100"}});
    LONGS_EQUAL(1, find.size());
    STRCMP_EQUAL("2.5 days", std::get<0>(find[0]->get(TA.F.Duration)).c_str());
  }
  catch (std::exception &e)
  {
    std::cout << "DBStorageModelCreate " << e.what() << "\n";
  }
}
