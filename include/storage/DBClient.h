#ifndef STORAGE_DB_CLIENT_MODEL_H_
#define STORAGE_DB_CLIENT_MODEL_H_

#include "storage/DBDatabase.h"
#include "storage/DBDriver.h"
#include "storage/RowItem.h"
#include "storage/Schema.h"
#include "storage/StorageException.h"

#include <string>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <tuple>
#include <atomic>
#include <vector>

using Storage::Domain::DBRetrieveMap;
using Storage::Domain::RowFields;
using Storage::Domain::RowItem;
using Storage::Domain::Schema;
using Storage::Driver::DBDriver;
using Storage::Driver::DBRowItem;
using Storage::Driver::DBStatement;
using Storage::Driver::StatementType;

namespace Storage
{

  /// Knows how to read and write a database for a set of tables
  class DBClient : public DBDatabase
  {
  protected:
    DBDriver *m_dbdr;
    //bool m_autocommit{true};

    // std::atomic<bool> m_batch{false};
    // std::atomic<bool> m_pause{false};
    // mutable std::mutex m_DBCallMutex{};
    // mutable std::mutex m_DBBatchMutex{};
    // mutable std::mutex m_DBBeginBatchMutex{};
    // mutable std::mutex m_DBEndBatchMutex{};
    // std::mutex m_DBPauseMutex;
    // std::condition_variable m_cvPause;
    static int tries;

  public:
    /// DBClient
    /**
	 * \param dbDr 
	 */
    DBClient(DBDriver *dbDr);//, bool autocommit = {true});

    /// Virtual deconstructor
    virtual ~DBClient();

    //DBClient(DBClient &other) = delete;
    //void operator=(const DBClient &) = delete;

    // void pauseClient();
    // void resumeClient();
    // // /// Reset the DBDriver (reconnection of DBStorage is only cause)
    // void resetDriver(DBDriver *dbdr, bool autocommit);
    // void beginBatch();
    // void endBatch();
    virtual void ddlTableStatement(
        const std::string &tableName,
        const Schema &schema,
        StatementType stype) const;

    /// Read the db storage
    std::vector<std::unique_ptr<DBEntity>> onFindAll(
        const std::string &entityName,
        const Schema &schema,
        OnCreateEntity createEntity,
        DBRetrieveMap where,
        const DBRetrieveMap &parameters);

    /// Add a new item in the db storage
    /**
	 * \param tableName 
	 * \param new_item 
	 * \return RowItem (updated)
	 */
    std::unique_ptr<DBRowItem> doAddItem(
        const std::string &entityName,
        const Schema &schema,
        const RowFields &fields);

    /// Add a new item in the db storage
    /**
	 * \param original_item reference to T()
	 * \param update_item reference to T()
	 * \return RowItem (updated)
	 */
    std::unique_ptr<DBRowItem> doUpdateItem(
        const std::string &entityName,
        const Schema &schema,
        const RowFields &original,
        const RowFields &update);
    /// Add a new item in the db storage
    /**
	 * \param tableName 
	 * \param remove_item 
	 */
    void doRemoveItem(const std::string &tableName, const RowItem &remove_item) const;

    virtual void beginTransaction();
    virtual void commit();

  private:
    inline static std::string toUppercase(const char *name);
    int rowCount(const std::string &entityName, DBRetrieveMap where = {}) const;

    std::unique_ptr<DBStatement> createStatement(
        const std::string entityName,
        const Schema &schema,
        StatementType stype, // SELECT or COUNT
        DBRetrieveMap where = {}) const;

    std::unique_ptr<DBStatement> createInsertStatement(
        const std::string entityName,
        const Schema &schema,
        const RowFields &fields) const;

    std::unique_ptr<DBStatement> createUpdateStatement(
        const std::string entityName,
        const Schema &schema,
        const RowFields &original,
        const RowFields &fields) const;

    RowItem whereCriteria(DBRetrieveMap &where) const;
  };

} /* namespace Storage */

#endif /* STORAGE_DB_CLIENT_MODEL_H_ */
