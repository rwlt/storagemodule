/*
 * DBDriver.h
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DRIVER_DB_DRIVER_H_
#define SRC_STORAGE_DRIVER_DB_DRIVER_H_

#include "storage/DBResult.h"
#include "storage/DBStatement.h"
#include "storage/DatabaseConnection.h"
#include "storage/DBRowItem.h"
#include "storage/DBSchema.h"
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace Storage::Driver
{

  /// DBDriver interface
  class DBDriver
  {
  protected:
    DatabaseConnection m_db_connect; ///< DatabaseConnection
    //bool m_isConnected{false};       ///< is it connected?
                                     // bool m_hasTransactionStarted{false}; ///< has transaction started?

  public:
    /// Constructor
    /**
	 * \param db_connect database name
	 */
    explicit DBDriver(DatabaseConnection db_connect) : m_db_connect{db_connect} {};
    virtual ~DBDriver() = default;
    ;

    /// Create DBDriver
    /**
	 * \param db_connect database name
   * \param user
   * \param password
   * \param role
	 * \return shared_ptr<DBDriver>
	 */
    static std::unique_ptr<DBDriver> CreateDBDriver(
        const DatabaseConnection &db_connect,
        const std::string &user = {},
        const std::string &password = {},
        const std::string &role = {});
    //   /// Connect to DB
    //   /**
    //  * \param user
    //  * \param password
    //  * \param role
    //  */
    //   virtual void Connect(const std::string &user, const std::string &password, const std::string &role) = 0;
    //   /// Connection to DB made
    /**
	 * \return bool
	 */
  //  virtual bool isConnected() = 0;

    /// Prepare with only sql string
    /**
	 * \param sql
	 * \return DBStatement unique ptr
	 */
    virtual std::unique_ptr<DBStatement> CreateStatement(
        const std::string &sql) = 0;

    /// Prepare with rowitem placeholders data
    /**
	 * \param schema DBSchema abstract interface
	 * \param sql
	 * \return DBStatement unique ptr
	 */
    virtual std::unique_ptr<DBStatement> CreateStatement(
        const DBSchema &schema,
        const std::string &sql) = 0;

    /// base sql statement builder
    /**
   * The Driver has a SQL builder for DatabaseStatement class is a base to build SQL
   */
    //virtual std::string buildStatement(const std::string entityName, )

    /// Create Select statement - is either SELECT or COUNT statement type
    virtual std::unique_ptr<DBStatement> CreateStatement(
        const std::string entityName,
        const DBSchema &schema,
        const DBRowItem *where = {nullptr},
        const StatementType statementType = StatementType::SELECT) = 0;

    // virtual std::unique_ptr<DBStatement> CreateDDLStatement(
    //   const std::string entityName,
    //   const DBSchema &schema,
    //   const DBRowItem *where,
    //   const StatementType statementType) = 0;

    virtual std::unique_ptr<DBStatement> CreateInsertStatement(
        const std::string entityName,
        const DBSchema &schema,
        const DBRowItem &row) = 0;

    virtual std::unique_ptr<DBStatement> CreateUpdateStatement(
        const std::string entityName,
        const DBSchema &schema,
        const DBRowItem &original,
        const DBRowItem &row) = 0;

    /// Prepare with rowitem placeholders data
    /**
	 * \param sql
	 * \return DBStatement unique ptr
	 */
    // virtual std::unique_ptr<DBStatement> CreateStatement(
    //     const std::string &sql,
    //     DBRowItem * dbRowItem = nullptr) = 0;

    // Begin Transaction
    virtual void BeginTransaction() = 0;
    /// has a transaction started
    /**
	 * \return bool
	 */
    virtual bool hasTransactionStarted() = 0;

    /// Commit
    virtual void Commit() = 0;

    /// SQL Value
    /**
         * \param entityName
         * \param item db row item for field values for sql
         * \param stype enum of statement type
         * \param dboType
         * \return string of the SQL for the statement
         */
    virtual std::string getSQL(
        const std::string &entityName,
        const DBRowItem &item,
        StatementType stype,
        DBOType dboType = DBOType::TABLE);

    virtual std::string getSQLCount(
        const std::string &entityName,
        const DBRowItem &item,
        DBOType dboType = DBOType::TABLE);

    virtual std::string getWhereSQL(StatementType stype, const DBRowItem &item);
    /// get statement Fields Detail list
    /**
         * \param stype enum of statement type
         * \param item row item for field values for where sql
         * \return string
         * */
    virtual std::string statementFieldDetail(StatementType stype, const DBRowItem &item);
    /// get statement Fields Detail list
    /**
         * \param item row item for field values for where sql
         * \return string
         * */
    virtual std::string statementFieldValueDetail(const DBRowItem &item);
    /// get statement Fields Returning Detail list
    /**
         * \param stype enum of statement type
         * \param item row item for field values for where sql
         * \return string
         * */
    virtual std::string statementReturningFieldDetail(StatementType stype, const DBRowItem &item);
    /// get statement Where Fields Detail list
    /**
         * \param item item for field value details
         * \return string
         * */
    virtual std::string statementWhereFieldDetail(const DBRowItem &item);

    virtual std::string columnStatement(
        StatementType stype,
        const DBRowItem &item,
        unsigned short index,
        unsigned short &made_count);

    virtual std::string parameterValues();
    /// add ParameterName and index
    /**
         * New parameters added will be initialized with initialized state.
         * No values are set and flagged is_set is false.
         * The parameter is added to MapParameters.
         * This is used only for Procedure database objects.
         * \param index order found and its index value for parameter
         * \param name const reference to string
         * \param field_type const reference to string
         * */
    virtual void setParameter(unsigned short index, const std::string &name,
                              FieldType field_type);

    // Helper function to drop table
    //static void drop_table(DBDriver *dr, const std::string &table_name, const std::string &sql_drop);
  };

} /* namespace Storage */

#endif /* SRC_STORAGE_DRIVER_DB_DRIVER_H_ */
