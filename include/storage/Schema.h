/*
 * Schema.h
 */

#ifndef D_SCHEMA_H_
#define D_SCHEMA_H_

#include "storage/RowItemType.h"
#include "storage/StorageType.h"
#include "storage/DBSchema.h"
#include "storage/RowItem.h"
#include <unordered_map>
#include <string>
#include <iostream>

using Storage::Driver::FieldType;
using Storage::Driver::DBSchema;
/**
 * Storage Schema
 */
namespace Storage::Domain {

  /// Schema abstract
  /**
   * Schema for database table.
    * The fields for table are RowFields and are setup using 
    * Schema constructor(collection of RowFields).
    * Derived instance of Entity (the table item type) is used a generic parameter
    *  
    *
    * \sa Schema constructer.
    *
    */

  using SchemaField = std::tuple<std::string, FieldType>;
  using SchemaFields = std::vector<SchemaField>;
  using FieldNameMap = std::unordered_map<std::string, unsigned short>;

  class Schema : public DBSchema {
    private:
      //SchemaFields m_fields{};
      RowFields m_fields{}; // Initial empty is fine
      FieldNameMap m_fieldNameIndex{};

    public:
        /// Constructor
        /**
         * \param fields map with index short int key and tuple of field name and type
         * \param dboType type of dbo (default is TABLE)
         */
        explicit Schema(SchemaFields fields);

        /// Virtual deconstructor
        virtual ~Schema();

        const RowFields &rowFields() const {
          return m_fields;
        };

        const FieldNameMap &fieldNameIndex() const {
          return m_fieldNameIndex;
        };

        std::unique_ptr<DBRowItem> createRow() const {
          return std::unique_ptr<DBRowItem>(new RowItem(m_fields));
        };


      private:
        void setField(
          unsigned short index,
          const std::string &field,
          FieldType field_type);
    };

} /* namespace Storage */

#endif /* D_SCHEMA_H_ */
