#ifndef STORAGE_EXCEPTION_H_
#define STORAGE_EXCEPTION_H_

#include <exception>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <ctime>

namespace Storage::Driver
{

/// Standard exception override
class StorageException: public std::exception
{
	//std::ofstream outfile;

public:
    /// Constructor
	StorageException ( std::string what ) :reason ( what ) {
		std::ofstream outfile;
		outfile.open("/var/log/storage/storage.log", std::ios_base::app);
		if (outfile.good()) {
			outfile << "Error: " << what << "\n";
		}
		outfile.close();

	}
    /// Virtual Deconstructor
    virtual ~StorageException() {
		//outfile.close();
    }
    /// What went wrong
    const char *what() const noexcept {
        return ( reason.c_str() );
    }

private:
    std::string reason;

};

} /* namespace Storage */

#endif /* STORAGE_EXCEPTION_H_ */
