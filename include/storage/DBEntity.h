/*
 * DBEntity.h
 */

#ifndef D_DBENTITY_H_
#define D_DBENTITY_H_

#include "storage/StorageType.h"
#include <string>
#include <tuple>
/**
 * Storage Schema
 */

using Storage::Driver::FieldType;

namespace Storage
{

  /// DBDatabase abstract (base to DBStorage)
  /**
   * Abstract for DBStorage for working with Schema
    * \sa Schema constructer.
    *
    */

  class DBEntity
  {

  public:
    /// Constructor
    /**
         * \param entityName const reference of string
          */
    DBEntity();

    /// Virtual deconstructor
    virtual ~DBEntity();

    virtual std::tuple<std::string, FieldType> get(const std::string &name) const = 0;
    virtual std::tuple<std::string, FieldType> getRetreived(const std::string &name) const = 0;

    /// Set field with name
    /**
         * \param name
         * \param value
         * \throw std::out_of_range
         */
    virtual void set(const std::string &name, const std::string &value) = 0;

    virtual int size() const = 0;

    virtual void save() = 0;

  };

} /* namespace Storage */

#endif /* D_DBENTITY_H_ */
