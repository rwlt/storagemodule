#ifndef D_DBSCHEMA_H
#define D_DBSCHEMA_H

#include "storage/StorageType.h"
#include "storage/DBRowItem.h"
#include <memory>

namespace Storage::Driver
{
  class DBSchema
  {
  public:
    virtual ~DBSchema();

    virtual std::unique_ptr<DBRowItem> createRow() const = 0;
  };

}

#endif /* D_DBSCHEMA_H */