#ifndef D_ROW_ITEM_TYPE_H
#define D_ROW_ITEM_TYPE_H

#include "storage/StorageType.h"
#include <unordered_map>
#include <sstream>

using Storage::Driver::FieldType;
using Storage::Driver::RelationType;

namespace Storage::Domain
{
  /// Field detail mapped with index of field
  struct FieldDetail
  {
    /// Default constructor
    FieldDetail(){};
    /// Constructor
    /**
    	 * Construct an FieldType::INTEGER field details
    	 * \param name
    	 * \param value
    	 */
    FieldDetail(std::string name, int value)
    {
      field_name = std::move(name);
      int_value = value;
      field_type = FieldType::INTEGER;
    }
    /// Constructor
    /**
    	 * Construct an FieldType::STRING field details
    	 * \param name
    	 * \param value
    	 */
    FieldDetail(std::string name, std::string value)
    {
      field_name = std::move(name);
      str_value = std::move(value);
      field_type = FieldType::STRING;
    }

    std::string field_name{""};              ///< field name
    FieldType field_type{FieldType::STRING}; ///< field type
    int int_value{0};                        ///< int value
    std::string str_value{""};               ///< string value

    /// details of the FieldDetail structure
    /**
         * \return string
         * */
    std::string detailed() const
    {
      std::stringstream value;
      value << "[" << field_name << " ";
      switch (field_type)
      {
      case FieldType::INTEGER:
      case FieldType::PK_INT:
      case FieldType::AUTOGENINT:
        value << "INTEGER ";
        break;
      case FieldType::STRING:
        value << "STRING ";
        break;
      default:
        /* nothing */
        break;
      }
      value << "STR:" << str_value << " ";
      value << "INT:" << int_value << " ";
      value << "]";
      return value.str();
    }

    /// Equality operator
    /**
         * \param rhs right hand side
         * \return true if equal
         */
    bool operator==(const FieldDetail &rhs) const
    {
      return field_name == rhs.field_name &&
             field_type == rhs.field_type &&
             int_value == rhs.int_value &&
             str_value == rhs.str_value;
    }
    /// Inequality operator
    /**
         * \param rhs right hand side
         * \return true if not equal
         */
    bool operator!=(const FieldDetail &rhs) const
    {
      return !(*this == rhs);
    }
  };

  /// Relation detail mapped with index of field

  struct RelationDetail
  {
    /// Default constructor
    RelationDetail(){};
    /// Constructor
    /**
    	 * Construct an FieldType::INTEGER field details
    	 * \param name
    	 * \param value
    	 */
    RelationDetail(std::string name, RelationType value)
    {
      field_name = std::move(name);
      relation_type = value;
    }

    std::string field_name{""};                                  ///< field name -
    RelationType relation_type{RelationType::ONETOONE}; ///< relation type
    FieldType field_type_pk{FieldType::INTEGER};        ///< field type of foreign key
    FieldType field_type_fk{FieldType::INTEGER};        ///< field type of foreign key
    std::string field_pk{""};                                    ///< primary key
    std::string field_fk{""};                                    ///< foreign key
    std::string foreign_table{""};                               ///< foreign table
    std::string foreign_pk{""};                                  ///< foreign primary key
    std::string foreign_fk{""};                                  ///< foreign foreign key

    /// details of the FieldDetail structure
    /**
         * \return string
         * */
    std::string detailed() const
    {
      std::stringstream value;
      if (field_name != "")
      {
        value << "[" << field_name << " ";
        switch (relation_type)
        {
        case RelationType::ONETOONE:
          value << "One ";
          break;
        case RelationType::ONETOMANY:
          value << "Many ";
          break;
        default:
          /* nothing */
          break;
        }
        if (field_pk != "")
        {
          value << "PK";
          switch (field_type_pk)
          {
          case FieldType::INTEGER:
            value << "(INT) ";
            break;
          case FieldType::STRING:
            value << "(STR) ";
            break;
          default:
            /* nothing */
            break;
          }
          value << field_pk << " ";
          value << "FK";
          switch (field_type_fk)
          {
          case FieldType::INTEGER:
            value << "(INT) ";
            break;
          case FieldType::STRING:
            value << "(STR) ";
            break;
          default:
            /* nothing */
            break;
          }
          value << field_fk << " ";
          value << " To table " << foreign_table << " ";
          value << "PK " << foreign_pk << " ";
          value << "FK " << foreign_fk;
        }
        value << "]";
      }
      return value.str();
    }

    /// Equality operator
    /**
         * \param rhs right hand side
         * \return true if equal
         */
    bool operator==(const RelationDetail &rhs) const
    {
      return field_name == rhs.field_name &&
             relation_type == rhs.relation_type &&
             field_type_pk == rhs.field_type_pk &&
             field_type_fk == rhs.field_type_fk &&
             field_pk == rhs.field_pk &&
             field_fk == rhs.field_fk &&
             foreign_table == rhs.foreign_table &&
             foreign_pk == rhs.foreign_pk &&
             foreign_fk == rhs.foreign_fk;
    }
    /// Inequality operator
    /**
         * \param rhs right hand side
         * \return true if not equal
         */
    bool operator!=(const RelationDetail &rhs) const
    {
      return !(*this == rhs);
    }
  };

  ///Row fields by index so to field name and type
  typedef std::unordered_map<unsigned short, FieldDetail> RowFields;
  ///Row fields by index so to field name and type value type for iterating
  typedef std::unordered_map<unsigned short, FieldDetail>::value_type RowFields_value_type;
  //typedef std::unordered_map<unsigned short, FieldDetail>::key_type RowField_key_type;
  typedef std::unordered_map<unsigned short, FieldDetail>::const_iterator RowFields_citerator;

  /// Map of field details to field name by user
  typedef std::vector<FieldDetail> DBRetrieveMap;

}

#endif /* D_ROW_ITEM_TYPE_H */