#ifndef STORAGE_DB_STORAGE_H_
#define STORAGE_DB_STORAGE_H_

#include "storage/DBClient.h"
#include "storage/DBDriver.h"
#include "storage/Schema.h"
#include "storage/Model.h"
#include "storage/StorageException.h"

#include <vector>
#include <unordered_map>
#include <string>
#include <memory>
#include <mutex>
#include <tuple>
#include <atomic>

using Storage::Driver::ConnectionType;
using Storage::Driver::CreateKind;
using Storage::Driver::DatabaseConnection;
using Storage::Driver::DBDriver;

using Storage::Domain::Schema;
using Storage::Driver::StatementType;

namespace Storage
{

  /// Knows how to read and write a database for a set of tables
  constexpr int POOLSIZE = 4;
  constexpr int MAXMODELS = 10;

  class DBStorage
  {
    DatabaseConnection m_dbconnect{"", "", "", ""};
    std::string m_UserName{""};
    std::string m_UserPassword{""};
    std::string m_UserRole{""};
    ConnectionType m_connectType{ConnectionType::LIVE};
    //bool m_autocommit{true};
    // Pool of connection
    std::array<std::unique_ptr<DBDriver>, POOLSIZE> m_dbdr{nullptr};

    int m_modelID{0};
    std::array<Model, MAXMODELS> m_modelList{};
    std::unique_ptr<DBClient> m_dbClient{nullptr};

  public:
    /// Create with a storage version so we can use the same tables in the version
    /**
	 * \param dbconnect 
	 * \param user (optional)
	 * \param password (optional)
	 * \param role (optional)
	 */
    DBStorage(DatabaseConnection connect,
              const std::string user = "",
              const std::string password = "",
              std::string role = "GUEST");

    /// Virtual deconstructor
    virtual ~DBStorage();

    DBStorage(DBStorage &other) = delete;
    void operator=(const DBStorage &) = delete;

    /// model - function to Create model object with Model constructor
    /**
   * The passed schema is given in the Model constructor
   * \param entityName const reference of string
   * \param schema reference to schema
   * 
   * \return Model 
   */
    Model model(const std::string &entityName, const Schema fields, CreateKind createKind = CreateKind::NONE);

    void beginTransaction();
    void commit();
  };

} /* namespace Storage */

//extern Storage::DBStorage dbStorage;

#endif /* DBSTORAGE_H_ */
