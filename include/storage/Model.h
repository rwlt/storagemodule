#ifndef D_MODEL_H
#define D_MODEL_H

#include "storage/DBModel.h"
#include "storage/DBDatabase.h"
#include "storage/RowItem.h"
#include "storage/Schema.h"
#include "storage/Entity.h"
#include "storage/DBClient.h"
#include <string>
#include <functional>
#include <mutex>

using namespace std::placeholders;
using Storage::Entity;

namespace Storage
{

    // Model callable events - onCreateEntity(DbRowItem)
  std::unique_ptr<DBEntity> defaultModelOnCreateEntityEvent(const DBRowItem &dbRowItem);

  /// Model
  /**
   */
  class Model :  protected DBClient, public DBModel
  {
    int m_modelID{0};
    std::string m_entityName{""};
    Schema m_schema{{}}; // Composed of one Schema when created.
    Entity::OnSave m_onSaveEventFunc{defaultEntityOnSaveEventFunc};
    Entity::OnUpdate m_onUpdateEventFunc{defaultEntityOnUpdateEventFunc};
    OnCreateEntity m_onCreateEntity{defaultModelOnCreateEntityEvent};

  public:
    Model(
        DBDriver* dbdr,
        int modelID,
        const std::string &entityName,
        const Schema schema);
    Model();

    // Model(const Model &other);
    // Model& operator=(const Model &);

    virtual ~Model();

    /// Object entity Name
    /**
     * \return std::string
      */
    std::string entityName() const
    {
      return m_entityName;
    };

    /// Schema
    /**
     * \return schema
      */
    const Schema &schema() const
    {
      return m_schema;
    }
    /// Create a row with this statements MapFields in an initial state
    /**
   * The Entity has a RowItem in an initialized state with no values set.
    * Just a RowItem with number of 0 to N fields and their data type ready
    * to be set a value.
    * Not all fields need to be set, and leaving them unset keeps them seen
    * as empty with no value set.
    * \return RowItem value type
    */
    //std::unique_ptr<Storage::DBEntity> entity(const RowFields &rowFields);
    std::unique_ptr<Storage::DBEntity> onCreateEntity(const DBRowItem &dbRowItem) ;
    std::unique_ptr<Storage::DBEntity> entity();

    std::unique_ptr<DBRowItem> save(const RowFields &fields) ;
    std::unique_ptr<DBRowItem> update(const RowFields &original, const RowFields &update) ;

    /// FindAll
    /**
     * \return collection of Entity
     */
    std::vector<std::unique_ptr<DBEntity>> findAll() ;

    std::vector<std::unique_ptr<DBEntity>> find(DBRetrieveMap where) ;
  };

}

#endif /* D_MODEL_H */
