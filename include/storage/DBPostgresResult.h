/*
 * DBPostgresResult.h
 *
 *  Created on: 30/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DRIVER_DBPOSTGRESRESULT_H_
#define SRC_STORAGE_DRIVER_DBPOSTGRESRESULT_H_

#include "storage/DBResult.h"
#include <pqxx/pqxx>

namespace Storage {
namespace Driver {

/// DBPostgresResult
class DBPostgresResult: public DBResult {
	int m_rowDone{0};
	pqxx::result m_result;

public:
	/// Constructor
	/**
	 * \param result pqxx::result
	 */
	DBPostgresResult(pqxx::result &result):
	   m_result{result} {
	}
	virtual ~DBPostgresResult() {
	}
	/// Fetch
	/**
	 * \param row Storage::RowItem
	 * \return bool of more to fetch
	 */
	virtual bool Fetch(Storage::RowItem &row) override;

};

} /* namespace Driver */
} /* namespace Storage */

#endif /* SRC_STORAGE_DRIVER_DBPOSTGRESRESULT_H_ */
