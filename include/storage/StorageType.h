/*
 * StorageType.h
 */

#ifndef D_STORAGETYPE_H_
#define D_STORAGETYPE_H_

#include <unordered_map>
#include <string>
#include <vector>

/**
 * Storage Schema
 */
namespace Storage::Driver {

  /**
   * Specify a StatementType in method to get SQL for it.
    */
  enum class StatementType : char {
      SELECT = 1, ///< Select
      COUNT = 2, ///< Select count rows
      INSERT = 3, ///< Insert
      UPDATE = 4, ///< Update sql
      REMOVE = 5,
      DROPTABLE = 6,
      CREATETABLE = 7
  };

  /**
   * Specify a DBOType in method to get SQL for it.
    */
  enum class DBOType : char {
      TABLE = 't', ///< Table
      PROCEDURE = 'p', ///< Procedure
  };

  enum class CreateKind : char {
      DROP_CREATE = 1, ///< Drop and Create Table
      CREATE_ONLY = 2, ///< Create Only
      NONE = 3
  };


  /// FieldType
  enum class FieldType : char
  {
    STRING = 1,    ///< String std::string
    INTEGER = 2,   ///< Integer int
    AUTOGENINT = 3, ///< Auto generated integer
    PK_INT = 4 ///< Auto generated integer
    //FK_INT = 5 ///< Auto generated integer
  };
 
  /// Relation type of relation
  enum class RelationType : char
  {
    ONETOONE = 1, ///< String std::string
    ONETOMANY = 2 ///< Integer int
  };

} /* namespace Storage */

#endif /* D_SCHEMA_H_ */
