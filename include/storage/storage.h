#ifndef D_STORAGE_LIB_H_
#define D_STORAGE_LIB_H_

#include "storage/StorageType.h"
#include "storage/Schema.h"
#include "storage/DBStorage.h"
#include "storage/Model.h"

/**
 * Storage::DBStorage provides extern Storage::dbStorage
 */

//extern Storage::DBStorage dbStorage; 

using Storage::Model;
using Storage::Driver::StorageException;

#endif