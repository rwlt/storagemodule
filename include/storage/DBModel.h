/*
 * DBModel.h
 */

#ifndef D_DBMODEL_H_
#define D_DBMODEL_H_

#include "storage/DBEntity.h"
#include "storage/RowItemType.h"
#include "storage/Schema.h"
#include <unordered_map>
#include <string>
#include <memory>
#include <functional>

using Storage::Domain::Schema;
using Storage::Domain::RowFields;
/**
 * Storage Schema
 */
namespace Storage
{
  typedef std::function<std::unique_ptr<DBEntity>(const DBRowItem &dbRowItem)> OnCreateEntity;

  /// DBDatabase abstract (base to DBStorage)
  /**
   * Abstract for DBStorage for working with Schema
    * \sa Schema constructer.
    *
    */

  class DBModel
  {

  public:
    /// Constructor
    /**
         * \param entityName const reference of string
          */
    DBModel();

    /// Virtual deconstructor
    virtual ~DBModel();

    virtual std::string entityName() const = 0;
    virtual const Schema &schema() const = 0;

    /// abstract entity create - model needs know how to create its concrete Entity
   // virtual std::unique_ptr<Storage::DBEntity> entity(const RowFields &rowFields) = 0;
    virtual std::unique_ptr<DBEntity> onCreateEntity(const DBRowItem &dbRowItem) = 0;
    virtual std::unique_ptr<DBEntity> entity() = 0;

    virtual std::unique_ptr<DBRowItem> save(const RowFields &fields)  = 0;
    virtual std::unique_ptr<DBRowItem> update(const RowFields &original, const RowFields &update)  = 0;

  };

} /* namespace Storage */

#endif /* D_DBMODEL_H_ */
