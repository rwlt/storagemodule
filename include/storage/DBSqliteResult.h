/*
 * DBSqliteResult.h
 *
 *  Created on: 30/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DRIVER_DB_SQLITE_RESULT_H_
#define SRC_STORAGE_DRIVER_DB_SQLITE_RESULT_H_

#include "storage/DBResult.h"
#include "storage/DBSchema.h"
#include <sqlite3.h>

namespace Storage::Driver
{

  /// DBSqliteResult
  class DBSqliteResult : public DBResult
  {
    int m_rowDone{0};
    int m_rc;
    sqlite3_stmt *m_st_fetch{nullptr};
    const DBSchema *m_schema{nullptr};
    bool m_raw{false};

  public:
    /// Constructor
    /**
	 * \param rc
	 * \param st
	 */
    DBSqliteResult(int rc, sqlite3_stmt *st) : m_rc(rc),
                                               m_st_fetch{st},
                                               m_schema{nullptr},
                                               m_raw{true}
    {
    }

    DBSqliteResult(int rc, sqlite3_stmt *st, const DBSchema *schema) : m_rc(rc),
                                                                 m_st_fetch{st},
                                                                 m_schema{schema},
                                                                 m_raw{false}
    {
    }

    virtual void useSchema(const DBSchema *schema) {
      m_schema = schema;
      m_raw = false;
    }

    virtual ~DBSqliteResult()
    {
      if (m_st_fetch)
      {
        sqlite3_finalize(m_st_fetch);
      }
    }

    virtual std::pair<bool, std::unique_ptr<DBRowItem>> Fetch();
  };

} /* namespace Storage */

#endif /* SRC_STORAGE_DRIVER_DB_SQLITE_RESULT_H_ */
