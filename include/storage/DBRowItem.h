#ifndef D_DBROWITEM_H
#define D_DBROWITEM_H

#include <string>
#include "storage/StorageType.h"

namespace Storage::Driver
{

  /// DBRowItem abstract interface
  /**
   * 
   */
  class DBRowItem
  {
  private:

  public:
    DBRowItem();
    virtual ~DBRowItem();

    
    /// Set field
    /**
         * \param index col index as short
         * \param value string value of field
         * \throw std::out_of_range
         */
    virtual void setField(unsigned short index, const std::string &value) = 0;
    /// Set field
    /**
         * \param index col index as short
         * \param value integer value of field
         * \throw std::out_of_range
         */
    virtual void setField(unsigned short index, int value) = 0;
    /// Get field std::string
    /**
         * \param[in] index int of col index
         * \param[out] value reference to string
         * \param[out] is_set is the field set with a value or empty
         * \throw std::out_of_range
         */
    virtual void getField(unsigned short index, std::string &value) const = 0;
    /// Get field by index for int type
    /**
         * \param[in] index int of col index
         * \param[out] value int value
         * \param[out] is_set is the field set with a value or empty
         * \throw std::out_of_range
         */
    virtual void getField(unsigned short index, int &value) const = 0;

     /// Get field type
    /**
         * \param[in] index int of col index
         * \return FieldType at index
         * \throw std::out_of_range
         */
    virtual const std::pair<std::string, FieldType> getFieldType(unsigned short index) const = 0;
    /// get number of feilds
    /** 
         * \return vector of field string
         * */
    virtual unsigned short TotalFields() const = 0;
    ///  get Item details as string value
    // /** 
    //      * \return string
    //      * */
    // virtual std::string ItemDetail() const = 0;

  };

}

#endif /* D_DBROWITEM_H */
