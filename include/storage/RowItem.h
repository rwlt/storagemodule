/*
 * RowItem.h
 */
#ifndef D_ROW_ITEM_H
#define D_ROW_ITEM_H

#include "storage/DBRowItem.h"
#include "storage/RowItemType.h"
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <utility>
#include <algorithm>

using Storage::Driver::DBRowItem;
using Storage::Domain::RowFields;
using Storage::Driver::FieldType;

namespace Storage::Domain
{

  /// RowItem implementation of DBRowItem
  /** 
   * Knows how to read and write a single data row
   */
  class RowItem : public DBRowItem
  {
    RowFields m_fields;

  public:
    /// Create an empty row
    RowItem();
    /**
     * RowItems and its fields
     */
    RowItem(RowFields fields);

    /// Virtual deconstructor
    virtual ~RowItem();

    const RowFields &rowFields()
    {
      return m_fields;
    }
    /// Set field
    /**
         * \param index col index as short
         * \param value string value of field
         * \throw std::out_of_range
         */
    void setField(unsigned short index, const std::string &value);
    /// Set field
    /**
         * \param index col index as short
         * \param value integer value of field
         * \throw std::out_of_range
         */
    void setField(unsigned short index, int value);
    /// Get field std::string
    /**
         * \param[in] index int of col index
         * \param[out] value reference to string
         * \throw std::out_of_range
         */
    void getField(unsigned short index, std::string &value) const;
    /// Get field by index for int type
    /**
         * \param[in] index int of col index
         * \param[out] value int value
         * \throw std::out_of_range
         */
    void getField(unsigned short index, int &value) const;

    /// Get field typel
    /**
         * \param[in] index int of col index
         * \return FieldType
         * \throw std::out_of_range
         */

    const std::pair<std::string, FieldType> getFieldType(unsigned short index) const;

    /// get number of feilds
    /** 
         * \return vector of field string
         * */
    unsigned short TotalFields() const;

    /// Equality operator
    /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if equal
         */
    friend bool operator==(const RowItem &lhs, const RowItem &rhs);
    /// Inequality operator
    /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if not equal
         */
    friend bool operator!=(const RowItem &lhs, const RowItem &rhs);

  private:

    /// Get field detail
    /**
         * \param[in] index int of col index
         * \return reference to FieldDetail at index (able to be changed)
         * \throw std::out_of_range
         */
    FieldDetail &getFieldDetail(unsigned short index) const;
  };

  /// Equality operator

  /**
     * \param lhs left hand side
     * \param rhs right hand side
     * \return true if not equal
     */
  inline bool operator!=(const RowItem &lhs, const RowItem &rhs)
  {
    return !(lhs == rhs);
  }

  /// Transfer rowFields in RowItems
  void transferDBRowItem2EntityRowItem(const DBRowItem &dbRow, DBRowItem &toRow);

}
#endif /* D_ROW_ITEM_H */
