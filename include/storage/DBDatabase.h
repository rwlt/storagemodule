/*
 * DBDatabase.h
 */

#ifndef D_DBDATABASE_H_
#define D_DBDATABASE_H_

#include "storage/RowItemType.h"
#include "storage/StorageType.h"
#include "storage/DBModel.h"
#include "storage/DBRowItem.h"
#include "storage/Schema.h"
#include "storage/DBSchema.h"
#include "storage/DBEntity.h"
#include <unordered_map>
#include <string>
#include <vector>
#include <memory>

using Storage::Domain::DBRetrieveMap;
using Storage::Domain::RowFields;
using Storage::Domain::Schema;
using Storage::Driver::StatementType;

/**
 * Storage Schema
 */
namespace Storage
{

  /// DBDatabase abstract (base to DBStorage)
  /**
   * Abstract for DBStorage for working with Schema
    * \sa Schema constructer.
    *
    */

  class DBDatabase
  {

  public:
    /// Constructor
    /**
         * \param entityName const reference of string
          */
    //  DBDatabase();

    /// Virtual deconstructor
    virtual ~DBDatabase();

    // DBDatabase(DBDatabase &other) = delete;
    // void operator=(const DBDatabase &) = delete;

    virtual void ddlTableStatement(
        const std::string &tableName,
        const Schema &schema,
        StatementType stype) const = 0;

    virtual std::vector<std::unique_ptr<DBEntity>> onFindAll(
        const std::string &entityName,
        const Schema &schema,
        OnCreateEntity createEntity,
        DBRetrieveMap where,
        const DBRetrieveMap &parameters) = 0;

    // virtual std::vector<std::unique_ptr<DBEntity>> findAll(
    //     const DBModel &model,
    //     DBRetrieveMap where = {},
    //     const DBRetrieveMap &parameters = {}) const = 0;

    // virtual void beginBatch(const std::string &entityName) = 0;
    // virtual void endBatch(const std::string &entityName) = 0;

    virtual std::unique_ptr<DBRowItem> doAddItem(
        const std::string &entityName,
        const Schema &schema,
        const RowFields &fields) = 0;

    virtual std::unique_ptr<DBRowItem> doUpdateItem(
        const std::string &entityName,
        const Schema &schema,
        const RowFields &original,
        const RowFields &updatefields) = 0;

    virtual void beginTransaction() = 0;
    virtual void commit() = 0;
  };

} /* namespace Storage */

#endif /* D_DBDATABASE_H_ */
