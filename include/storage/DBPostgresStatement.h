/*
 * DBPostgresStatement.h
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DBPOSTGRESSTATEMENT_H_
#define SRC_STORAGE_DBPOSTGRESSTATEMENT_H_

#include "storage/DBDriver.h"
#include <pqxx/pqxx>
#include <memory>

namespace Storage {
namespace Driver {

/// DBPostgresStatement
class DBPostgresStatement: public DBStatement {

	std::shared_ptr<pqxx::connection> m_pC;
	std::shared_ptr<pqxx::work> m_pT;
	std::string m_sql{""};
	Storage::RowItem m_placeHolderRow{};

public:
	/// Constructor
	/**
	 * \param pC sharedPtr pqxx connection
	 * \param pT sharedPtr pqxx work (postgres xx trasaction)
	 * \param sql string
	 * \param placeHolderRow Rowitem (default empty)
	 */
	DBPostgresStatement(std::shared_ptr<pqxx::connection> pC,
			std::shared_ptr<pqxx::work> pT, const std::string &sql,
			const Storage::RowItem &placeHolderRow = {}):
		 m_pC{pC}, m_pT{pT}, m_sql(sql), m_placeHolderRow{placeHolderRow}
	{
	};
	/// Deconstructor
	virtual ~DBPostgresStatement(){
	};


	/// Execute
	/**
	 * \return DBResult unique ptr
	 */
	virtual std::unique_ptr<DBResult> Execute() override;
	/// Execute
	/**
	 * \param name string name
	 * \return DBResult unique ptr
	 */
	virtual std::unique_ptr<DBResult> ExecuteCursor(const std::string &name) override;
	/// Execute
	/**
	 * \return RowItem returning
	 */
	virtual Storage::RowItem ExecuteReturn() override;

};

} /* namespace Driver */
} /* namespace Storage */

#endif /* SRC_STORAGE_DBPOSTGRESSTATEMENT_H_ */
