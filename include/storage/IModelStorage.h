#ifndef MODEL_STORAGE_INTERFACE_H_
#define MODEL_STORAGE_INTERFACE_H_

#include "storage/DBDatabase.h"
#include "storage/DBEntity.h"
#include "storage/StorageException.h"

using Storage::Driver::StorageException;

namespace Storage::Interface
{

  typedef std::function<std::vector<std::unique_ptr<DBEntity>>(
      const std::string &,
      const Schema &,
      OnCreateEntity,
      DBRetrieveMap,
      const DBRetrieveMap &)>
      OnFindAll;

  typedef std::function<void(const std::string &)> OnBeginBatch;
  typedef std::function<void(const std::string &)> OnEndBatch;

  std::vector<std::unique_ptr<DBEntity>> defaultModelOnFindAllEvent(
      const std::string &entityName,
      const Schema &schema,
      OnCreateEntity createEntity,
      DBRetrieveMap where,
      const DBRetrieveMap &parameters);

  void defaultModelOnBeginBatchEvent(const std::string &);

  void defaultModelOnEndBatchEvent(const std::string &);

  class IModelStorage
  {
    DBDatabase *m_dbDatabase;

  public:
    IModelStorage(DBDatabase *dbDatabase);
    // Model::OnFindAll m_onFindAllEvent{defaultModelOnFindAllEvent};
    // Model::OnBeginBatch m_onBeginBatchEvent{defaultModelOnBeginBatchEvent};
    // Model::OnEndBatch m_onEndBatchEvent{defaultModelOnEndBatchEvent};
    OnFindAll m_onFindAll{defaultModelOnFindAllEvent};
    // OnBeginBatch m_onBeginBatch;
    // OnEndBatch m_onEndBatch;
  };

} /* Storage namespace */

#endif /* MODEL_STORAGE_INTERFACE_H_ */