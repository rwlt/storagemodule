/*
 * DBPostgresDriver.h
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DBPOSTGRESDRIVER_H_
#define SRC_STORAGE_DBPOSTGRESDRIVER_H_

#include "storage/DBDriver.h"
#include <pqxx/pqxx>
#include <memory>
namespace Storage {
namespace Driver {

/// DBPostgresDriver
class DBPostgresDriver: public DBDriver {

	std::shared_ptr<pqxx::connection> m_pC;
	std::shared_ptr<pqxx::work> m_pT;

public:
	/// Constructor
	/**
	 * \param dbconnect
	 */
	DBPostgresDriver(const DatabaseConnection &dbconnect):	DBDriver(dbconnect)
	{
	};
	/// Deconstructor
	virtual ~DBPostgresDriver(){
	};

	/// Connect to DB
	/**
	 * \param user
	 * \param password
	 * \param role
	 */
	virtual void Connect(const std::string &user, const std::string &password, const std::string &role) override;
	/// Connection to DB made
	/**
	 * \return bool
	 */
	virtual bool isConnected() {
		return m_isConnected;
	}
	/// Prepare with rowitem placeholders data
	/**
	 * \param sql
	 * \param placeHolderRow
	 * \return DBStatement unique ptr
	 */
	virtual std::unique_ptr<DBStatement>  CreateStatement(const std::string &sql, const Storage::RowItem &placeHolderRow = {}) override;
	/// has a transaction started
	/**
	 * \return bool
	 */
	virtual bool hasTransactionStarted() {
		return m_hasTransactionStarted;
	}

	/// Commit
	virtual void Commit() override;

private:
	void disconnect();

};

} /* namespace Driver */
} /* namespace Storage */

#endif /* SRC_STORAGE_DBPOSTGRESDRIVER_H_ */
