/*
 * DBFirebirdDriver.h
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DB_SQLITE_DRIVER_H_
#define SRC_STORAGE_DB_SQLITE_DRIVER_H_

#ifdef NDEBUG
#define D(x)
#else
#define D(x) x
#endif

#include "storage/DBDriver.h"
#include "storage/DBStatement.h"
#include "storage/DBRowItem.h"
#include "storage/DBModel.h"
#include "storage/StorageException.h"
#include <sqlite3.h>
#include <fstream>
#include <mutex>
#include <atomic>

int busy_callback(void *, int tries);

namespace Storage::Driver
{

  /// DBSqliteDriver
  class DBSqliteDriver : public DBDriver
  {
    static int xtries1;
    static int xtries;

    sqlite3 *m_db{nullptr};
    std::atomic<bool> m_hasTransactionStarted{false}; ///< has transaction started?
    std::mutex m_beginTransaction{};
    std::mutex m_endTransaction{};
    std::mutex m_TransactionMutex{};

  public:
    /**
	 * \param db_connect
	 */
    explicit DBSqliteDriver(
        const DatabaseConnection &db_connect,
        const std::string &user = {},
        const std::string &password = {},
        const std::string &role = {});

    virtual ~DBSqliteDriver();

  //   /// Connection to DB made
  //   /**
	//  * \return bool
	//  */
  //   virtual bool isConnected()
  //   {
  //     return m_isConnected;
  //   }

    virtual std::unique_ptr<DBStatement> CreateStatement(
        const std::string &sql);

    virtual std::unique_ptr<DBStatement> CreateStatement(
        const DBSchema &schema,
        const std::string &sql);

    virtual std::unique_ptr<DBStatement> CreateStatement(
        const std::string entityName,
        const DBSchema &schema,
        const DBRowItem *where = {nullptr},
        const StatementType statementType = StatementType::SELECT);

    // virtual std::unique_ptr<DBStatement> CreateDDLStatement(
    //   const std::string entityName,
    //   const DBSchema &schema,
    //   const DBRowItem *where,
    //   const StatementType statementType);

    virtual std::unique_ptr<DBStatement> CreateInsertStatement(
        const std::string entityName,
        const DBSchema &schema,
        const DBRowItem &row);

    virtual std::unique_ptr<DBStatement> CreateUpdateStatement(
        const std::string entityName,
        const DBSchema &schema,
        const DBRowItem &original,
        const DBRowItem &row);

    // Begin Transaction
    virtual void BeginTransaction();

    /// has a transaction started
    /**
	 * \return bool
	 */
    virtual bool hasTransactionStarted()
    {
      return m_hasTransactionStarted;
    }
    /// Commit
    virtual void Commit();

    // /// Create Sqlite database
    // bool createDatabase(const std::string &file);

  // private:
  //   void disconnect();
  };

} /* namespace Storage */

#endif /* SRC_STORAGE_DB_SQLITE_DRIVER_H_ */
