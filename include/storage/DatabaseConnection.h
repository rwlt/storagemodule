/*
 * DatabaseConnection.h
 */

#ifndef D_DATABASE_CONNECTION_H_
#define D_DATABASE_CONNECTION_H_

#include <string>
#include <map>

/// Storage namespace
namespace Storage::Driver {
    /// Connection type

    /**
     * Specify a Connection Type.
     */
    enum class ConnectionType : char {
        LIVE, ///< Live connection
        TEST ///< Test connections
    };

    /// DatabaseConnection

    /**
     * Database connection to a database query.
     */
    class DatabaseConnection {
        std::string m_driver;
        std::string m_server;
        std::string m_dbFile;
        std::string m_charSet;

    public:
        /// Constructor
        /**
         * \param driver const reference of string
         * \param server const reference of string
         * \param dbFile const reference of string
         * \param charSet const reference of string
         */
        DatabaseConnection(std::string driver,
                std::string server,
                std::string dbFile,
                std::string charSet);

        /// Driver
        /**
         * \return string
         */
        std::string getDriver() const;
        /// Server
        /**
         * \return string
         */
        std::string getServer();
        /// DBFile
        /**
         * \return string
         */
        std::string getDBFile();
        /// CharSet
        /**
         * \return string
         * */
        std::string getCharSet();

        /// Equality operator
        /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if equal
         */
        friend bool operator==(const DatabaseConnection &lhs, const DatabaseConnection &rhs);
        /// Inequality operator
        /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if not equal
         */
        friend bool operator!=(const DatabaseConnection &lhs, const DatabaseConnection &rhs);

    private:

        friend class ParseDatabaseConnection;

    };

    /// Equality operator
    inline bool operator==(const DatabaseConnection &lhs, const DatabaseConnection &rhs) {
        //    bool result = false;
        return (lhs.m_driver == rhs.m_driver &&
                lhs.m_dbFile == rhs.m_dbFile &&
                lhs.m_server == rhs.m_server &&
                lhs.m_charSet == rhs.m_charSet);
    }
    /// Equality operator
    inline bool operator!=(const DatabaseConnection &lhs, const DatabaseConnection &rhs) {
        return !(lhs == rhs);
    }

} /* namespace Storage */

#endif /* D_DATABASE_CONNECTION_H_ */
