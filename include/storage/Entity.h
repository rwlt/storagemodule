/*
 * Entity.h
 */
#ifndef D_ENTITY_H
#define D_ENTITY_H

#include "storage/DBEntity.h"
#include "storage/RowItem.h"
#include "storage/Schema.h"
#include "storage/StorageException.h"
#include <memory>
#include <unordered_map>
#include <functional>

using Storage::Domain::RowItem;
using Storage::Domain::Schema;
using Storage::Driver::DBRowItem;
using Storage::Driver::FieldType;
using Storage::Driver::StorageException;

namespace Storage
{
  typedef std::unordered_map<std::string, unsigned short> FieldNameMap;

  // Entity callable events - onSave method
  std::unique_ptr<DBRowItem> defaultEntityOnSaveEventFunc(const RowFields &fields);
  std::unique_ptr<DBRowItem> defaultEntityOnUpdateEventFunc(const RowFields &original, const RowFields &update);

  class Entity : public DBEntity
  {
  public:
    typedef std::function<std::unique_ptr<DBRowItem>(const RowFields &fields)> OnSave;
    typedef std::function<std::unique_ptr<DBRowItem>(const RowFields &original, const RowFields &update)> OnUpdate;

  private:
//    const DBModel *m_dBModel{nullptr};
    const Schema *m_schema{nullptr};
    std::unique_ptr<RowItem> m_rowItem{nullptr};
    std::unique_ptr<RowItem> m_previousRowItem{nullptr};
    bool m_new{true};
    OnSave m_onSave;
    OnUpdate m_onUpdate;

  public:
    /// This entity is a new to add to dbstorage - Entity constructor from RowItem is a new entity.
    Entity(/*const DBModel &dBModel,*/
           const Schema &schema,
           std::unique_ptr<RowItem> rowItem,
           OnSave onSave = {defaultEntityOnSaveEventFunc},
           OnUpdate onUpdate = {defaultEntityOnUpdateEventFunc});

    /// This entity is an exisitng entry from dbstorage - Entity constructor from DBRowItem is an existing
    /// entity.
    Entity(/*const DBModel &dBModel,*/
           const Schema &schema,
           const DBRowItem &rowItem,
           OnSave onSave = {defaultEntityOnSaveEventFunc},
           OnUpdate onUpdate = {defaultEntityOnUpdateEventFunc});

    virtual ~Entity();

    Entity &operator=(const Entity &o) = delete;
    Entity(const Entity &o) = delete;
    Entity(Entity &&entity) noexcept;

    /// Get field value with name
    /**
         * \param name
         * \return string and field type (tuple<0, 1>)
         * \throw std::out_of_range
         * \sa Storage::FieldType
         */
    std::tuple<std::string, FieldType> get(const std::string &name) const;
    std::tuple<std::string, FieldType> getRetreived(const std::string &name) const;

    /// Set field with name
    /**
         * \param name
         * \param value
         * \throw std::out_of_range
         */
    void set(const std::string &name, const std::string &value);

    int size() const;

    void save();

  private:
    std::tuple<int, FieldType> fieldTypeByName(const std::string &name) const;
  };

}

#endif /* D_ENTITY_H */
