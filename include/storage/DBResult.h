/*
 * DBResult.h
 *
 *  Created on: 30/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DRIVER_DB_RESULT_H_
#define SRC_STORAGE_DRIVER_DB_RESULT_H_

#include "storage/DBRowItem.h"
#include "storage/DBSchema.h"
#include <memory>

namespace Storage::Driver {

class DBDriver;

/// DBResult
class DBResult {

public:
	DBResult() = default;
	virtual ~DBResult() = default;

  virtual void useSchema(const DBSchema *schema) = 0;

	/// Fetch
	/**
	 * \param row DBRowItem
	 * \return if more to fetch
	 */
	virtual std::pair<bool, std::unique_ptr<DBRowItem>> Fetch() = 0;

};

} /* namespace Storage */

#endif /* SRC_STORAGE_DRIVER_DB_RESULT_H_ */
