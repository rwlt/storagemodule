/*
 * DBFirebirdStatement.h
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DB_SQLITE_STATEMENT_H_
#define SRC_STORAGE_DB_SQLITE_STATEMENT_H_

#include <utility>

#include "storage/DBDriver.h"
#include <sqlite3.h>

//extern C 

namespace Storage::Driver
{

  /// DBSqliteStatement
  class DBSqliteStatement : public DBStatement
  {

    sqlite3 *m_st_db{nullptr};
    std::string m_sql{};
    const DBSchema *m_schema{nullptr};
    bool m_raw{false};


    static void splitReturningStatement(std::string &sql, std::string &table);

  public:
    /// Constructor
    /**
	 * \param st IBPP Statement
	 * \param sql string
	 */
    DBSqliteStatement(sqlite3 *db,
                      std::string sql,
                      const DBSchema &schema) : m_st_db(db),
                                                m_sql(std::move(sql)),
                                                m_schema{&schema},
                                                m_raw{false} {};
    /// Constructor
    /**
	 * \param st IBPP Statement
	 * \param sql string
	 */
    DBSqliteStatement(sqlite3 *db,
                      std::string sql) : m_st_db(db),
                                         m_sql(std::move(sql)),
                                         m_schema{nullptr},
                                         m_raw{true} {};
    /// Deconstructor
    virtual ~DBSqliteStatement(){};

    /// Execute
    /**
	 * \return DBResult
	 */
    virtual std::unique_ptr<DBResult> Execute();
    /// Execute
    /**
	 * \param name string name
	 * \return DBResult
	 */
    virtual std::unique_ptr<DBResult> ExecuteCursor(const std::string &name);
    /// Execute
    /**
	 * \return RowItem returning
	 */
    virtual void ExecuteReturn();
  };

} /* namespace Storage */

#endif /* SRC_STORAGE_DB_SQLITE_STATEMENT_H_ */
