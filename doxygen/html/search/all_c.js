var searchData=
[
  ['relation_5ftype_87',['relation_type',['../structStorage_1_1RelationDetail.html#aa1c15a77b4d9b0d10d220f3915e162d6',1,'Storage::RelationDetail']]],
  ['relationdetail_88',['RelationDetail',['../structStorage_1_1RelationDetail.html',1,'Storage::RelationDetail'],['../structStorage_1_1RelationDetail.html#a8022e3692ec8e6325dbbdeb1de4ecf33',1,'Storage::RelationDetail::RelationDetail()'],['../structStorage_1_1RelationDetail.html#aa71abfa6c6f1613bf248110e71cfb086',1,'Storage::RelationDetail::RelationDetail(std::string name, Storage::RelationType value)']]],
  ['relationtype_89',['RelationType',['../namespaceStorage.html#a3318f1d43560e837babe37d023caf095',1,'Storage']]],
  ['resultretrieval_90',['ResultRetrieval',['../classStorage_1_1ResultRetrieval.html',1,'Storage']]],
  ['rowfields_91',['rowFields',['../classStorage_1_1Schema.html#ae88f5894bbb24023f93cb587f6320474',1,'Storage::Schema::rowFields()'],['../namespaceStorage.html#aa43b004951b1725b987b9fc33a4629c1',1,'Storage::RowFields()']]],
  ['rowfields_5fvalue_5ftype_92',['RowFields_value_type',['../namespaceStorage.html#a63dd3922528cdd57ea1958b642d558d9',1,'Storage']]],
  ['rowitem_93',['RowItem',['../classStorage_1_1RowItem.html',1,'Storage::RowItem'],['../classStorage_1_1RowItem.html#af18a9c6a24eae1a2850cd3fc0cf8d0d0',1,'Storage::RowItem::RowItem()'],['../classStorage_1_1RowItem.html#a4222cdbcc22fb468840a31544f8bcba4',1,'Storage::RowItem::RowItem(RowFields fields)']]]
];
