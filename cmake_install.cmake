# Install script for directory: /home/rwlltt/rwlltt_server/dockers/storagemodule

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/rwlltt/rwlltt_server/dockers/storagemodule/libStorageModule.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/rlwapp/storage" TYPE FILE FILES
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DatabaseConnection.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBClient.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBDatabase.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBDriver.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBEntity.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBModel.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBPostgresDriver.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBPostgresResult.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBPostgresStatement.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBResult.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBRowItem.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBSchema.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBSqliteDriver.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBSqliteResult.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBSqliteStatement.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBStatement.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/DBStorage.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/Entity.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/Model.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/RowItem.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/RowItemType.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/Schema.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/StorageException.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/StorageType.h"
    "/home/rwlltt/rwlltt_server/dockers/storagemodule/include/storage/storage.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/StorageModule/StorageModuleTargets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/StorageModule/StorageModuleTargets.cmake"
         "/home/rwlltt/rwlltt_server/dockers/storagemodule/CMakeFiles/Export/lib/cmake/StorageModule/StorageModuleTargets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/StorageModule/StorageModuleTargets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/StorageModule/StorageModuleTargets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/StorageModule" TYPE FILE FILES "/home/rwlltt/rwlltt_server/dockers/storagemodule/CMakeFiles/Export/lib/cmake/StorageModule/StorageModuleTargets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/StorageModule" TYPE FILE FILES "/home/rwlltt/rwlltt_server/dockers/storagemodule/CMakeFiles/Export/lib/cmake/StorageModule/StorageModuleTargets-release.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/StorageModule" TYPE FILE FILES "/home/rwlltt/rwlltt_server/dockers/storagemodule/StorageModuleConfig.cmake")
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/rwlltt/rwlltt_server/dockers/storagemodule/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
