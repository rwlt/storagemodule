#include "storage/Entity.h"
#ifdef NDEBUG
#define D(x)
#else
#define D(x) x
#endif

namespace Storage
{
  Entity::Entity(
      const Schema &schema,
      std::unique_ptr<RowItem> rowItem,
      OnSave onSave,
      OnUpdate onUpdate) : DBEntity(),
                       m_schema{&schema},
                       m_rowItem{std::move(rowItem)},
                       m_previousRowItem{nullptr},
                       m_new{true},
                       m_onSave{onSave},
                       m_onUpdate{onUpdate} {};

  Entity::Entity(
      const Schema &schema,
      const DBRowItem &rowTranItem,
      OnSave onSave,
      OnUpdate onUpdate) : DBEntity(),
                       m_schema{&schema},
                       m_rowItem{nullptr},
                       m_previousRowItem{nullptr},
                       m_new{false},
                       m_onSave{onSave},
                       m_onUpdate{onUpdate} 
  {
    m_rowItem = std::unique_ptr<RowItem>(new RowItem(m_schema->rowFields()));
    m_previousRowItem = std::unique_ptr<RowItem>(new RowItem(m_schema->rowFields()));
    transferDBRowItem2EntityRowItem(rowTranItem, *m_rowItem);
    transferDBRowItem2EntityRowItem(rowTranItem, *m_previousRowItem);
  };

  Entity::~Entity(){
    //std::cerr << ".";
  };

  Entity::Entity(Entity &&entity) noexcept : DBEntity(),
                                             m_schema{nullptr},
                                             m_rowItem{nullptr},
                                             m_previousRowItem{nullptr},
                                             m_new{false}
  {
    m_schema = entity.m_schema;
    m_rowItem = std::move(entity.m_rowItem);
    m_previousRowItem = std::move(entity.m_previousRowItem);
    m_new = entity.m_new;
    m_onSave = entity.m_onSave;
    m_onUpdate = entity.m_onUpdate;
  }
  // Get field value with name
  /**
         * \param name
         * \return string and field type (tuple<0, 1>)
         * \throw std::out_of_range
         * \sa FieldType
         */
  std::tuple<std::string, FieldType> Entity::get(const std::string &name) const
  {
    auto fieldDetailIndex = fieldTypeByName(name);
    auto index = std::get<0>(fieldDetailIndex);
    auto field_type = std::get<1>(fieldDetailIndex);

    switch (field_type)
    {
    case FieldType::INTEGER:
    case FieldType::PK_INT:
    case FieldType::AUTOGENINT:
    {
      int value;
      m_rowItem->getField(index, value);
      return {std::to_string(value), field_type};
    }
    case FieldType::STRING:
    {
      std::string value;
      m_rowItem->getField(index, value);
      return {value, field_type};
    }
    default:
      throw std::out_of_range("Entity: invalid type for " + name);
    }
  }

  std::tuple<std::string, FieldType> Entity::getRetreived(const std::string &name) const
  {
    auto fieldDetailIndex = fieldTypeByName(name);
    auto index = std::get<0>(fieldDetailIndex);
    auto field_type = std::get<1>(fieldDetailIndex);
    if (m_new) {
      throw std::out_of_range("Entity: getRetrieved not available for " + name);
    }
    switch (field_type)
    {
    case FieldType::INTEGER:
    case FieldType::PK_INT:
    case FieldType::AUTOGENINT:
    {
      int value;
      m_previousRowItem->getField(index, value);
      return {std::to_string(value), field_type};
    }
    case FieldType::STRING:
    {
      std::string value;
      m_previousRowItem->getField(index, value);
      return {value, field_type};
    }
    default:
      throw std::out_of_range("Entity: invalid type for " + name);
    }
  }


  /// Set field with name
  /**
         * \param name
         * \param value
         * \throw std::out_of_range
         */

  void Entity::set(const std::string &name, const std::string &value)
  {
    auto fieldDetailIndex = fieldTypeByName(name);
    auto field_type = std::get<1>(fieldDetailIndex);
    auto index = std::get<0>(fieldDetailIndex);

    //std::basic_string<char, std::char_traits<char>, std::allocator<char> >
    // Use string as value, and if field type is int do convert string to int
    // The caller needs to set string for int, and feild type of INT will error
    // if string not convert to int.
    switch (field_type)
    {
    case FieldType::STRING:
      m_rowItem->setField(index, value);
      break;
    case FieldType::INTEGER:
    case FieldType::PK_INT:
    case FieldType::AUTOGENINT:
    {
      auto number = std::atoi(value.c_str());
      if (std::to_string(number) == value)
      {
        m_rowItem->setField(index, std::atoi(value.c_str()));
      }
      else
      {
        throw std::out_of_range("Entity:invalid number found: \"" + value + "\"");
      }
    }
    break;
    default:
      throw std::out_of_range("Entity:invalid type for " + name);
      break;
    }
  };

  int Entity::size() const
  {
    return m_rowItem->TotalFields();
  }

  void Entity::save()
  {
    if (m_new == true)
    {
      // New and first save - uses OnSave callback to save the entity by who owns the entity
      try
      {
        auto rowItem = m_onSave(m_rowItem->rowFields());
        if (rowItem)
        {
          if (m_new)
          {
            m_previousRowItem = std::unique_ptr<RowItem>(new RowItem(m_schema->rowFields()));
            m_new = false;
          }
          //std::cout << "save " << rowItem->TotalFields() << "\n";
          transferDBRowItem2EntityRowItem(*rowItem, *m_rowItem);
          transferDBRowItem2EntityRowItem(*rowItem, *m_previousRowItem);
        }
        // else
        // {
        //   throw StorageException("No save callback - not saved by default.");
        // }
      }
      catch (StorageException &e)
      {
        throw StorageException(std::string("No save callback found - not saved by default.") + e.what());
      }
    }
    else
    {
      // Update
      auto rowItem = m_onUpdate(m_previousRowItem->rowFields(), m_rowItem->rowFields());
      if (rowItem)
      {
        transferDBRowItem2EntityRowItem(*rowItem, *m_rowItem);
        transferDBRowItem2EntityRowItem(*rowItem, *m_previousRowItem);
      }
    }
  }

  //===========================================================================
  // Private
  //===========================================================================
  std::tuple<int, FieldType> Entity::fieldTypeByName(const std::string &name) const
  {
    unsigned short fieldIndex = m_schema->fieldNameIndex().at(name);
    return std::tuple<int, FieldType>{fieldIndex, m_rowItem->getFieldType(fieldIndex).second};
  }

  std::unique_ptr<DBRowItem> defaultEntityOnSaveEventFunc(const RowFields &)
  {
    throw StorageException("No implemented save callback - not saved by defaultEntityOnSaveEventFunc.");
  };

  std::unique_ptr<DBRowItem> defaultEntityOnUpdateEventFunc(const RowFields &, const RowFields &)
  {
    throw StorageException("No implemented update callback - not updated by defaultEntityOnUpdateEventFunc.");
  };

}