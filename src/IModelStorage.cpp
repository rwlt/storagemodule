#include "storage/IModelStorage.h"
#include <functional>

using namespace std::placeholders;

namespace Storage::Interface
{
  IModelStorage::IModelStorage(DBDatabase *dbDatabase) : m_dbDatabase{dbDatabase} {
    m_onFindAll = std::bind(&DBDatabase::onFindAll, dbDatabase, _1, _2, _3, _4, _5);
    // m_onFindAll = [dbDatabase](const std::string &,
    //   const Schema &,
    //   OnCreateEntity,
    //   DBRetrieveMap,
    //   const DBRetrieveMap &) ->std::vector<std::unique_ptr<DBEntity>> {

    //   };
  }
  std::vector<std::unique_ptr<DBEntity>> defaultModelOnFindAllEvent(
      const std::string &,
      const Schema &,
      OnCreateEntity,
      DBRetrieveMap,
      const DBRetrieveMap &)
  {
    throw StorageException("No implemented callback - cannot findAll defaultModelOnFindAllEvent.");
  };

  void defaultModelOnBeginBatchEvent(const std::string &)
  {
    throw StorageException("No implemented callback - cannot beginBatch defaultModelOnBeginBatchEvent.");
  };

  void defaultModelOnEndBatchEvent(const std::string &)
  {
    throw StorageException("No implemented callback - cannot endBatch defaultModelOnEndBatchEvent.");
  };

}