/*
 * DBPostgresResult.cpp
 *
 *  Created on: 30/06/2017
 *      Author: rodney
 */

#include "storage/DBPostgresResult.h"
#include "storage/StorageException.h"

namespace Storage {
namespace Driver {


/// Execute
/**
 * \return if more to fetch
 */
bool DBPostgresResult::Fetch(Storage::RowItem &row)
{
	try {
		if (m_result.empty()) {
			return false;
		}
		if (m_rowDone == m_result.size()) {
			return false;
		}
		for (unsigned short index = 0; index < row.TotalFields();
				++index) {
			int int_value;
			std::string str_value;
			FieldDetail fd = row.getFieldDetail(index);
			switch (fd.field_type) {
			case Storage::FieldType::INTEGER:
				int_value = m_result[m_rowDone][index-1].as<int>();
				row.setField(index, int_value);
				break;
			case Storage::FieldType::STRING:
				str_value = m_result[m_rowDone][index-1].as<std::string>();
				row.setField(index, str_value);
				break;
			default:
				//nothing
				break;
			}
		}

		++m_rowDone;

		return true;

	} catch (std::exception& e) {
		std::string what(e.what());
		std::cout << "Fetch error " << what << "\n";
		std::stringstream message;
		message << "DBPostgresResult::Fetch row on database exception " << e.what();
		throw(StorageException(message.str()));
	}

}


} /* namespace Driver */
} /* namespace Storage */
