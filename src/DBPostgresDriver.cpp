/*
 * DBPostgresDriver.cpp
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#include "storage/DBPostgresDriver.h"
#include "storage/DBPostgresStatement.h"
#include "storage/DBPostgresResult.h"
#include "storage/StorageException.h"
#include <memory>

namespace Storage {
namespace Driver {

/// Connect to Postgres DB
/**
 * Able to use Connect which will disconnect an earlier Connect on this driver instance
 * \param user
 * \param password
 * \param role
 */
void DBPostgresDriver::Connect(const std::string &user, const std::string &password, const std::string &role)
{
	try {
		disconnect();

		std::stringstream conn_detail;
		conn_detail << "dbname=" << m_dbconnect.getDBFile();
		conn_detail << " user=" << user;
		conn_detail << " password=" << password;
		conn_detail << " hostaddr=" << m_dbconnect.getServer();
		conn_detail << " port=5432";

        this->m_pC = std::make_shared<pqxx::connection>(conn_detail.str());
 		m_isConnected = true;
	} catch (std::exception& e) {
		//std::string what(e.what());
		//std::cout << "Connect error " << what << "\n";
		std::stringstream message;
		message << "DBPostgresDriver::Connect user to database exception " << e.what();
		throw(StorageException(message.str()));
	}

}
/// Prepare
/**
 * \param sql
 */
std::unique_ptr<DBStatement> DBPostgresDriver::CreateStatement(const std::string &sql, const Storage::RowItem &placeHolderRow)
{
    if (!m_isConnected) {
		throw(StorageException("DBPostgresDriver::CreateStatement No connected database."));
    }
	try {
		// Requires a pqxx transaction<> (work) for statements
		if (!m_hasTransactionStarted) {
	        this->m_pT = std::make_shared<pqxx::work>(*m_pC);
			m_hasTransactionStarted = true;
		}
		std::unique_ptr<DBStatement> statement = std::unique_ptr<DBStatement>(
				new DBPostgresStatement(m_pC, m_pT, sql, placeHolderRow));
		return statement;

	} catch (std::exception& e) {
		std::stringstream message;
		message << "BPostgresDriver::CreateStatement Prepare statement on database exception " << e.what();
		throw(StorageException(message.str()));
	}

}

/// Commit
/**
 * User should try the commit at end of try blocks to get result
 */
void DBPostgresDriver::Commit() {
	if (m_pT.get() != 0 && m_hasTransactionStarted) {
		try {
			m_pT->commit();
			m_hasTransactionStarted = false;
		} catch (std::exception& e) {
			m_hasTransactionStarted = false;
			//std::string what(e.what());
			//std::cout << "Commit error " << what << "\n";
			std::stringstream message;
			message << "DBPostgresDriver::Commit on database exception " << e.what();
			throw(StorageException(message.str()));
		}
	}
}

//
//--------------------------------------------------------------
// Private
//---------------------------------------------------------------
//
void DBPostgresDriver::disconnect() {
	if (m_pT.get() != 0 && m_hasTransactionStarted) {
		try {
			m_pT->commit();
		} catch (std::exception&) {/* privately ignore aborted transactions */}
	}
	if (m_pC.get() != 0 && m_isConnected) {
		m_pC->disconnect();
	}
	m_hasTransactionStarted = false;
	m_isConnected = false;
}


} /* namespace Driver */
} /* namespace Storage */
