/*
 * RowItem.cpp
 */
#ifdef NDEBUG
#define D(x)
#else
#define D(x) x
#endif

#include "storage/RowItem.h"
#include <sstream>
#include <algorithm>

namespace Storage::Domain
{
  // Create an empty row

  RowItem::RowItem() : DBRowItem()
  {
  }
  //

  RowItem::RowItem(RowFields fields) : DBRowItem(), m_fields(fields)
  {
  }

  // Virtual deconstructor
  RowItem::~RowItem()
  {
  }

  void RowItem::setField(unsigned short index, const std::string &value)
  {
    auto &fd = getFieldDetail(index);  // If index is a string change the value
    if (fd.field_type == FieldType::STRING)
    {
      fd.str_value = value;
    }
    else
    {
      std::stringstream message;
      message << "RowItem: out_of_range setField is not STRING type at index " << index;
      throw std::out_of_range(message.str());
    }
  }

  void RowItem::setField(unsigned short index, int value)
  {
    auto &fd = getFieldDetail(index);   // If index is an integer change the value
    switch (fd.field_type)
    {
    case FieldType::INTEGER:
    case FieldType::PK_INT:
    case FieldType::AUTOGENINT:
    {
      fd.int_value = value;
    }
    break;
    default:
    {
      std::stringstream message;
      message << "RowItem: out_of_range setField is not INTEGER type at index " << index;
      throw std::out_of_range(message.str());
    }
    }
  }

  void RowItem::getField(unsigned short index, std::string &value) const
  {
    auto &fd = getFieldDetail(index);
    if (fd.field_type == FieldType::STRING)
    {
      value = fd.str_value;
    }
    else
    {
      std::stringstream message;
      message << "RowItem: out_of_range getField is not STRING type at index " << index;
      throw std::out_of_range(message.str());
    }
  }

  void RowItem::getField(unsigned short index, int &value) const
  {
    auto &fd = getFieldDetail(index);
    switch (fd.field_type)
    {
    case FieldType::INTEGER:
    case FieldType::PK_INT:
    case FieldType::AUTOGENINT:
    {
      value = fd.int_value;
    }
    break;
    default:
    {
      std::stringstream message;
      message << "RowItem: out_of_range getField is not INTEGER type at index " << index;
      throw std::out_of_range(message.str());
    }
    }
  }

  const std::pair<std::string, FieldType> RowItem::getFieldType(unsigned short index) const
  {
    unsigned short test = index;
    auto itr = std::find_if(m_fields.begin(), m_fields.end(),
                            [&test](const RowFields_value_type &p) -> bool {
                              return (p.first == test);
                            });
    if (itr != m_fields.end())
    {
      return std::make_pair((itr->second).field_name, (itr->second).field_type);
    }
    else
    {
      std::stringstream message;
      message << "RowItem: out_of_range getFieldDetail index " << index;
      throw std::out_of_range(message.str());
    }
  }

  // get number of fields

  unsigned short RowItem::TotalFields() const
  {
    return (unsigned short)m_fields.size();
  }

  /// Equality operator

  /**
     * \param lhs left hand side
     * \param rhs right hand side
     * \return true if equal
     */
  bool operator==(const RowItem &lhs, const RowItem &rhs)
  {
    if (lhs.TotalFields() == rhs.TotalFields())
    {
      for (unsigned short index = 0; index < lhs.TotalFields(); ++index)
      {
        FieldDetail lhs_fd = lhs.m_fields.at(index);
        FieldDetail rhs_fd = rhs.m_fields.at(index);
        if (!(lhs_fd == rhs_fd))
        {
          return false;
        }
      }
      return true;
    }
    else
    {
      return false;
    }
  }

  FieldDetail &RowItem::getFieldDetail(unsigned short index) const
  {
    try
    {
      return const_cast<FieldDetail &>(m_fields.at(index));
    }
    catch (const std::exception &e)
    {
      std::stringstream message;
      message << "RowItem: out_of_range getFieldDetail index " << index;
      throw std::out_of_range(message.str());
    }
  }

  void transferDBRowItem2EntityRowItem(const DBRowItem &dbRow, DBRowItem &toRow)
  {
    for (unsigned int index = 0; index < dbRow.TotalFields(); index++)
    {
      int int_value;
      std::string str_value;
      auto fd = dbRow.getFieldType(index);
      auto fd2 = toRow.getFieldType(index);
      D(std::cout << "transferDBRowItem2EntityRowItem " << fd.first << " " << fd2.first << "\n";)
      switch (fd.second)
      {
      case FieldType::INTEGER:
      case FieldType::PK_INT:
      case FieldType::AUTOGENINT:
      {
        dbRow.getField(index, int_value);
        D(std::cout << "transferDBRowItem2EntityRowItem INTEGER " << int_value << "\n";)
        toRow.setField(index, int_value);
      }
      break;
      case FieldType::STRING:
      {
        dbRow.getField(index, str_value);
        D(std::cout << "transferDBRowItem2EntityRowItem STRING " << str_value << "\n";)
        toRow.setField(index, str_value);
      }
      break;
      }
    }
  }

} /* namespace Storage */
