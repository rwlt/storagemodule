/*
 * DBFirebirdStatement.cpp
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#include "storage/DBSqliteStatement.h"
#include "storage/DBSqliteResult.h"
#include "storage/StorageException.h"
#include <memory>

#ifdef NDEBUG
#define D(x)
#else
#define D(x) x
#endif

namespace Storage::Driver
{

  /// Execute
  /**
 * \return DBResult of fetch ready for iteration
 */
  std::unique_ptr<DBResult> DBSqliteStatement::Execute()
  {
    int rc;
    sqlite3_stmt *stmt;
    D(std::cout << "Execute " << m_sql.c_str() << "\n";)

    rc = sqlite3_prepare_v2(m_st_db, m_sql.c_str(), -1, &stmt, nullptr);
    if (rc)
    {
      std::stringstream message;
      message << "DBSqliteStatement::Execute prepare statement on database exception  "
              << sqlite3_errmsg(m_st_db);
      sqlite3_finalize(stmt);
      throw(StorageException(message.str()));
    }
    // do the first step here so to to complete Sql with out required fetch results
    rc = sqlite3_step(stmt);
    //Ref: see https://sqlite.org/rescode.html#row of the return codes
    /* Here we create a DBResult with this rc code and the st_mt
	 * It could be completed insert/delete/insert operation which requires no fetch result
	 * If SQLITE_ROW then furthur rows can be read.
	 * DBResult is where the rows are read with fetch().
	 */

    if (rc == SQLITE_ROW || rc == SQLITE_DONE)
    {
      D(std::cerr << "prepare " << m_sql << "\n";)
      if (rc == SQLITE_DONE) {
          sqlite3_finalize(stmt);
          D(std::cerr << "prepare stmt 3 finalized\n";)
          stmt = nullptr;
      }

      std::unique_ptr<DBResult> result;
      if (m_raw) {
        result = std::make_unique<DBSqliteResult>(rc, stmt);
      } else {
        result = std::make_unique<DBSqliteResult>(rc, stmt, m_schema);
      }
      return result;
    }

    sqlite3_finalize(stmt);
    std::stringstream message;
    message << "DBSqliteStatement::Execute statement on database exception (" << rc << ") " << sqlite3_errmsg(m_st_db);
    throw(StorageException(message.str()));
  }
  /// Execute a cursor
  /**
 * \return DBResult of fetch ready for iteration
 */
  std::unique_ptr<DBResult> DBSqliteStatement::ExecuteCursor(const std::string &name)
  {
    std::stringstream message;
    message << "DBSqliteStatement::ExecuteCursor " << name << " statement on database exception ";
    throw(StorageException(message.str()));
  }
  /// Execute with Returning prepare statement
  /**
 * \return RowItem returning
 */
  void DBSqliteStatement::ExecuteReturn()
  {
    int rc;
    std::string table;
    sqlite3_stmt *stmt;
    //sqlite3_stmt *retstmt;
    std::string sql(m_sql);

    // DBStorage builds RETURNING sql which is not available in Sqlite so we alter the sql
    D(std::cout << "ExecuteReturn " << sql << "\n";)
    splitReturningStatement(sql, table);
    D(std::cout << "ExecuteReturn split statement " << sql << " " << table << "\n";)

    rc = sqlite3_prepare_v2(m_st_db, sql.c_str(), -1, &stmt, nullptr);
    if (rc)
    {
      std::stringstream message;
      message << "DBSqliteStatement::ExecuteReturn statement on database exception  "
              << sqlite3_errmsg(m_st_db);
      throw(StorageException(message.str()));
    }

    rc = sqlite3_step(stmt);
    if (rc == SQLITE_DONE)
    {
      sqlite3_finalize(stmt);
      return;
    }

    std::stringstream message;
    message << "DBSqliteStatement::ExecuteReturn statement on database exception " << sqlite3_errmsg(m_st_db);
    throw(StorageException(message.str()));
  }

  //
  // Private
  //
  void DBSqliteStatement::splitReturningStatement(std::string &sql, std::string &table)
  {
    size_t pos1 = sql.find("INSERT INTO");
    size_t pos2 = sql.find('(');
    size_t pos3 = sql.find("RETURNING");
    if ((pos1 < pos2) && ((pos1 + 11) < pos2) && (pos2 < pos3))
    {
      table = sql.substr(pos1 + 11, pos2 - (pos1 + 11));
      sql = sql.erase(pos3);
    }
  }

} /* namespace Storage */
