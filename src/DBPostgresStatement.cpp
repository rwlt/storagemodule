/*
 * DBPostgresStatement.cpp
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#include "storage/DBPostgresStatement.h"
#include "storage/DBPostgresResult.h"
#include "storage/StorageException.h"
#include <memory>

namespace Storage {
namespace Driver {


/// Execute
/**
 * \return DBResult of fetch ready for iteration
 */
std::unique_ptr<DBResult> DBPostgresStatement::Execute()
{
	try {
		pqxx::result pqresult = m_pT->exec(m_sql);
		std::unique_ptr<DBResult> result = std::unique_ptr<DBResult>(new DBPostgresResult(pqresult));
		return result;
	} catch (std::exception& e) {
		//Postgres aborts transaction in failures
//		std::string what(e.what());
//		std::cout << "Execute error " << what << "\n";
		std::stringstream message;
		message << "DBPostgresStatement::Execute statement on database exception " << e.what();
		throw(StorageException(message.str()));
	}

}
/// Execute a cursor
/**
 * \return DBResult of fetch ready for iteration
 */
std::unique_ptr<DBResult> DBPostgresStatement::ExecuteCursor(const std::string &name)
{
	try {
		pqxx::result pgresult = m_pT->exec(m_sql);
		std::unique_ptr<DBResult> result = std::unique_ptr<DBResult>(new DBPostgresResult(pgresult));
		return result;
	} catch (std::exception& e) {
//		std::string what(e.what());
//		std::cout << "ExecuteCursor error " << what << "\n";
		std::stringstream message;
		message << "DBPostgresStatement::ExecuteCursor " << name << " statement on database exception " << e.what();
		throw(StorageException(message.str()));
	}
}
/// Execute with Returning prepare statement
/**
 * \return RowItem returning
 */
Storage::RowItem DBPostgresStatement::ExecuteReturn() {
    if (m_placeHolderRow.TotalFields() == 0) {
		throw(StorageException("No place holder for execute."));
    }
	try {
		pqxx::result pqresult = m_pT->exec(m_sql);

		if (!pqresult.empty()) {
	        for (pqxx::result::const_iterator c = pqresult.begin(); c != pqresult.end(); ++c) {
				for (unsigned short index = 0; index < m_placeHolderRow.TotalFields();
						++index) {
					int int_value;
					std::string str_value;
					FieldDetail fd = m_placeHolderRow.getFieldDetail(index);
					switch (fd.field_type) {
					case Storage::FieldType::INTEGER:
						int_value = c[index-1].as<int>();
						m_placeHolderRow.setField(index, int_value);
						break;
					case Storage::FieldType::STRING:
						str_value = c[index-1].as<std::string>();
						m_placeHolderRow.setField(index, str_value);
						break;
					default:
						//nothing
						break;
					}
				}
	        }
		}
		return m_placeHolderRow;

	} catch (std::exception& e) {
		std::stringstream message;
		message << "DBPostgresStatement::ExecuteReturn statement on database exception " << e.what();
		throw(StorageException(message.str()));
	}

}


} /* namespace Driver */
} /* namespace Storage */
