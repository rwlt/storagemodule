#include "storage/Model.h"

namespace Storage
{

  Model::Model(
      DBDriver* dbdr,
      int modelID,
      const std::string &entityName,
      const Schema schema) : DBClient(dbdr), DBModel(),
                             m_modelID{modelID},
                             m_entityName(entityName),
                             m_schema(schema)
  {
    // m_onSaveEventFunc = std::bind(&Model::save, this, _1);
    // m_onCreateEntity = std::bind(&Model::onCreateEntity, this, _1);
  }

  Model::Model() : DBClient(nullptr), DBModel(), 
                   m_modelID{0},
                   m_entityName(""),
                   m_schema{{}}
  {
  }

  // Model::Model(const Model &other) : DBClient{nullptr}, DBModel(), 
  //                                    m_modelID{0},
  //                                    m_entityName(""),
  //                                    m_schema{{}}
  // {
  //   m_dbdr = other.m_dbdr;
  //   m_autocommit = other.m_autocommit;

  //   m_modelID = other.m_modelID;
  //   m_entityName = other.m_entityName;
  //   m_schema = other.m_schema;
  //   m_onSaveEventFunc = std::bind(&Model::save, this, _1);
  //   m_onCreateEntity = std::bind(&Model::onCreateEntity, this, _1);
  // }

  // Model& Model::operator=(const Model &other) {
  //   if (this == &other) {
  //     std::cerr << "Assign were same\n";
  //     return *this;
  //   }
  //   std::cerr << "Assign = model\n";
  //   m_dbdr = other.m_dbdr;
  //   m_autocommit = other.m_autocommit;

  //   m_modelID = other.m_modelID;
  //   m_entityName = other.m_entityName;
  //   m_schema = other.m_schema;
  //   m_onSaveEventFunc = std::bind(&Model::save, this, _1);
  //   m_onCreateEntity = std::bind(&Model::onCreateEntity, this, _1);
  //   return*this;   
  // };

  Model::~Model()
  {
  }

  /// This entity is an exisitng entry from dbstorage - Entity constructor from DBRowItem is an existing
  /// entity.
  std::unique_ptr<Storage::DBEntity> Model::onCreateEntity(const DBRowItem &dbRowItem)
  {
    // Use DBRowItem RowFields passed with arg

    m_onSaveEventFunc = std::bind(&Model::save, this, _1);
    m_onUpdateEventFunc = std::bind(&Model::update, this, _1, _2);

    return std::unique_ptr<DBEntity>(new Entity(
        m_schema,
        dbRowItem, 
        m_onSaveEventFunc,
        m_onUpdateEventFunc));
        // [=](const RowFields &fields) -> std::unique_ptr<DBRowItem> {
        //   return m_dbClient->doAddItem(*this, fields, m_isBatch);
        // }));
  };

  /// This entity is a new to add to dbstorage - Entity constructor from RowItem is a new entity.
  std::unique_ptr<Storage::DBEntity> Model::entity()
  {
    //std::lock_guard<std::mutex> lock(m_DBCallMutex);
    // Use RowItemtypes RowFields from schema
    //std::cerr << "Model::entity " << m_isBatch << "\n";

    m_onSaveEventFunc = std::bind(&Model::save, this, _1);
    m_onUpdateEventFunc = std::bind(&Model::update, this, _1, _2);
    return std::unique_ptr<DBEntity>(new Entity(
        m_schema,
        std::unique_ptr<RowItem>(new RowItem(m_schema.rowFields())),
        m_onSaveEventFunc,
        m_onUpdateEventFunc));
        // [=](const RowFields &fields) -> std::unique_ptr<DBRowItem> {
        //   return m_dbClient->doAddItem(*this, fields, m_isBatch);
        // }));
  };

  // This is the Entity OnSave callback - it is also a base override of DBModel
  std::unique_ptr<DBRowItem> Model::save(const RowFields &fields)
  {
    //std::lock_guard<std::mutex> lock(m_DBCallMutex);
    //std::cerr << "Model::save " << m_isBatch << "\n";
    auto saved = /*m_dbClient->*/doAddItem(m_entityName, m_schema, fields);
    //m_dbClient->
    return saved;
  }

  std::unique_ptr<DBRowItem> Model::update(const RowFields &original, const RowFields &update)
  {
    //std::lock_guard<std::mutex> lock(m_DBCallMutex);
    return /*m_dbClient->*/doUpdateItem(m_entityName, m_schema, original, update);
  }

  std::vector<std::unique_ptr<DBEntity>> Model::findAll() 
  {
    //std::lock_guard<std::mutex> lock(m_DBCallMutex);
    //return m_onFindAll(m_entityName, m_schema, m_onCreateEntity, {}, {});
    m_onCreateEntity = std::bind(&Model::onCreateEntity, this, _1);
    return /*m_dbClient->*/onFindAll(m_entityName, m_schema, m_onCreateEntity, {}, {});
  };

  std::vector<std::unique_ptr<DBEntity>> Model::find(DBRetrieveMap where) 
  {
    //std::lock_guard<std::mutex> lock(m_DBCallMutex);
    m_onCreateEntity = std::bind(&Model::onCreateEntity, this, _1);
    return /*m_dbClient->*/onFindAll(m_entityName, m_schema, m_onCreateEntity, where, {});
  };

  // Events
  std::unique_ptr<DBEntity> defaultModelOnCreateEntityEvent(const DBRowItem &)
  {
    throw StorageException("No implemented callback - not saved by defaultModelOnCreateEntityEvent.");
  };

}