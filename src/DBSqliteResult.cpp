/*
 * DBSqliteResult.cpp
 *
 *  Created on: 30/06/2017
 *      Author: rodney
 */

#include "storage/DBSqliteResult.h"
#include "storage/StorageException.h"

#ifdef NDEBUG
#define D(x)
#else
#define D(x) x
#endif

namespace Storage::Driver
{

  /// Execute
  /**
 * \return if more to fetch
 */
  std::pair<bool, std::unique_ptr<DBRowItem>> DBSqliteResult::Fetch()
  {
    if (m_st_fetch == nullptr)
    {
      return {false, nullptr};
    }
    if (m_rc == SQLITE_DONE)
    {
      m_rc = -1;
      sqlite3_finalize(m_st_fetch);
      D(std::cerr << "prepare stmt 1 finalized\n";)
      m_st_fetch = nullptr;
      return {false, nullptr};
    }

    std::unique_ptr<DBRowItem> row;
    if (!m_raw && m_schema)
    {
      row = m_schema->createRow();
    }
    for (unsigned short index = 0; index < row->TotalFields(); ++index)
    {
      int int_value;
      std::string str_value;

      auto fd = row->getFieldType(index);
      switch (fd.second)
      {
      case FieldType::INTEGER:
      case FieldType::PK_INT:
      case FieldType::AUTOGENINT:
      {
        int_value = sqlite3_column_int(m_st_fetch, index);
        row->setField(index, int_value);
      }
      break;
      case FieldType::STRING:
      {
        str_value = std::string(reinterpret_cast<const char *>(
            sqlite3_column_text(m_st_fetch, index)));
        row->setField(index, str_value);
      }
      break;
      default: //nothing
        break;
      }
    }
    //std::cout << "Fetch:complete\n";
    m_rc = sqlite3_step(m_st_fetch);
    if (m_rc == SQLITE_DONE)
    {
      sqlite3_finalize(m_st_fetch);
      D(std::cerr << "prepare stmt 2 finalized\n";)
      m_st_fetch = nullptr;
      return {true, std::move(row)};
    }
    return {true, std::move(row)};
  }

} /* namespace Storage */
