/*
 * DBStorage.cpp
 */
#ifdef NDEBUG
#define D(x)
#else
#define D(x) x
#endif

#include "storage/DBStorage.h"
#include <iostream>
#include <unordered_map>
#include <string>
#include <sstream>
#include <functional>
#include <thread>
using namespace Storage::Driver;
using namespace Storage::Domain;

//Storage::DBStorage dbStorage; // Default dbStorage constructor

namespace Storage
{

  /**
     * DBDriver is shared interface to the abstract SQL driver - the API to database. DBDriver represents
     * a single connection to a DB (Sql or any type of datastore). DBClient is the interface to using 
     * the connected DBDriver.
     * 
     * DBStorage is constructed (<databaseconnectio>, <user?>, <password?>, <role?>) with 
     * a Database connection from DBDriver and creates the DBClient for Models with 
     * DBStorage.model(<tableName>, <schema>). Each model gets a shared interface to DBClient.
     *
     * DBStorage is not thread safe. It is require to run in a single thread to Connect to database.
     * Starting a new thread, use DBStorage in the thread is safe.
     * 
     * It is possible to use many combined DBStorage and thread, and to use multiple DBStorages that way.
     * Depends on the Database driver third party implementation. Some will lock files (Sqlite uses files). 
     * Most implementations allow many connections.
     * 
     * It is acheiveable with DBStorage to make [1:N) Models work on one connection, it has to be on the same
     * thread. 
     * 
     * For multiple threads, setup one DBStorage object per thread.
     *  One thread - one DBStorage connection, [1:N) Models.
     *
     * For batched transaction the DBStorage uses option of BeginTransaction and Commit to put sql
     * statements in a batch (makes it faster). Sqlite does autocommit with writes. Using BEGIN TRANSACTION
     * makes statements work in batches and END TRANSACTION to commit.
     * To do Non sqlite autocommit :  Use DBStorage.beginTransaction and DBStorage.commit().
     *                                Then proceed normal for Model DBClient calls on DBDriver connection.
     *     eg. beginTransaction();  (Model find, save and create); commit();
     * 
     *  POOLSIZE is max amount of DBStorages object
     *   - index [1:POOLSIZE) are DBStorage. Then Models are used with DBStorage by creating with 
     *     DBStorage.model. [1:N) Models allowed. N is any amount.
     */
  DBStorage::DBStorage(DatabaseConnection connect,
                       const std::string user,
                       const std::string password,
                       std::string role) : m_dbconnect("", "", "", ""),
                                          m_UserName(""),
                                          m_UserPassword(""),
                                          m_UserRole(""),
                                          m_connectType{ConnectionType::LIVE},
                                          m_dbdr{nullptr}
  {
    try
    {
      for (int i = 0; i < POOLSIZE; i++)
      {
        m_dbdr[i].reset();
        m_dbdr[i] = DBDriver::CreateDBDriver(connect, user, password, role);
      }

      m_UserName = user;
      m_UserPassword = password;
      m_UserRole = role;

      m_dbClient = std::unique_ptr<DBClient>(new DBClient(m_dbdr.at(0).get()));
    }
    catch (StorageException &ex)
    {
      throw(StorageException("DBStorage construction: " + std::string(ex.what())));
    }
    catch (std::exception &e)
    {
      throw(StorageException("Your user name and password are not defined. Ask your database administrator to set up a login. " + std::string(e.what())));
    }
  }

  DBStorage::~DBStorage()
  {
  }

  Model DBStorage::model(
      const std::string &entityName,
      const Schema schema,
      CreateKind createKind)
  {
    if (!m_dbdr.at(0))
    {
      throw(StorageException("No connection"));
    }

    switch (createKind)
    {
    case CreateKind::DROP_CREATE:
    {
      m_dbClient->ddlTableStatement(entityName, schema, StatementType::DROPTABLE);
      m_dbClient->ddlTableStatement(entityName, schema, StatementType::CREATETABLE);
    }
    break;
    case CreateKind::CREATE_ONLY:
      m_dbClient->ddlTableStatement(entityName, schema, StatementType::CREATETABLE);
    default:
      break;
    }
    m_modelList[m_modelID] = Model(m_dbdr.at(0).get(), m_modelID, entityName, schema);
    //std::cerr << "Model ID " << m_modelID << " " << m_modelList[m_modelID].entityName() << "\n";
    m_modelID++;

    return m_modelList[m_modelID - 1];
  }

  void DBStorage::beginTransaction()
  {
    m_dbClient->beginTransaction();
  }

  void DBStorage::commit()
  {
    m_dbClient->commit();
  }

} /* namespace Storage */
