/*
 * DBStorage.cpp
 */
#ifdef DEBUG
#define D(x)
#else
#define D(x) x
#endif

#include "storage/DBClient.h"
#include <iostream>
#include <unordered_map>
#include <string>
#include <sstream>
#include <functional>

using namespace Storage::Driver;
using namespace Storage::Domain;

namespace Storage
{

  int DBClient::tries = 0;

  DBClient::DBClient(DBDriver *dbDr) : m_dbdr{dbDr}
  {
  }

  DBClient::~DBClient()
  {
  }

  // void DBClient::pauseClient()
  // {
  //   {
  //     std::lock_guard<std::mutex> lk(m_DBPauseMutex);
  //     m_pause = true;
  //     D(std::cout << "DBClient pauseRun\n";)
  //   }
  //   m_cvPause.notify_all();
  // }

  // void DBClient::resumeClient()
  // {
  //   {
  //     std::lock_guard<std::mutex> lk(m_DBPauseMutex);
  //     m_pause = false;
  //     D(std::cout << "DBClient resumeRun\n";)
  //   }
  //   m_cvPause.notify_all();
  // }

  std::vector<std::unique_ptr<DBEntity>> DBClient::onFindAll(
      const std::string &entityName,
      const Schema &schema,
      OnCreateEntity createEntity,
      DBRetrieveMap where,
      const DBRetrieveMap &)
  {
    if (!m_dbdr)
    {
      throw(StorageException("No connection"));
    }
    // while (m_pause)
    // {
    //   std::unique_lock<std::mutex> lk(m_DBPauseMutex); // cv will wait and give us owner of lk
    //   m_cvPause.wait(lk, [this] { return &m_pause; });
    // }
    // std::lock_guard<std::mutex> lock(m_DBCallMutex);
    std::vector<std::unique_ptr<DBEntity>> list{};
    int row_count = rowCount(entityName, where);
    if (row_count < 1)
    {
      return list;
    }

    try
    {
      // if (m_autocommit)
      //   m_dbdr->BeginTransaction();

      auto stmt_select = createStatement(entityName, schema, StatementType::SELECT, where);
      auto result_select = stmt_select->Execute();
      while (row_count > 0)
      {
        auto fetch = result_select->Fetch();
        list.push_back(std::move(createEntity(*fetch.second)));
        row_count--;
      }

      // if (m_autocommit)
      //   m_dbdr->Commit();

      return list;
    }
    catch (std::exception &e)
    {
      std::string message;
      message = "Read store " + entityName + " database exception and unable to read " + e.what();
      throw StorageException(message);
    }
  }

  std::unique_ptr<DBRowItem> DBClient::doAddItem(
      const std::string &entityName,
      const Schema &schema,
      const RowFields &rowFields)
  {
    if (!m_dbdr)
    {
      throw(StorageException("No connection"));
    }
    // while (m_pause)
    // {
    //   std::unique_lock<std::mutex> lk(m_DBPauseMutex); // cv will wait and give us owner of lk
    //   m_cvPause.wait(lk, [this] { return &m_pause; });
    //   std::cerr << "*";
    // }
    // std::lock_guard<std::mutex> lock(m_DBCallMutex);
    try
    {
      // if (m_autocommit)
      //   m_dbdr->BeginTransaction();

      auto stmt = createInsertStatement(entityName, schema, rowFields);
      auto result_select = stmt->Execute(); // Sqlite use RETURNING so get the row fetch here
      auto fetch = result_select->Fetch();

      if (!fetch.first)
        return nullptr;
      // if (m_autocommit)
      //   m_dbdr->Commit();

      return std::move(fetch.second);
    }
    catch (std::exception &e)
    {
      std::string message{"Add item " + entityName + " database exception and unable to read " + e.what()};
      throw StorageException(message);
    }
  }

  std::unique_ptr<DBRowItem> DBClient::doUpdateItem(
      const std::string &entityName,
      const Schema &schema,
      const RowFields &original,
      const RowFields &rowFields)
  {
    if (!m_dbdr)
    {
      throw(StorageException("No connection"));
    }
    // while (m_pause)
    // {
    //   std::unique_lock<std::mutex> lk(m_DBPauseMutex); // cv will wait and give us owner of lk
    //   m_cvPause.wait(lk, [this] { return &m_pause; });
    // }
    // std::lock_guard<std::mutex> lock(m_DBCallMutex);

    std::stringstream sql;
    try
    {
      // for (auto col : rowFields)
      // {
      //   std::cerr << col.second.detailed() << "\n";
      // }
      // if (m_autocommit)
      //   m_dbdr->BeginTransaction();

      auto stmt = createUpdateStatement(entityName, schema, original, rowFields);
      auto result_select = stmt->Execute(); // Sqlite use RETURNING so get the row fetch here
      auto fetch = result_select->Fetch();

      // if (m_autocommit)
      //   m_dbdr->Commit();

      return std::move(fetch.second);
    }
    catch (const std::exception &e)
    {
      std::string message{"Update item " + entityName + " database exception " + e.what()};
      throw StorageException(message);
    }
    catch (...)
    {
      std::string message{"Update item " + entityName + " system exception and unable to read."};
      throw StorageException(message);
    }
  }

  void DBClient::doRemoveItem(const std::string &tableName, const RowItem &) const
  {
    std::stringstream sqldelete;
    //DatabaseStatement dbStatement(tableName);
    //dbStatement.setFields(schema.rowFields());

    try
    {
      //sqldelete << dbStatement.getSQL(Storage::StatementType::REMOVE, item)
      //          << dbStatement.getWhereSQL(Storage::StatementType::REMOVE, item);
      // {
      //   std::lock_guard<std::mutex> lock(m_DBCallMutex);
      //   auto st = m_dbdr->CreateStatement(item, sqldelete.str());
      //   st->Execute();
      //   m_dbdr->Commit();
      // }
    }
    catch (std::exception & /*e*/)
    {
      std::string message{"Remove item " + tableName + " database exception and unable to read."};
      throw StorageException(message);
    }
  }

  void DBClient::beginTransaction()
  {
    m_dbdr->BeginTransaction();
  }

  void DBClient::commit()
  {
    m_dbdr->Commit();
  }

  void DBClient::ddlTableStatement(
      const std::string &tableName,
      const Schema &schema,
      StatementType stype) const
  {
    //    std::lock_guard<std::mutex> lock(m_DBCallMutex);
    try
    {
      //m_dbdr->BeginTransaction();
      auto stmt = createStatement(tableName, schema, stype);
      stmt->ExecuteReturn();
      //m_dbdr->Commit();
    }
    catch (StorageException &e)
    {
      D(std::cerr << "ddlTableStatement " << e.what() << "\n";)
      return; // If no table found means no table to drop is good.
    }
    catch (std::exception &e)
    {
      std::string message;
      message = "ddlTableStatement " + tableName + " database exception and unable to drop " + e.what();
      throw StorageException(message);
    }
  }

  int DBClient::rowCount(const std::string &entityName, DBRetrieveMap where) const
  {
    int row_count;
    Schema schemaCount({{"COUNT", FieldType::INTEGER}});
    try
    {
      // if (m_autocommit)
      //   m_dbdr->BeginTransaction();

      auto stmt_cnt = createStatement(entityName, schemaCount, StatementType::COUNT, where);
      auto result = stmt_cnt->Execute();
      result->useSchema(&schemaCount);
      auto fetch = result->Fetch();
      fetch.second->getField(0, row_count);
      
      // if (m_autocommit)
      //   m_dbdr->Commit();

      return row_count;
    }
    catch (std::exception &e)
    {
      std::string message;
      message = "Read store rowCount " + entityName + " database exception and unable to read " + e.what();
      throw StorageException(message);
    }
  }

  std::unique_ptr<DBStatement> DBClient::createStatement(
      const std::string entityName,
      const Schema &schema,
      StatementType stype,
      DBRetrieveMap where) const
  {
    std::unique_ptr<DBStatement> stmt;

    if (!where.empty())
    {
      RowItem rowWhereCriteria = whereCriteria(where);
      stmt = m_dbdr->CreateStatement(
          entityName,
          schema,
          &rowWhereCriteria,
          stype);
    }
    else
    {
      stmt = m_dbdr->CreateStatement(
          entityName,
          schema,
          {nullptr},
          stype);
    }
    return stmt;
  }

  std::unique_ptr<DBStatement> DBClient::createInsertStatement(
      const std::string entityName,
      const Schema &schema,
      const RowFields &fields) const
  {
    std::unique_ptr<DBStatement> stmt;
    RowItem rowItem(fields);
    stmt = m_dbdr->CreateInsertStatement(
        entityName,
        schema,
        rowItem);
    return stmt;
  }

  std::unique_ptr<DBStatement> DBClient::createUpdateStatement(
      const std::string entityName,
      const Schema &schema,
      const RowFields &original,
      const RowFields &fields) const
  {
    std::unique_ptr<DBStatement> stmt;
    RowItem originalRow(original);
    RowItem rowItem(fields);
    stmt = m_dbdr->CreateUpdateStatement(
        entityName,
        schema,
        originalRow,
        rowItem);
    return stmt;
  }

  RowItem DBClient::whereCriteria(DBRetrieveMap &where) const
  {
    RowFields fields;
    unsigned short index = 0;
    for (const FieldDetail &fd : where)
    {
      fields.insert(std::make_pair(index, fd));
      ++index;
    }
    RowItem rowWhereCriteria(fields);
    return rowWhereCriteria;
  }

  std::string DBClient::toUppercase(const char *name)
  {
    int counter = 0;
    char upd[40];
    char *p_upd = upd;
    while (name[counter])
    {
      *p_upd = (char)toupper(name[counter]);
      p_upd++;
      counter++;
    }
    *p_upd = '\0';
    return std::string(upd);
  }

} /* namespace Storage */
