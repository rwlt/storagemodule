
#include "storage/DatabaseStatement.h"
#include <algorithm>

namespace Storage
{

  DatabaseStatement::DatabaseStatement(
      const std::string &tableName,
      DBOType dboType) : m_tableName(tableName),
                         m_fields{},
                         m_dboType(dboType),
                         m_params{}
  {
    //std::cout << "DatabaseStatement constr\n";
  }

  /// Virtual deconstructor
  DatabaseStatement::~DatabaseStatement(){};

  void DatabaseStatement::setFields(const RowFields &fields)
  {
    std::cerr << "DatabaseStatement::setFields : " << fields.size() << "\n";
    m_fields = fields;
  }

  /// Output a sql statement to do the statement type
  std::string DatabaseStatement::getSQL(StatementType stype, const RowItem &item)
  {
    std::stringstream sql;
    if (m_dboType == DBOType::TABLE)
    {
      switch (stype)
      {
      case Storage::StatementType::SELECT:
        sql << "SELECT " << statementFieldDetail(stype, item) << " FROM " << m_tableName << " ";
        break;
      case Storage::StatementType::INSERT:
        sql << "INSERT INTO " << m_tableName << "("
            << statementFieldDetail(stype, item)
            << ") VALUES ("
            << statementFieldValueDetail(item) << ") "
            << "RETURNING " << statementReturningFieldDetail(stype, item);
        break;
      case Storage::StatementType::UPDATE:
        sql << "UPDATE " << m_tableName << " "
            << statementFieldDetail(stype, item) << " ";
        break;
      case Storage::StatementType::REMOVE:
        sql << "DELETE FROM " << m_tableName << " ";
        break;
      default:
        /* do nothing */
        break;
      }
    }
    else if (m_dboType == DBOType::PROCEDURE)
    {
      if (stype == Storage::StatementType::SELECT)
      {
        // Depending on if a cursor or function we can select the fields
        sql << "SELECT " << statementFieldDetail(stype, item) << " FROM " << m_tableName
            << "(" << parameterValues() << ") ";
      }
    }
    return sql.str();
    ;
  }
  /// Output a sql statement to Count what result will be of statement type
  std::string DatabaseStatement::getSQLCount(StatementType /*stype*/, const RowItem &item)
  {
    std::stringstream sql;
    unsigned short mcount = 0;
    auto countColumn = columnStatement(StatementType::SELECT, item, 0, mcount);
    if (m_dboType == DBOType::TABLE)
    {
      sql << "SELECT COUNT(" << countColumn << ") FROM " << m_tableName << " ";
    }
    else if (m_dboType == DBOType::PROCEDURE)
    {
      sql << "SELECT COUNT(*) FROM " << m_tableName << "(" << parameterValues() << ") ";
    }
    return sql.str();
    ;
  }
  //
  std::string DatabaseStatement::getWhereSQL(StatementType stype, const RowItem &item)
  {

    std::stringstream sql;
    switch (stype)
    {
    case Storage::StatementType::SELECT:
      sql << " WHERE " << statementWhereFieldDetail(item) << " ";
      break;
    case Storage::StatementType::INSERT:
      sql << " ";
      break;
    case Storage::StatementType::UPDATE:
      sql << " WHERE " << statementWhereFieldDetail(item) << " ";
      break;
    case Storage::StatementType::REMOVE:
      sql << " WHERE " << statementWhereFieldDetail(item) << " ";
      break;
      //    default:
      //        /* do nothing */
      //        break;
    }
    return sql.str();
    ;
  }
  //
  // For a report of what is in database statement - only for reporting and is a string
  std::string DatabaseStatement::StatementDetail()
  {
    std::stringstream value;
    value << m_tableName;
    if (m_fields.size() > 0)
    {
      value << ":";
    }
    for (unsigned short index = 0; index < m_fields.size(); ++index)
    {
      auto fd = m_fields.at(index);
      value << " " << index + 1 << ". " << fd.detailed();
    }
    return value.str();
  }
  //
  void DatabaseStatement::setParameterValues(const Storage::DBRetrieveMap &params)
  {
    // Set the parameters with new values for next sql statement creation
    unsigned short index = 0;
    for (FieldDetail fdvalue : params)
    {
      if (m_params.count(index))
      {
        FieldDetail &fdparam = m_params.at(index);
        switch (fdparam.field_type)
        {
        case FieldType::INTEGER:
          fdparam.int_value = fdvalue.int_value;
          break;
        case FieldType::STRING:
          fdparam.str_value = fdvalue.str_value;
          break;
        default:
          /* nothing */
          break;
        }
      }
      ++index;
    }
  }
  //-----------------------------------------------------------------------------
  // Private methods
  // Used from parse database statement to set up m_parameters with configuration
  //-----------------------------------------------------------------------------
  /// Set Parameter
  void DatabaseStatement::setParameter(unsigned short index, const std::string &name,
                                       Storage::FieldType field_type)
  {
    if (m_params.count(index) == 0)
    {
      // Create the parameter detail and its initialize state
      FieldDetail fd;
      fd.field_type = field_type;
      fd.field_name = name;
      fd.int_value = 0;
      fd.str_value = "";
      //  fd.key_field = false;
      fd.is_set = false;
      m_params.insert(std::make_pair(index, fd));
      // End of create parameter detail initial state
    }
  }
  //
  //

  std::string DatabaseStatement::columnStatement(
      StatementType stype,
      const RowItem &item,
      unsigned short index,
      unsigned short &made_count)
  {
    std::stringstream value;
    FieldDetail fd = item.getFieldDetail(index);
    if (stype == Storage::StatementType::UPDATE)
    {
      int int_value;
      std::string str_value;
      bool is_set;
      if (index == 0)
      {
        value << "SET ";
      }
      if (made_count > 0)
      {
        value << ", ";
      }
      value << fd.field_name << " = ";
      switch (fd.field_type)
      {
      case Storage::FieldType::INTEGER:
        item.getField(index, int_value, is_set);
        value << int_value;
        break;
      case Storage::FieldType::STRING:
        item.getField(index, str_value, is_set);
        value << "'" << str_value << "'";
        break;
      default:
        //nothing
        break;
      }
      //value << fd.field_name << " = ?";
      ++made_count;
    }
    else if (stype == Storage::StatementType::INSERT)
    {
      //std::cout << "INSERT DBS: " << fd.detailed() << "\n";
      if (fd.is_autogen && fd.int_value == 0 && fd.field_type == FieldType::INTEGER)
      {
        return value.str();
      }
      if (made_count > 0)
      {
        value << ", ";
      }
      value << fd.field_name;
      ++made_count;
    }
    else
    {
      if (made_count > 0)
      {
        value << ", ";
      }
      value << fd.field_name;
      ++made_count;
    }
    return value.str();
  }

  std::string DatabaseStatement::statementFieldDetail(StatementType stype, const RowItem &item)
  {
    std::stringstream value; // we always do index from 1 and sequentially + 1 to Total fields
    // It is required to have a normal sequence of 1 to N without any missed values
    unsigned short total = item.TotalFields();
    unsigned short made_count = 0;
    for (unsigned short index = 0; index < total; ++index)
    {
      value << columnStatement(stype, item, index, made_count);
    }
    return value.str();
  }
  //
  std::string DatabaseStatement::statementReturningFieldDetail(StatementType stype, const RowItem &item)
  {
    std::stringstream value; // we always do index from 1 and sequentially + 1 to Total fields
    // It is required to have a normal sequence of 1 to N without any missed values
    unsigned short total = item.TotalFields();
    unsigned short made_count = 0;
    for (unsigned short index = 0; index < total; ++index)
    {
      FieldDetail fd = item.getFieldDetail(index);
      if (stype == Storage::StatementType::UPDATE)
      {
        if (index == 0)
        {
          value << "SET ";
        }
        if (made_count > 0)
        {
          value << ", ";
        }
        value << fd.field_name << " = ?";
        ++made_count;
      }
      else
      {
        if (made_count > 0)
        {
          value << ", ";
        }
        value << fd.field_name;
        ++made_count;
      }
    }
    return value.str();
  }
  //
  std::string DatabaseStatement::statementFieldValueDetail(const RowItem &item)
  {
    std::stringstream value; // we always do index from 1 and sequentially + 1 to Total fields
    // It is required to have a normal sequence of 1 to N without any missed values
    unsigned short total = item.TotalFields();
    unsigned short made_count = 0;
    for (unsigned short index = 0; index < total; ++index)
    {
      FieldDetail fd = item.getFieldDetail(index);
      int int_value;
      std::string str_value;
      bool is_set;

      if (fd.is_autogen && fd.int_value == 0 && fd.field_type == FieldType::INTEGER)
      {
        continue;
      }
      if (made_count > 0)
      {
        value << ", ";
      }
      switch (fd.field_type)
      {
      case Storage::FieldType::INTEGER:
        item.getField(index, int_value, is_set);
        if (is_set == true)
        {
          value << int_value;
        }
        else
        {
          value << 0;
        }
        ++made_count;
        break;
      case Storage::FieldType::STRING:
        item.getField(index, str_value, is_set);
        if (is_set == true)
        {
          value << "'" << str_value << "'";
        }
        else
        {
          value << "''";
        }
        ++made_count;
        break;
      default:
        //nothing
        break;
      }
      ++made_count;
    }
    return value.str();
  }
  // create where clause using the item field values
  // only add field conditon if a value is present
  std::string DatabaseStatement::statementWhereFieldDetail(const RowItem &item)
  {
    std::stringstream value;
    // we always do index from 1 and sequentially + 1 to Total fields
    // It is required to have a normal sequence of 1 to N without any missed values
    unsigned short total = item.TotalFields();
    unsigned short made_count = 0;
    for (unsigned short index = 0; index < total; ++index)
    {
      // If field is a key we output it for where clause - specific to this storage
      // lib configuration and only when it has a value given.
      FieldDetail fd = item.getFieldDetail(index);
      int int_value;
      std::string str_value;
      bool is_set;
      switch (fd.field_type)
      {
      case Storage::FieldType::INTEGER:
      case Storage::FieldType::AUTOGENINT:
        item.getField(index, int_value, is_set);
        if (is_set == true)
        {
          if (made_count > 0)
          {
            value << " AND ";
          }
          value << fd.field_name << " = " << int_value;
          ++made_count;
        }
        break;
      case Storage::FieldType::STRING:
        item.getField(index, str_value, is_set);
        if (is_set == true)
        {
          if (made_count > 0)
          {
            value << " AND ";
          }
          value << fd.field_name << " = '" << str_value << "'";
          ++made_count;
        }
        break;
      }
    }
    return value.str();
  }

  std::string DatabaseStatement::parameterValues()
  {
    std::stringstream params_str;
    for (unsigned short index = 0; index < m_params.size(); ++index)
    {
      if (m_params.count(index))
      {
        if (index > 0)
        {
          params_str << ",";
        }
        FieldDetail &fdparam = m_params.at(index);
        switch (fdparam.field_type)
        {
        case FieldType::INTEGER:
          params_str << fdparam.int_value;
          break;
        case FieldType::STRING:
          params_str << "'" << fdparam.str_value << "'";
          break;
        default:
          /* nothing */
          break;
        }
      }
    }
    return params_str.str();
  }

}
