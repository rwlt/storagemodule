
#include "storage/Schema.h"
#include <algorithm>
#include <sstream>

namespace Storage::Domain
{

  Schema::Schema(SchemaFields fields) : m_fields{}, m_fieldNameIndex{}
  {
    // SchemaFields is translated to RowFields structure.
    unsigned short index = 0;
    std::for_each(fields.begin(), fields.end(), [&](const SchemaField &fd) -> void {
      auto field_name = std::get<0>(fd);
      auto field_type = std::get<1>(fd);
      setField(index, field_name, field_type);
      m_fieldNameIndex[field_name] = index;
      index++;
    });
  };

  /// Virtual deconstructor
  Schema::~Schema(){};

  //
  //-----------------------------------------------------------------------------
  // Private methods
  // Used from constructor to set up RowFields m_fields configuration
  //-----------------------------------------------------------------------------
  /// Set Field
  void Schema::setField(
      unsigned short index,
      const std::string &field,
      FieldType field_type)
  {
    if (m_fields.count(index) == 0)
    {
      FieldDetail fd;
      fd.field_type = field_type;
      fd.field_name = field;
      fd.int_value = 0;
      fd.str_value = "";
      m_fields.insert(std::make_pair(index, fd));
    }
  };

}
