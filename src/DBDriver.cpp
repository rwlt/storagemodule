/*
 * Driver.cpp
 *
 *  Created on: 3/07/2017
 *      Author: rodney
 */

#include "storage/DBDriver.h"
#ifdef HAVE_POSTGRES
#include "storage/DBPostgresDriver.h"
#endif
#ifdef HAVE_SQLITE
#include "storage/DBSqliteDriver.h"
#endif
#include "storage/StorageException.h"
#include <memory>
#include <sstream>
//#include "storage/Schema.h"

#ifdef NDEBUG
#define D(x)
#else
#define D(x) x
#endif

namespace Storage::Driver
{
  std::unique_ptr<DBDriver> DBDriver::CreateDBDriver(
      const DatabaseConnection &db_connect,
      const std::string &user,
      const std::string &password,
      const std::string &role)
  {
    std::shared_ptr<DBDriver> dr{};
    if (db_connect.getDriver().empty())
    {
      throw(StorageException("CreateDBDriver error no driver specified exception."));
    }
#ifdef HAVE_POSTGRES
    if (dbconnect.getDriver().compare("postgres") == 0)
    {
      return std::unique_ptr<DBPostgresDriver>(new DBPostgresDriver(dbconnect, user, password, role));
    }
#endif
#ifdef HAVE_SQLITE
    if (db_connect.getDriver() == "Sqlite")
    {
      return std::make_unique<DBSqliteDriver>(db_connect, user, password, role);
    }
#endif
    std::stringstream message;
    message << "CreateDBDriver error no driver for " << db_connect.getDriver() << " exception.";
    throw(StorageException(message.str()));
  }

  // Base SQL generator - can be overridden by specific Derived drivers
  std::string DBDriver::getSQL(
      const std::string &entityName,
      const DBRowItem &item,
      StatementType stype,
      DBOType dboType)
  {
    std::stringstream sql;
    if (dboType == DBOType::TABLE)
    {
      switch (stype)
      {
      case StatementType::SELECT:
        sql << "SELECT " << statementFieldDetail(stype, item) << " FROM " << entityName << " ";
        break;
      case StatementType::INSERT:
        sql << "INSERT INTO " << entityName << "("
            << statementFieldDetail(stype, item)
            << ") VALUES ("
            << statementFieldValueDetail(item) << ") ";
            //<< "RETURNING " << statementReturningFieldDetail(stype, item);
        break;
      case StatementType::UPDATE:
        sql << "UPDATE " << entityName << " "
            << statementFieldDetail(stype, item) << " ";
        break;
      case StatementType::REMOVE:
        sql << "DELETE FROM " << entityName << " ";
        break;
      case StatementType::DROPTABLE:
        sql << "DROP TABLE " << entityName << " ";
        break;
      case StatementType::CREATETABLE:
        sql << "CREATE TABLE " << entityName << " (" << statementFieldDetail(stype, item) << "); ";
        break;
      default:
        /* do nothing */
        break;
      }
    }
    else if (dboType == DBOType::PROCEDURE)
    {
      if (stype == StatementType::SELECT)
      {
        // Depending on if a cursor or function we can select the fields
        sql << "SELECT " << statementFieldDetail(stype, item) << " FROM " << entityName;
        // << "(" << parameterValues() << ") ";
      }
    }
    return sql.str();
    ;
  }

  /// Output a sql statement to Count what result will be of statement type
  std::string DBDriver::getSQLCount(
      const std::string &entityName,
      const DBRowItem &,
      DBOType dboType)
  {
    std::stringstream sql;
    //unsigned short mcount = 0;
    //auto countColumn = columnStatement(StatementType::SELECT, item, 0, mcount);
    if (dboType == DBOType::TABLE)
    {
      sql << "SELECT COUNT(*) FROM " << entityName << " ";
      //sql << "SELECT COUNT(" << countColumn << ") FROM " << entityName << " ";
    }
    else if (dboType == DBOType::PROCEDURE)
    {
      //sql << "SELECT COUNT(*) FROM " << entityName << "(" << parameterValues() << ") ";
    }
    return sql.str();
    ;
  }

  std::string DBDriver::getWhereSQL(StatementType stype, const DBRowItem &item)
  {

    std::stringstream sql;
    switch (stype)
    {
    case StatementType::SELECT:
      sql << " WHERE " << statementWhereFieldDetail(item) << " ";
      break;
    case StatementType::COUNT:
      sql << " WHERE " << statementWhereFieldDetail(item) << " ";
      break;
    case StatementType::INSERT:
      sql << " ";
      break;
    case StatementType::UPDATE:
      sql << " WHERE " << statementWhereFieldDetail(item) << " ";
      break;
    case StatementType::REMOVE:
      sql << " WHERE " << statementWhereFieldDetail(item) << " ";
      break;
    default:
      throw std::out_of_range("getWhereSQL storage type not found");
    }
    return sql.str();
    ;
  }

  std::string DBDriver::statementFieldDetail(StatementType stype, const DBRowItem &item)
  {
    std::stringstream value;
    // It is required to have a normal sequence of [0:N) without any missed values
    unsigned short total = item.TotalFields();
    unsigned short made_count = 0;
    for (unsigned short index = 0; index < total; ++index)
    {
      value << columnStatement(stype, item, index, made_count);
    }
    return value.str();
  }
  //
  std::string DBDriver::statementReturningFieldDetail(StatementType stype, const DBRowItem &item)
  {
    std::stringstream value;
    // It is required to have a normal sequence of [0:N) without any missed values
    unsigned short total = item.TotalFields();
    unsigned short made_count = 0;
    for (unsigned short index = 0; index < total; ++index)
    {

      auto fd = item.getFieldType(index);
      if (stype == StatementType::UPDATE)
      {
        if (index == 0)
        {
          value << "SET ";
        }
        if (made_count > 0)
        {
          value << ", ";
        }
        value << fd.first << " = ?";
        ++made_count;
      }
      else
      {
        if (made_count > 0)
        {
          value << ", ";
        }
        value << fd.first;
        ++made_count;
      }
    }
    return value.str();
  }
  //
  std::string DBDriver::statementFieldValueDetail(const DBRowItem &item)
  {
    std::stringstream value;
    // It is required to have a normal sequence of [0:N) without any missed values
    unsigned short total = item.TotalFields();
    unsigned short made_count = 0;
    for (unsigned short index = 0; index < total; ++index)
    {
      auto fd = item.getFieldType(index);
      int int_value;
      std::string str_value;

      if (fd.second == FieldType::AUTOGENINT)
      {
        continue;
      }
      if (made_count > 0)
      {
        value << ", ";
      }
      switch (fd.second)
      {
      case FieldType::INTEGER:
      case FieldType::PK_INT:
      {
        item.getField(index, int_value);
        value << int_value;
        ++made_count;
      }
      break;
      case FieldType::STRING:
      {
        item.getField(index, str_value);
        value << "'" << str_value << "'";
        ++made_count;
      }
      break;
      default:
        //nothing
        break;
      }
      ++made_count;
    }
    return value.str();
  }

  // create where clause using the item field values
  // only add field conditon if a value is present
  std::string DBDriver::statementWhereFieldDetail(const DBRowItem &item)
  {
    std::stringstream value;
    // It is required to have a normal sequence of [0:N) without any missed values
    unsigned short total = item.TotalFields();
    unsigned short made_count = 0;
    for (unsigned short index = 0; index < total; ++index)
    {
      // If field is a key we output it for where clause - specific to this storage
      auto fd = item.getFieldType(index);
      int int_value;
      std::string str_value;

      switch (fd.second)
      {
      case FieldType::INTEGER:
      case FieldType::AUTOGENINT:
      case FieldType::PK_INT:
      {
        item.getField(index, int_value);
        if (made_count > 0)
        {
          value << " AND ";
        }
        value << fd.first << " = " << int_value;
        ++made_count;
      }
      break;
      case FieldType::STRING:
      {
        item.getField(index, str_value);
        if (made_count > 0)
        {
          value << " AND ";
        }
        value << fd.first << " = '" << str_value << "'";
        ++made_count;
      }
      break;
      }
    }
    return value.str();
  }

  std::string DBDriver::columnStatement(
      StatementType stype,
      const DBRowItem &item,
      unsigned short index,
      unsigned short &made_count)
  {
    std::stringstream value;
    auto fd = item.getFieldType(index);
    if (stype == StatementType::UPDATE)
    {
      int int_value;
      std::string str_value;

      if (index == 0)
      {
        value << "SET ";
      }
      if (made_count > 0)
      {
        value << ", ";
      }
      value << fd.first << " = ";
      switch (fd.second)
      {
      case FieldType::INTEGER:
      case FieldType::PK_INT:
      case FieldType::AUTOGENINT:
      {
        item.getField(index, int_value);
        value << int_value;
      }
      break;
      case FieldType::STRING:
      {
        item.getField(index, str_value);
        value << "'" << str_value << "'";
      }
      break;
      default:
        //nothing
        break;
      }
      //value << fd.field_name << " = ?";
      ++made_count;
    }
    else if (stype == StatementType::INSERT)
    {
      //std::cout << "INSERT DBS: " << fd.detailed() << "\n";
      if (fd.second == FieldType::AUTOGENINT)
      {
        return value.str(); //empty string
      }
      if (made_count > 0)
      {
        value << ", ";
      }
      value << fd.first;
      ++made_count;
    }
    else if (stype == StatementType::CREATETABLE)
    {
      if (made_count > 0)
      {
        value << ", ";
      }
      if (fd.second == FieldType::AUTOGENINT)
      {
        value << fd.first << " INTEGER PRIMARY KEY NOT NULL";
      }
      else if (fd.second == FieldType::PK_INT)
      {
        value << fd.first << " INTEGER PRIMARY KEY NOT NULL";
      }
      else if (fd.second == FieldType::INTEGER)
      {
        value << fd.first << " INT NOT NULL";
      }
      else if (fd.second == FieldType::STRING)
      {
        value << fd.first << " VARCHAR(50) NOT NULL";
      }
      ++made_count;
    }
    else
    {
      if (made_count > 0)
      {
        value << ", ";
      }
      value << fd.first;
      ++made_count;
    }
    return value.str();
  }

  std::string DBDriver::parameterValues()
  {
    std::stringstream params_str;
    // for (unsigned short index = 0; index < m_params.size(); ++index)
    // {
    //   if (m_params.count(index))
    //   {
    //     if (index > 0)
    //     {
    //       params_str << ",";
    //     }
    //     FieldDetail &fdparam = m_params.at(index);
    //     switch (fdparam.field_type)
    //     {
    //     case FieldType::INTEGER:
    //       params_str << fdparam.int_value;
    //       break;
    //     case FieldType::STRING:
    //       params_str << "'" << fdparam.str_value << "'";
    //       break;
    //     default:
    //       /* nothing */
    //       break;
    //     }
    //   }
    // }
    return params_str.str();
  }

  void DBDriver::setParameter(
      unsigned short,
      const std::string &,
      FieldType)
  {
    // if (m_params.count(index) == 0)
    // {
    //   // Create the parameter detail and its initialize state
    //   FieldDetail fd;
    //   fd.field_type = field_type;
    //   fd.field_name = name;
    //   fd.int_value = 0;
    //   fd.str_value = "";
    //   m_params.insert(std::make_pair(index, fd));
    //   // End of create parameter detail initial state
    // }
  }
  //
  //

} /* namespace Storage */
