/*
 * DBSqliteDriver.cpp
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#include "storage/DBSqliteDriver.h"
#include "storage/DBSqliteStatement.h"
#include "storage/StorageException.h"
#include <memory>
#include <fstream>
#include <thread>

#ifdef NDEBUG
#define D(x)
#else
#define D(x) x
#endif

int busy_callback(void *, int tries)
{
  if (tries > 4000000)
  {
    std::cerr << "too many";
    return 0;
  }
  else
  {
    return 1;
  }
}

namespace Storage::Driver
{
  int DBSqliteDriver::xtries1 = 0;
  int DBSqliteDriver::xtries = 0;

  /// Connect to Sqlite3 DB
  /**
 * Able to use Connect which will disconnect an earlier Connect on this driver instance
 * \param user
 * \param password
 * \param role
 */
  DBSqliteDriver::DBSqliteDriver(
      const DatabaseConnection &db_connect,
      const std::string & /*user*/,
      const std::string & /*password*/,
      const std::string & /*role*/) : DBDriver(db_connect)
  {
    xtries++;
    // if (xtries == 1)
    // {
    //   int rc;
    //   rc = sqlite3_config(SQLITE_CONFIG_SERIALIZED);
    //   if (rc)
    //   {
    //     std::stringstream message;
    //     message << "DBSqliteDriver::config exception " << rc << " ";
    //     throw(StorageException(message.str()));
    //   }
    //   D(std::cerr << "Theadsafe " << sqlite3_threadsafe() << "\n";)
    //   rc = sqlite3_initialize();
    //   if (rc)
    //   {
    //     std::stringstream message;
    //     message << "DBSqliteDriver::initialize exception " << rc << " ";
    //     throw(StorageException(message.str()));
    //   }
    // }
    /* Open database */
    int rc = sqlite3_open_v2(m_db_connect.getDBFile().c_str(), &m_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX, nullptr);
    //rc = sqlite3_open(m_db_connect.getDBFile().c_str(), &m_db); //, SQLITE_OPEN_FULLMUTEX, "");
    if (rc || (!m_db))
    {
      std::stringstream message;
      message << "DBSqliteDriver::Connect database exception ";
      if (m_db)
        message << sqlite3_errmsg(m_db);
      throw(StorageException(message.str()));
    }
    //sqlite3_busy_timeout(m_db, 4000); // 4 secs for one thread to finish a write if another waits
    sqlite3_busy_handler(m_db, &busy_callback, nullptr);
    D(std::cerr << "Sqlite created " << xtries1++ << "\n";)
  }

  DBSqliteDriver::~DBSqliteDriver()
  {
    //disconnect();
    xtries--;
    // if (xtries == 0)
    // {
    //   //std::cerr << "sqlite shutdown " << xtries << "\n";
    //   sqlite3_shutdown();
    // }
    m_TransactionMutex.unlock();
    int rc;
    rc = sqlite3_close(m_db);
    if (rc == SQLITE_OK)
    {
      m_db = nullptr;
    }
    else
    {
      std::cerr << "DBSqliteDriver::~DBSqliteDriver sqlite close with " + std::to_string(rc) + "\n";
    }
    D(std::cerr << "Sqlite closed " << xtries++ << "\n";)
  };

  /// BeginTransaction
  /** 
   * Begin a transaction block to allow more than one statement to be done before Commit or End transaction
   * which happens in Commit() method of this class.
   */
  void DBSqliteDriver::BeginTransaction()
  {
    if (m_hasTransactionStarted)
    {
      throw(StorageException(
          "DBSqliteDriver::BeginTransaction. Can not begin transactional another is started."));
    }

    m_TransactionMutex.lock(); // this is unlocked at Commit allowing owner the transactions
    m_hasTransactionStarted = true;
    // This will use sqlite3_busy_handler to try to get sqlite file to be exclusive
    // BEGIN IMMEDIATE means the write or read process is to be exclusive at begin of
    // sql statements transactions.
    int rc;
    D(std::cerr << "DBSqliteDriver::CreateStatement. Transactional Started\n";)
    rc = sqlite3_exec(m_db, "BEGIN IMMEDIATE TRANSACTION;", nullptr, nullptr, nullptr);
    if (rc)
    {
      m_TransactionMutex.unlock(); // a problem we remove transaction lock now
      m_hasTransactionStarted = false;
      std::stringstream message;
      message << "DBSqliteDriver::CreateStatement. Prepare transaction on database exception  "
              << sqlite3_errmsg(m_db);
      throw(StorageException(message.str()));
    }
  }

  /// Commit
  void DBSqliteDriver::Commit()
  {
    // if (!m_hasTransactionStarted)
    // {
    //   throw(StorageException(
    //       "DBSqliteDriver::Commit. No transactional in progress."));
    // }
    if (m_hasTransactionStarted)
    {
      //std::lock_guard<std::mutex> lock(m_endTransaction);
      m_TransactionMutex.unlock();
      int rc;
      rc = sqlite3_exec(m_db, "END TRANSACTION;", nullptr, nullptr, nullptr);
      D(std::cerr << "DBSqliteDriver::Commit Transactional Ended \n";)
      if (rc)
      {
        std::stringstream message;
        message << "DBSqliteDriver::Commit on database exception  "
                << sqlite3_errmsg(m_db);
        throw(StorageException(message.str()));
      }
      m_hasTransactionStarted = false;
    }
  }

  /// Prepare
  /**
 * \param sql
 */
  std::unique_ptr<DBStatement> DBSqliteDriver::CreateStatement(
      const std::string &sql)
  {
    // if (!m_hasTransactionStarted)
    // {
    //   throw(StorageException(
    //       "DBSqliteDriver::CreateStatement. No transaction in database. Did you need beginTransaction()."));
    // }
    D(std::cerr << "DBSqliteDriver::CreateStatement. Created Statement " << sql << "\n";)
    std::unique_ptr<DBStatement> statement = std::make_unique<DBSqliteStatement>(m_db, sql);
    return statement;
  }

  std::unique_ptr<DBStatement> DBSqliteDriver::CreateStatement(
      const DBSchema &schema,
      const std::string &sql)
  {
    // if (!m_hasTransactionStarted)
    // {
    //   throw(StorageException(
    //       "DBSqliteDriver::CreateStatement. No transaction in database. Did you need beginTransaction()."));
    // }
    D(std::cerr << "DBSqliteDriver::CreateStatementwith schema. Created Statement " << sql << "\n";)
    std::unique_ptr<DBStatement> statement = std::make_unique<DBSqliteStatement>(m_db, sql, schema);
    return statement;
  }

  std::unique_ptr<DBStatement> DBSqliteDriver::CreateStatement(
      const std::string entityName,
      const DBSchema &schema,
      const DBRowItem *where,
      const StatementType statementType)
  {
    auto rowItem = schema.createRow();
    std::stringstream sql{};
    if (statementType == StatementType::COUNT)
    {
      sql << getSQLCount(entityName, *rowItem);
    }
    else
    {
      sql << getSQL(entityName, *rowItem, statementType);
    }
    if (where)
    {
      sql << getWhereSQL(statementType, *where); // Do Where SQL with where dbrowitem
    }
    //std::cout << "SQL " << sql.str() << "\n";
    return CreateStatement(schema, sql.str());
  }

  std::unique_ptr<DBStatement> DBSqliteDriver::CreateInsertStatement(
      const std::string entityName,
      const DBSchema &schema,
      const DBRowItem &row)
  {
    std::stringstream sql{};
    sql << getSQL(entityName, row, StatementType::INSERT);
    return CreateStatement(schema, sql.str());
  }

  std::unique_ptr<DBStatement> DBSqliteDriver::CreateUpdateStatement(
      const std::string entityName,
      const DBSchema &schema,
      const DBRowItem &original,
      const DBRowItem &row)
  {
    std::stringstream sql{};
    // for (auto col = 0;  col < original.TotalFields(); col++)
    // {
    //   std::cerr << "SQLite : " << original.getFieldType(col).first << "\n";
    // }
    sql << getSQL(entityName, row, StatementType::UPDATE);
    sql << getWhereSQL(StatementType::UPDATE, original);
    //std::cerr << "SQLite : " << sql.str() << "\n";
    return CreateStatement(schema, sql.str());
  }

} /* namespace Storage */
