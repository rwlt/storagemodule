
#include "storage/DatabaseConnection.h"

Storage::Driver::DatabaseConnection::DatabaseConnection(
    std::string driver,
    std::string server,
    std::string dbFile,
    std::string charSet) : m_driver{driver},
                           m_server{server},
                           m_dbFile{dbFile},
                           m_charSet{charSet}
{
}

/// Driver

std::string Storage::Driver::DatabaseConnection::getDriver() const
{
  return m_driver;
}
/// Server

std::string Storage::Driver::DatabaseConnection::getServer()
{
  return m_server;
}
/// DBFile

std::string Storage::Driver::DatabaseConnection::getDBFile()
{
  return m_dbFile;
}
/// CharSet

std::string Storage::Driver::DatabaseConnection::getCharSet()
{
  return m_charSet;
}
